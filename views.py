from run import app
from flask import jsonify, render_template

@app.route('/')
def index():
    return app.send_static_file('index.html')