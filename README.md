# Status Hammer

Development Stack:
- Custom headless frontend framework running on a apache server
- Flask API on Heroku
- (Not in this repo) linux container with Slack and Selenium 

Status Hammer was a remote Slack client that interacts with slack for you. We would spawn a container that would run selenium and Slack. We would then control interactions based on variables to keep the user engaged with slack just the right amount to appease anyone observing (beyond posting and status activation nothing more exciting was put in practice). 

Slack is a distracting program and not always ideal to have open and running. But you don’t want your team to feel like you’re not there. Thus... Status Hammer. 

## Legacy

So what happened to it... work, life, COVID. 

The headless framework was adopted by Quiltster.com. 
