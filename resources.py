from functools import wraps
from globals import Globals
import json
from flask import request, jsonify
from flask_restful import Resource, reqparse
from models import UserModel, RevokedTokenModel, TeamModel, NotificationType, ExceptionModel, ShclientResponses
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies,
    set_refresh_cookies, unset_jwt_cookies, get_raw_jwt,get_csrf_token
)
from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime, timedelta, timezone
from dateutil.relativedelta import *
import stripe
import requests
from requests_futures.sessions import FuturesSession
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from sentry_sdk import capture_exception
from sentry_sdk import capture_message
import digitalocean
from cryptography.fernet import Fernet
import socket
import os
import uuid 

sg = SendGridAPIClient('SGKEY')
stripe.api_key = "STRIPEKEY"
session = FuturesSession()

def response_hook(resp, *args, **kwargs):
    print(args)
    print(kwargs)
    print(resp.json)
session.hooks['response'] = response_hook
def update_team(session, team, now):
    print("i should update")
    endpoint = "http://" + team.client_ip + ":5000/action"
    data = {
        "action": "slk_simple_runner",
        "data": {}
    }
    headers = {"Authorization": "Bearer " + Globals.shclient_key, 'Content-Type': 'application/json'}

    message = session.post(endpoint, data=json.dumps(data), headers=headers)
    result = message.result().json()
    if result['response']['presence'] == "active":
        print("I'm active, and i'm updating client_last_update")
        team.client_last_update = now
def schedule_tasks():
    # get team
    utc_offset = lambda offset: timezone(timedelta(seconds=offset*60*60))
    now = datetime.now()
    
    teams = TeamModel.return_all()
    for team in teams:
        now_offset = now.replace(tzinfo=utc_offset(team.team_timezone_offset))
        current_user = UserModel.find_by_id(team.user)
        print("=============")
        print(team.slug)
        is_team_active = False
        if team.schedule_enabled:
            if team.schedule_active_sunday and now.weekday() == 6:
                is_team_active = True
            if team.schedule_active_monday and now.weekday() == 0:
                is_team_active = True
            if team.schedule_active_tuesday and now.weekday() == 1:
                is_team_active = True
            if team.schedule_active_wednesday and now.weekday() == 2:
                is_team_active = True
            if team.schedule_active_thursday and now.weekday() == 3:
                is_team_active = True
            if team.schedule_active_friday and now.weekday() == 4:
                is_team_active = True
            if team.schedule_active_saturday and now.weekday() == 5:
                is_team_active = True
        else:
            is_team_active = True

        start_time = now.replace(hour=team.schedule_active_start.hour, minute=team.schedule_active_start.minute, tzinfo=utc_offset(team.team_timezone_offset))
        end_time = now.replace(hour=team.schedule_active_end.hour, minute=team.schedule_active_end.minute, tzinfo=utc_offset(team.team_timezone_offset)) - timedelta(minutes=30)
        if end_time < now_offset:
            end_time = end_time + timedelta(days=1)
        if is_team_active:
            if now_offset > start_time and now_offset < end_time:
                is_team_active = True
            else:
                is_team_active = False

        else:
            is_team_active = False
        print(team.schedule_enabled)
        print(start_time)
        print(end_time)
        print(now_offset)
        print(end_time - now_offset)
        print(now_offset - start_time)
        print(now - team.client_last_update >= timedelta(minutes=15))
        print(team.schedule_enabled and end_time - now_offset < timedelta(minutes=2) and end_time - now_offset > timedelta(minutes=-2) and now - team.client_last_update >= timedelta(minutes=2))
        print(team.schedule_enabled and now_offset - start_time < timedelta(minutes=2) and now_offset - start_time > timedelta(minutes=-2) and now - team.client_last_update >= timedelta(minutes=2))
        if is_team_active:
            team.current_presence = True
            if now - team.client_last_update >= timedelta(minutes=15):
                update_team(session, team, now)
            elif team.schedule_enabled and end_time - now_offset < timedelta(minutes=2) and end_time - now_offset > timedelta(minutes=-2) and now - team.client_last_update >= timedelta(minutes=2):
                update_team(session, team, now)
            elif team.schedule_enabled and now_offset - start_time < timedelta(minutes=2) and now_offset - start_time > timedelta(minutes=-2) and now - team.client_last_update >= timedelta(minutes=2):
                update_team(session, team, now)
                
                
                    
                
            else:
                print("its been less than 2 min")
                print(now - team.client_last_update)
        else:
            team.current_presence = False
            
                
            
        # if is_team_active != team.current_presence:
        #     # 
        #     # FIRE TO THE DIGITAL OCEAN TEAM, and get health ping
        #     # 
        #     print("Update the team presence to " + str(is_team_active) + "... and the response will set it.")
        #     team.current_presence = is_team_active
        #     team.client_connected = True
            

        # else:
        #     print("No change to team presence")
        #     # Health Ping to client_ip, update if needed
        #     team.client_connected = True

        

        # BILLING NOW HERE
        # print(team.next_invoice_notice_date < now) 
        if team.next_invoice_date < now:
            # invoice and update
            paidInvoice = {'paid':False}
            try:
                paidInvoice = stripe.Charge.create(
                    amount=Globals.team_stripe_price,
                    currency="usd",
                    source=current_user.stripe_card,
                    description="Team '" + team.slug + "'",
                    customer=current_user.stripe_customer,
                )
            except Exception as e:
                capture_exception(e)
           

            if paidInvoice['paid']:

                next_invoice = now + relativedelta(months=1)
                team.date_updated = now
                team.last_invoice_date = now
                team.next_invoice_date = next_invoice
                team.next_invoice_notice_date = next_invoice.replace(day=(next_invoice.day - Globals.charge_notification_advance))
                team.last_stripe_charge = paidInvoice['id']

                try:
                    email_data = {
                        "from": {
                            "email": Globals.sh_email_address,
                            "name": Globals.sh_email_name
                        },
                        "personalizations": [
                            {
                                "dynamic_template_data": {
                                    "team_slug": team.slug,
                                    'teams_link' : Globals.sh_teams_page
                                },
                                "to": [
                                    {
                                        "email": current_user.username,
                                    }
                                ]
                            }
                        ],
                        "template_id": "d-5983304806e74618aca067aae920f9c5",
                    }
                    rocking = sg.client.mail.send.post(request_body=email_data)    
                except Exception as e:
                    capture_exception(e)

            
            else:
                try:
                    capture_message("Team cannot be captured at this time")
                except Exception as e:
                    capture_exception(e)
            
        
        if team.next_invoice_notice_date < now:
            # send invoice notice
            next_invoice = now + relativedelta(months=1)
            team.next_invoice_notice_date = next_invoice.replace(day=(next_invoice.day - Globals.charge_notification_advance))
            try:
                email_data = {
                    "from": {
                        "email": Globals.sh_email_address,
                        "name": Globals.sh_email_name
                    },
                    "personalizations": [
                        {
                            "dynamic_template_data": {
                                "team_slug": team.slug,
                                'teams_link' : Globals.sh_teams_page
                            },
                            "to": [
                                {
                                    "email": current_user.username,
                                }
                            ]
                        }
                    ],
                    "template_id": "d-4202ce379f294d5ebd4b64d5de20b729",
                }
                rocking = sg.client.mail.send.post(request_body=email_data)    
            except Exception as e:
                capture_exception(e)

        
        # Save team changes
        team.save_to_db()

scheduler = BackgroundScheduler()
# scheduler.add_job(func=schedule_tasks, trigger="interval", minutes=0.1)
scheduler.add_job(func=schedule_tasks, trigger="interval", seconds=Globals.scheduler_frequency_seconds)
scheduler.start()


parser = reqparse.RequestParser()
parser.add_argument('username', help = 'This field cannot be blank', required = True)
parser.add_argument('password', help = 'This field cannot be blank', required = True)

def secure_client(f):
    @wraps(f)
    def check_authorization(*args, **kwargs):
        try:
            client_key = request.headers.get("Authorization").replace('Bearer ', '')
        except Exception as e:
            capture_exception(e)
            resp = jsonify({
                'message': '☠️, Just kidding2'
            })
            resp.status_code = 401
            return resp

        team = TeamModel.find_by_client_key(client_key)
        if team:
            return f(team, *args, **kwargs)
        else:
            resp = jsonify({
                'message': '☠️, Just kidding!'
            })
            resp.status_code = 401
            return resp
        
            
    
    return check_authorization
    
class UserRegistration(Resource):
    def post(self):
        data = parserUserUpdate.parse_args()
        
        if UserModel.find_by_username(data['username']):
            return {'message': 'Email {} cannot be registered at this time'.format(data['username'])}
        try:
            stripe_response = stripe.Customer.create(email=data['username'],)
        except Exception as e:
            capture_exception(e)
            return {'message': 'Message from Stripe - ' + str(e)}
        
        next_invoice = datetime.now()
        invoice_day = next_invoice.day
        

        new_user = UserModel(
            username = data['username'],
            password = UserModel.generate_hash(data['password']),
            stripe_customer = stripe_response.id,
            registration_date = next_invoice,
            terms_of_service = True,
            privacy_policy = True,
        )
        if data['new-cc-number']:
            try:
                token = stripe.Token.create(
                    card={
                        "number": str(data['new-cc-number']),
                        "exp_month": data['new-cc-exp-month'],
                        "exp_year": data['new-cc-exp-year'],
                        "cvc": str(data['new-cc-cvc']),
                    },
                )
            except Exception as e:
                capture_exception(e)
                return {'message': 'Message from Stripe - ' + str(e)}
            try:
                card = stripe.Customer.create_source(
                    new_user.stripe_customer,
                    source=token.id
                )
            except Exception as e:
                capture_exception(e)
                return {'message': 'Message from Stripe - ' + str(e)}
            
            if card:
                new_user.stripe_card = card.id
                new_user.save_to_db()
                try:
                    new_user.save_to_db()
                    
                    access_token = create_access_token(identity = data['username'])
                    refresh_token = create_refresh_token(identity = data['username'])
                    # Set the JWTs and the CSRF double submit protection cookies
                    # in this response
                    resp = jsonify({
                        'login': True,
                        'csrf_access_token': get_csrf_token(access_token),
                        'csrf_refresh_token': get_csrf_token(refresh_token)
                    })
                    set_access_cookies(resp, access_token)
                    set_refresh_cookies(resp, refresh_token)
                    resp.status_code = 200
                    return resp
                except Exception as e:
                    capture_exception(e)
                    return {'message': 'Something went wrong'}, 500
            else:
                return {'message': "There's an issue with your card details, please review and try again. If you continue to see this error, please email us."}, 200
        else:
            return {'message': 'Please enter a CC number'}, 200


# By default, the CRSF cookies will be called csrf_access_token and
# csrf_refresh_token, and in protected endpoints we will look for the
# CSRF token in the 'X-CSRF-TOKEN' header. You can modify all of these
# with various app.config options. Check the options page for details.


# With JWT_COOKIE_CSRF_PROTECT set to True, set_access_cookies() and
# set_refresh_cookies() will now also set the non-httponly CSRF cookies
# as well

forgot_parser = reqparse.RequestParser()
forgot_parser.add_argument('username', help = 'This field cannot be blank', required = True)

class UserForgot(Resource):
    def post(self):
        data = forgot_parser.parse_args()
        current_user = UserModel.find_by_username(data['username'])

        if not current_user:
            return {'message': 'Credentials for {} are incorrect, please try again'.format(data['username'])}
        
        # Generate code and date and save
        now = datetime.now()
        current_user.password_reset_date = now
        current_user.password_reset_code = create_access_token(identity = data['username']+str(now))[0:255]
        
        reset_link = Globals.sh_reset_url + current_user.password_reset_code 

        current_user.save_to_db()
        try:
            email_data = {
                "from": {
                    "email": Globals.sh_email_address,
                    "name": Globals.sh_email_name
                },
                "personalizations": [
                    {
                        "dynamic_template_data": {
                            "reset_link": reset_link,
                            
                        },
                        "to": [
                            {
                                "email": current_user.username,
                            }
                        ]
                    }
                ],
                "template_id": "d-5ad5208d6e074e3fa0d5e9023a05e058",
            }
            rocking = sg.client.mail.send.post(request_body=email_data)    
        except Exception as e:
            capture_exception(e)
        
        return {'message': 'Something went wrong', 'success' : True}, 200

reset_parser = reqparse.RequestParser()
reset_parser.add_argument('password', help = 'This field cannot be blank', required = True)
reset_parser.add_argument('reset_code', help = 'This field cannot be blank', required = True)
class UserPasswordReset(Resource):
    def post(self):
        data = reset_parser.parse_args()  
        
        current_user = UserModel.find_by_reset_code(data['reset_code'])
        if((datetime.now() - current_user.password_reset_date).total_seconds() < 600):
            current_user.password = UserModel.generate_hash(data['password'])
            message = "Password updated"
            try:
                email_data = {
                    "from": {
                        "email": Globals.sh_email_address,
                        "name": Globals.sh_email_name
                    },
                    "personalizations": [
                        {
                            "dynamic_template_data": {
                                "account_element": "password",
                                
                            },
                            "to": [
                                {
                                    "email": current_user.username,
                                }
                            ]
                        }
                    ],
                    "template_id": "d-0e44317815b94117aa42819de325f612",
                }
                rocking = sg.client.mail.send.post(request_body=email_data)    
            except Exception as e:
                capture_exception(e)
            current_user.save_to_db()
            return {'message': 'Win!', 'success': True}, 200
        else:
            return {'message': 'This reset code has expired, please get a new reset code', 'success': False}, 200
class UserLogin(Resource):
    def post(self):
        data = parser.parse_args()
        current_user = UserModel.find_by_username(data['username'])

        if not current_user:
            return {'message': 'Credentials for {} are incorrect, please try again'.format(data['username'])}
        
        if UserModel.verify_hash(data['password'], current_user.password):
            access_token = create_access_token(identity = data['username'])
            refresh_token = create_refresh_token(identity = data['username'])
            # Set the JWTs and the CSRF double submit protection cookies
            # in this response
            resp = jsonify({
                'login': True,
                'csrf_access_token': get_csrf_token(access_token),
                'csrf_refresh_token': get_csrf_token(refresh_token)
            })
            set_access_cookies(resp, access_token)
            set_refresh_cookies(resp, refresh_token)
            resp.status_code = 200
            return resp
        else:
            return {'message': 'Credentials for {} are incorrect, please try again'.format(data['username'])}



class UserLogoutAccess(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti = jti)
            revoked_token.add()
            resp = jsonify({'logout': True,'message': 'Access token has been revoked'})
            resp.status_code = 200
            return resp
        except Exception as e:
            capture_exception(e)
            return {'message': 'Something went wrong'}, 500


class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti = jti)
            revoked_token.add()
            resp = jsonify({'logout': True,'message': 'Refresh token has been revoked'})
            unset_jwt_cookies(resp)
            resp.status_code = 200
            return resp
        except Exception as e:
            capture_exception(e)
            return {'message': 'Something went wrong'}, 500

# Because the JWTs are stored in an httponly cookie now, we cannot
# log the user out by simply deleting the cookie in the frontend.
# We need the backend to send us a response to delete the cookies
# in order to logout. unset_jwt_cookies is a helper function to
# do just that.
class UserLogoutCookie(Resource):
    @jwt_required
    def post(self):
        resp = jsonify({'logout': True})
        unset_jwt_cookies(resp)
        resp.status_code = 200
        return resp

class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity = current_user)
        # Set the access JWT and CSRF double submit protection cookies
        # in this response
        resp = jsonify({'refresh': True})
        set_access_cookies(resp, access_token)
        resp.status_code = 200
        return resp


# class AllUsers(Resource):
#     def get(self):
#         return UserModel.return_all()
    
#     def delete(self):
#         return UserModel.delete_all()


class SecretResource(Resource):
    @jwt_required
    def get(self):
        return {
            'answer': 423
        }

parserUserUpdate = reqparse.RequestParser()
parserUserUpdate.add_argument('username', help = 'This field cannot be blank', required = True)
parserUserUpdate.add_argument('password', help = 'This field cannot be blank', required = True)
parserUserUpdate.add_argument('new-password', required = False)
parserUserUpdate.add_argument('new-password-copy', required = False)
parserUserUpdate.add_argument('new-email', required = False)
parserUserUpdate.add_argument('new-cc-number', required = False)
parserUserUpdate.add_argument('new-cc-exp-month', type=int, required = False)
parserUserUpdate.add_argument('new-cc-exp-year', type=int, required = False)
parserUserUpdate.add_argument('new-cc-cvc', required = False)
parserUserUpdate.add_argument('new-billing-date', type=int, required = False)

class CurrentUser(Resource):
    @jwt_required
    def delete(self):
        identity = get_jwt_identity()
        current_user = UserModel.find_by_username(identity)
        teams = TeamModel.find_teams_for_user_raw(current_user.id)
        now = datetime.now()
        deleted_teams = []
        teams_list = []
        for team in teams:
            days_left = team.next_invoice_date - now
            days_total = team.next_invoice_date - team.last_invoice_date
            days_remain = days_total.days - days_left.days
            days_ratio = days_left.days/days_total.days
            stripe_fee = Globals.team_price * 0.029 + 0.30
            amount_to_refund = (Globals.team_price * days_ratio) - stripe_fee 
            if team.client_id:
                try:
                    manager = digitalocean.Manager(token=Globals.do_token)
                    droplet = manager.get_droplet(team.client_id)
                    thisResult = droplet.destroy()
                except Exception as e:    
                    capture_exception(e)
            try: 
                refund = stripe.Refund.create(
                    charge=team.last_stripe_charge,
                    amount=int(amount_to_refund * 100),
                )
                team_charge = stripe.Charge.retrieve(
                    team.last_stripe_charge,
                    )
            except Exception as e:
                capture_exception(e)
                return {'message': 'Message from Stripe - ' + str(e)}
            # amount_to_refund_string = "${0:.2f}".format(amount_to_refund)
            teams_list.append({"slug":team.slug, "refunded_amount":"${0:.2f}".format(amount_to_refund), "receipt_url": team_charge['receipt_url']})
            team.delete_from_db()
            deleted_teams.append({"slug":team.slug, "refunded_amount":amount_to_refund, "days_remain":days_remain})
        current_user.delete_from_db()
        print(current_user)
        print(deleted_teams)
        try:
            email_data = {
                "from": {
                    "email": Globals.sh_email_address,
                    "name": Globals.sh_email_name
                },
                "personalizations": [
                    {
                        "dynamic_template_data": {
                            "teams_list": teams_list,
                            
                        },
                        "to": [
                            {
                                "email": current_user.username,
                            }
                        ]
                    }
                ],
                "template_id": "d-c658ac52108b472c855b0392ace7f3aa",
            }
            rocking = sg.client.mail.send.post(request_body=email_data)    
        except Exception as e:
            capture_exception(e)
        resp = jsonify({'success': True})
        resp.status_code = 200
        return resp

    @jwt_required
    def get(self):
        identity = get_jwt_identity()
        current_user = UserModel.get_account_info(identity)
        return current_user

    @jwt_required
    def post(self):
        data = parserUserUpdate.parse_args()
        current_user = UserModel.find_by_username(data['username'])

        if not current_user:
            return {'message': 'Credentials for {} are incorrect, please try again'.format(data['username'])}
            # return {'message': 'User {} doesn\'t exist'.format(data['username'])}
        
        if UserModel.verify_hash(data['password'], current_user.password):
            # Set the JWTs and the CSRF double submit protection cookies
            # in this response
            message = "nothing changed"

            if data['new-cc-number']:
                try:
                    token = stripe.Token.create(
                        card={
                            "number": str(data['new-cc-number']),
                            "exp_month": data['new-cc-exp-month'],
                            "exp_year": data['new-cc-exp-year'],
                            "cvc": str(data['new-cc-cvc']),
                        },
                    )
                except Exception as e:
                    capture_exception(e)
                    return {'message': 'Message from Stripe - ' + str(e)}
                try:
                    card = stripe.Customer.create_source(
                        current_user.stripe_customer,
                        source=token.id
                    )
                except Exception as e:
                    capture_exception(e)
                    return {'message': 'Message from Stripe - ' + str(e)}
                current_user.stripe_card = card.id
                message = "Card updated"
                try:
                    email_data = {
                        "from": {
                            "email": Globals.sh_email_address,
                            "name": Globals.sh_email_name
                        },
                        "personalizations": [
                            {
                                "dynamic_template_data": {
                                    "account_element": "payment method",
                                    
                                },
                                "to": [
                                    {
                                        "email": current_user.username,
                                    }
                                ]
                            }
                        ],
                        "template_id": "d-0e44317815b94117aa42819de325f612",
                    }
                    rocking = sg.client.mail.send.post(request_body=email_data)    
                except Exception as e:
                    capture_exception(e)
                current_user.save_to_db()

            if data['new-password']:
                if data['new-password'] == data['new-password-copy']:
                    current_user.password = UserModel.generate_hash(data['new-password'])
                    message = "Password updated"
                    try:
                        email_data = {
                            "from": {
                                "email": Globals.sh_email_address,
                                "name": Globals.sh_email_name
                            },
                            "personalizations": [
                                {
                                    "dynamic_template_data": {
                                        "account_element": "password",
                                        
                                    },
                                    "to": [
                                        {
                                            "email": current_user.username,
                                        }
                                    ]
                                }
                            ],
                            "template_id": "d-0e44317815b94117aa42819de325f612",
                        }
                        rocking = sg.client.mail.send.post(request_body=email_data)    
                    except Exception as e:
                        capture_exception(e)
                    current_user.save_to_db()
            
            if data['new-email']:
                current_user.username = data['new-email']
                old_email = current_user.username
                message = "Email updated"
                try:
                    email_data = {
                        "from": {
                            "email": Globals.sh_email_address,
                            "name": Globals.sh_email_name
                        },
                        "personalizations": [
                            {
                                "dynamic_template_data": {
                                    "account_element": "email address (and username)",
                                    
                                },
                                "to": [
                                    {
                                        "email": old_email,
                                    }
                                ]
                            }
                        ],
                        "template_id": "d-0e44317815b94117aa42819de325f612",
                    }
                    rocking = sg.client.mail.send.post(request_body=email_data)    
                except Exception as e:
                    capture_exception(e)
                
                try:
                    email_data = {
                        "from": {
                            "email": Globals.sh_email_address,
                            "name": Globals.sh_email_name
                        },
                        "personalizations": [
                            {
                                "dynamic_template_data": {
                                    "account_element": "email address (and username)",
                                    
                                },
                                "to": [
                                    {
                                        "email": current_user.username,
                                    }
                                ]
                            }
                        ],
                        "template_id": "d-0e44317815b94117aa42819de325f612",
                    }
                    rocking = sg.client.mail.send.post(request_body=email_data)    
                except Exception as e:
                    capture_exception(e)
                current_user.save_to_db()
                access_token = create_access_token(identity = data['new-email'])
                refresh_token = create_refresh_token(identity = data['new-email'])
                

                resp = jsonify({
                    'success': True,
                    'message': message, 
                    'csrf_access_token': get_csrf_token(access_token),
                    'csrf_refresh_token': get_csrf_token(refresh_token)
                })
                set_access_cookies(resp, access_token)
                set_refresh_cookies(resp, refresh_token)
                resp.status_code = 200
                return resp            
            
            resp = jsonify({
                'success': True,
                'message': message, 
            })
            resp.status_code = 200
            return resp
        else:
            if data['new-password']:
                return {'message': 'The current credentials are incorrect, please try again'}
            else:
                return {'message': 'Credentials for {} are incorrect, please try again'.format(data['username'])}

scheduleParser = reqparse.RequestParser()
scheduleParser.add_argument('time', help = 'This field cannot be blank', required = True)
scheduleParser.add_argument('text', help = 'This field cannot be blank', required = True)




class ScheduleResource(Resource):
    def post(self):
        def printing_something(text):
            print("printing %s at %s" % (text, datetime.now()))
        data = scheduleParser.parse_args()
        #get time to schedule and text to print from the json
        print(data)
        text = data['text']
        time = data['time']
        #convert to datetime
        date_time = datetime.strptime(str(time), '%Y-%m-%dT%H:%M')
        #schedule the method 'printing_something' to run the the given 'date_time' with the args 'text'
        job = scheduler.add_job(printing_something, trigger='date', next_run_time=str(date_time),
                                args=[text])
        return "job details: %s" % job
    
shclientResponseParser = reqparse.RequestParser()
shclientResponseParser.add_argument('client_current_presence', required = True)
shclientResponseParser.add_argument('client_current_status', required = True)
shclientResponseParser.add_argument('client_current_status_emoji', required = True)
shclientResponseParser.add_argument('client_current_notifications_type', required = True)
shclientResponseParser.add_argument('client_current_notifications_keywords', required = True)
shclientResponseParser.add_argument('client_team_timezone_offset', required = True)
shclientResponseParser.add_argument('client_team_timezone_text', required = True)
shclientResponseParser.add_argument('client_do_not_disturb_enabled', required = True)
shclientResponseParser.add_argument('client_do_not_disturb_start', required = True)
shclientResponseParser.add_argument('client_do_not_disturb_start_string', required = True)
shclientResponseParser.add_argument('client_do_not_disturb_end', required = True)
shclientResponseParser.add_argument('client_do_not_disturb_end_string', required = True)
shclientResponseParser.add_argument('update', required = True)

class ShclientResponse(Resource):
    @secure_client
    def get(team, self):
        team2 = TeamModel.find_by_id(team.id)
        return team2

    @secure_client
    def post(team, self):
        data = shclientResponseParser.parse_args()
        print(data)
        print(request.remote_addr)
        print(team.id)
        updateRequested = False
        if data['update'] == "True":
            updateRequested = True
        
        if updateRequested:
            client_current_presence = False
            if  data['client_current_presence'] == "True":
                client_current_presence = True

            team.current_presence = client_current_presence
            team.current_status = data['client_current_status']
            team.current_status_emoji = data['client_current_status_emoji']
            team.current_notifications_type = data['client_current_notifications_type']
            team.current_notifications_keywords = data['client_current_notifications_keywords']
            team.team_timezone_offset = round(float(data['client_team_timezone_offset']),1)

            client_do_not_disturb_enabled = False
            if  data['client_do_not_disturb_enabled'] == "True":
                client_do_not_disturb_enabled = True
            team.do_not_disturb_enabled = client_do_not_disturb_enabled

            team.do_not_disturb_start = int(data['client_do_not_disturb_start'])
            team.do_not_disturb_end = int(data['client_do_not_disturb_end'])
            team.date_updated = datetime.utcnow()
            team.save_to_db()

        new_response = ShclientResponses(
            team = team.id,
            address = request.remote_addr, 
            updateRequested = updateRequested,
            data = jsonify(data).get_data(as_text=True), 
            created = datetime.utcnow()
        )

        
        try:
            new_response.add()
            
            resp = jsonify({
                'success': True,
                'response': team.id,
            })
            resp.status_code = 200
            return resp
        except Exception as e:
            capture_exception(e)
            return {'message': 'Something went wrong'}, 500


teamParser = reqparse.RequestParser()
teamParser.add_argument('slug', help = 'This field cannot be blank', required = True)
teamParser.add_argument('teamTimezone', type=float, required = False)
teamParser.add_argument('setTimezone', required = False)
teamParser.add_argument('currentStatus', required = False)
teamParser.add_argument('currentNotificationsKeywords', required = False)
teamParser.add_argument('new_slug', required = False)
teamParser.add_argument('username', required = False)
teamParser.add_argument('password', required = False)
teamParser.add_argument('team_is_deleted', type=bool, required = False)
teamParser.add_argument('currentNotificationsType', required = False)
teamParser.add_argument('presence', required = False)
teamParser.add_argument('statusActive', type=bool, required = False)
teamParser.add_argument('notificationsActive', type=bool, required = False)
teamParser.add_argument('presenceActive', type=bool, required = False)
teamParser.add_argument('scheduleActive', type=bool, required = False)
teamParser.add_argument('doNotDisturbActive', type=bool, required = False)
teamParser.add_argument('doNotDisturbStart', type=int, required = False)
teamParser.add_argument('doNotDisturbEnd', type=int, required = False)
teamParser.add_argument('setBool', required = False)
teamParser.add_argument('scheduleActiveStatusEnabled', type=bool, required = False)
teamParser.add_argument('scheduleActiveNotificationsEnabled', type=bool, required = False)
teamParser.add_argument('scheduleInactiveStatusEnabled', type=bool, required = False)
teamParser.add_argument('scheduleInactiveNotificationsEnabled', type=bool, required = False)
teamParser.add_argument('scheduleActiveStatus', required = False)
teamParser.add_argument('scheduleInactiveStatus', required = False)
teamParser.add_argument('scheduleActiveNotificationsKeywords', required = False)
teamParser.add_argument('scheduleInactiveNotificationsKeywords', required = False)
teamParser.add_argument('scheduleActiveNotificationsType', required = False)
teamParser.add_argument('scheduleInactiveNotificationsType', required = False)
teamParser.add_argument('scheduleActiveSunday', type=bool, required = False)
teamParser.add_argument('scheduleActiveMonday', type=bool, required = False)
teamParser.add_argument('scheduleActiveTuesday', type=bool, required = False)
teamParser.add_argument('scheduleActiveWednesday', type=bool, required = False)
teamParser.add_argument('scheduleActiveThursday', type=bool, required = False)
teamParser.add_argument('scheduleActiveFriday', type=bool, required = False)
teamParser.add_argument('scheduleActiveSaturday', type=bool, required = False)
teamParser.add_argument('scheduleActiveStart', type=lambda x: datetime.strptime(x,'%Y-%m-%dT%H:%M:%S.%fZ'), required = False)
teamParser.add_argument('scheduleActiveEnd', type=lambda x: datetime.strptime(x,'%Y-%m-%dT%H:%M:%S.%fZ'), required = False)
teamParser.add_argument('exceptionNew', type=bool, required = False)
teamParser.add_argument('exceptionUpdate', type=int, required = False)
teamParser.add_argument('exceptionDelete', type=int, required = False)
teamParser.add_argument('exceptionStartDate', type=lambda x: datetime.strptime(x,'%Y-%m-%dT%H:%M:%S.%fZ'), required = False)
teamParser.add_argument('exceptionEndDate', type=lambda x: datetime.strptime(x,'%Y-%m-%dT%H:%M:%S.%fZ'), required = False)

def check_new_team_status_and_update(team, current_user):
    if team.client_id:
        if team.client_status == "in-progress":
            manager = digitalocean.Manager(token=Globals.do_token)
            try:
                droplet = manager.get_droplet(team.client_id)
                actions = droplet.get_actions()
                for action in actions:
                    action.load()
                    # Once it shows complete, droplet is up and running
                    team.client_status = action.status
                if team.client_status == "completed":
                    team.client_ip = droplet.networks['v4'][0]['ip_address']
                   
            except Exception as e:
                team.client_status = "none"
                capture_exception(e)
                try:
                    email_data = {
                            "from": {
                                "email": Globals.sh_email_address,
                                "name": Globals.sh_email_name
                            },
                            "personalizations": [
                                {
                                    "dynamic_template_data": {
                                        "title": "Team does not have a droplet",
                                        'message': team.id + " - " + team.slug + " has no droplet (i.e. `client_id` == none)", 
                                    },
                                    "to": [
                                        {
                                            "email": Globals.sh_email_address,
                                        }
                                    ]
                                }
                            ],
                            "template_id": "d-771d07e5ffc34737a4eb275eb12d3e1f",
                        }
                    rocking = sg.client.mail.send.post(request_body=email_data)    
                except Exception as e:
                    capture_exception(e)
                
            
                # IF LOGIN SUCCESSFUL! Charge team

                # IF LOGIN UNSUCCESSFUL or Charge Unsucsessful! Mark team droplet for deletion in 48 hrs
                
            # if the team is now completed, send welcome email with ip and shit
        elif team.client_status == "completed":
            if team.team_client_status != "logged-in" and team.client_ip:
                # save team IP
                try:
                    f = Fernet(Globals.fernet_key)
                    endpoint = "http://" + team.client_ip + ":5000/action"
                    data = {
                        "action": "slk_login",
                        "data": {
                            "url": "https://" + team.slug + ".slack.com/",
                            "email": team.team_credentials_username,
                            "password": f.decrypt(team.team_credentials_password.encode('utf-8')).decode("utf-8")
                        }
                    }
                    headers = {"Authorization": "Bearer " + Globals.shclient_key, 'Content-Type': 'application/json'}

                    message = requests.post(endpoint, data=json.dumps(data), headers=headers).json()
                    print(message['response'])
                    if message['response'] == "logged=in" or message['response'] == "logged-in-again":
                        print("AYYY")
                        team.team_client_status = "logged-in"
                        try:
                            email_data = {
                                "from": {
                                    "email": Globals.sh_email_address,
                                    "name": Globals.sh_email_name
                                },
                                "personalizations": [
                                    {
                                        "dynamic_template_data": {
                                            "team_slug": team.slug,
                                            'ip_address': team.client_ip,
                                            'teams_link' : Globals.sh_teams_page
                                        },
                                        "to": [
                                            {
                                                "email": current_user.username,
                                            }
                                        ]
                                    }
                                ],
                                "template_id": "d-24f446a976c04494aecaf3165e808614",
                            }
                            rocking = sg.client.mail.send.post(request_body=email_data)    
                        except Exception as e:
                            capture_exception(e)
                    else:
                        # THIS IS WHERE WRONG CREDENTIALS GO... AND WHERE 2FA likely goes
                        team.team_client_status = "none"
                except Exception as e:
                    print(e)
                    team.team_client_status = "none"
            else:
                try:
                    droplet = manager.get_droplet(team.client_id)
                    actions = droplet.get_actions()
                    for action in actions:
                        action.load()
                        # Once it shows complete, droplet is up and running
                        team.client_status = action.status
                    team.client_ip = droplet.networks['v4'][0]['ip_address']
                    
                except Exception as e:
                    capture_exception(e)
        else:
            if team.client_status != "errored":
                try:
                    email_data = {
                            "from": {
                                "email": Globals.sh_email_address,
                                "name": Globals.sh_email_name
                            },
                            "personalizations": [
                                {
                                    "dynamic_template_data": {
                                        "title": "Team droplet creation error",
                                        'message': team.id + " - " + team.slug + " errored on droplet creation. Droplet ID is " + team.client_id, 
                                    },
                                    "to": [
                                        {
                                            "email": Globals.sh_email_address,
                                        }
                                    ]
                                }
                            ],
                            "template_id": "d-771d07e5ffc34737a4eb275eb12d3e1f",
                        }
                    rocking = sg.client.mail.send.post(request_body=email_data)    
                except Exception as e:
                    capture_exception(e)
            team.client_status = "errored"
            # send error to info@statushammer
    else:
        if team.client_status != "none":
            try:
                email_data = {
                        "from": {
                            "email": Globals.sh_email_address,
                            "name": Globals.sh_email_name
                        },
                        "personalizations": [
                            {
                                "dynamic_template_data": {
                                    "title": "Team does not have a droplet",
                                    'message': team.id + " - " + team.slug + " has no droplet (i.e. `client_id` == none)", 
                                },
                                "to": [
                                    {
                                        "email": Globals.sh_email_address,
                                    }
                                ]
                            }
                        ],
                        "template_id": "d-771d07e5ffc34737a4eb275eb12d3e1f",
                    }
                rocking = sg.client.mail.send.post(request_body=email_data)    
            except Exception as e:
                capture_exception(e)
        team.client_status = "none"
    team.save_to_db()
    return team
class TeamSingle(Resource):
    @jwt_required
    def get(self, team_slug):
        print(team_slug)
        identity = get_jwt_identity()
        current_user = UserModel.find_by_username(identity)
        team = TeamModel.find_by_slug_and_user_for_update(team_slug, current_user.id)
        check_new_team_status_and_update(team, current_user)
        team = TeamModel.find_by_slug_and_user(team_slug, current_user.id)
        return team
    
    @jwt_required
    def delete(self, team_slug):
        identity = get_jwt_identity()
        current_user = UserModel.find_by_username(identity)

        # GEt the current values... if a value is set, replace it, otherwise use default
        team = TeamModel.find_by_slug_and_user_for_update(team_slug, current_user.id)
        now = datetime.now()
        days_left = team.next_invoice_date - now
        days_total = team.next_invoice_date - team.last_invoice_date
        days_remain = days_total.days - days_left.days
        days_ratio = days_left.days/days_total.days
        stripe_fee = Globals.team_price * 0.029 + 0.30
        amount_to_refund = (Globals.team_price * days_ratio) - stripe_fee 
        if team.client_id:
            try:
                manager = digitalocean.Manager(token=Globals.do_token)
                droplet = manager.get_droplet(team.client_id)
                thisResult = droplet.destroy()
            except Exception as e:    
                capture_exception(e)

        try:
            refund = stripe.Refund.create(
                charge=team.last_stripe_charge,
                amount=int(amount_to_refund * 100),
            )
            team_charge = stripe.Charge.retrieve(
                    team.last_stripe_charge,
                    )
        except Exception as e:
            capture_exception(e)
            return {'message': 'Message from Stripe - ' + str(e)}        
        
        team.delete_from_db()
        try:
            email_data = {
                    "from": {
                        "email": Globals.sh_email_address,
                        "name": Globals.sh_email_name
                    },
                    "personalizations": [
                        {
                            "dynamic_template_data": {
                                "team_slug": team.slug,
                                'refund_amount': "${0:.2f}".format(amount_to_refund),
                                'receipt_url':team_charge['receipt_url']
                            },
                            "to": [
                                {
                                    "email": current_user.username,
                                }
                            ]
                        }
                    ],
                    "template_id": "d-7eec53155f3c4978ab05b939f93fa0b7",
                }
            rocking = sg.client.mail.send.post(request_body=email_data)    
        except Exception as e:
            capture_exception(e)
        resp = jsonify({'success': True, 'slug': team_slug})
        resp.status_code = 200
        return resp

    @jwt_required
    def post(self,team_slug):
        data = teamParser.parse_args()
        identity = get_jwt_identity()
        current_user = UserModel.find_by_username(identity)
        # GEt the current values... if a value is set, replace it, otherwise use default
        team = TeamModel.find_by_slug_and_user_for_update(team_slug, current_user.id)
        print(team)
        if data['new_slug']:
            team.slug = data['new_slug']
            team_slug = data['new_slug']

        if data['currentStatus']:
            team.current_status = data['currentStatus']
        
        if data['scheduleActiveStatus']:
            team.schedule_active_status = data['scheduleActiveStatus']
        if data['scheduleInactiveStatus']:
            team.schedule_inactive_status = data['scheduleInactiveStatus']
        
        if data['scheduleActiveNotificationsKeywords']:
            team.schedule_active_notifications_keywords = data['scheduleActiveNotificationsKeywords']
        if data['scheduleInactiveNotificationsKeywords']:
            team.schedule_inactive_notifications_keywords = data['scheduleInactiveNotificationsKeywords']


        if data['currentNotificationsType']:
            team.current_notifications_type = data['currentNotificationsType']
        if data['currentNotificationsKeywords']:
            team.current_notifications_keywords = data['currentNotificationsKeywords']

        if data['scheduleActiveNotificationsType']:
            team.schedule_active_notifications_type = data['scheduleActiveNotificationsType']
        if data['scheduleInactiveNotificationsType']:
            team.schedule_inactive_notifications_type = data['scheduleInactiveNotificationsType']

        
        if data['scheduleActiveStart']:
            team.schedule_active_start = data['scheduleActiveStart']
        if data['scheduleActiveEnd']:
            team.schedule_active_end = data['scheduleActiveEnd']

        if data['doNotDisturbStart']:
            team.do_not_disturb_start = data['doNotDisturbStart']
        if data['doNotDisturbEnd']:
            team.do_not_disturb_end = data['doNotDisturbEnd']

        if data['presence']:
            team.presence = data['presence']        
        
        if data['setTimezone']:
            if data['setTimezone'] == "teamTimezone":
                team.team_timezone_offset = data[data['setTimezone']]

        if data['setBool']:
            if data['setBool'] == "presenceActive":
                team.current_presence = data[data['setBool']]
            elif data['setBool'] == "notificationsActive":
                team.notificationsActive = data[data['setBool']]
            elif data['setBool'] == "statusActive":
                team.statusActive = data[data['setBool']]
            elif data['setBool'] == "scheduleActive":
                team.schedule_enabled = data[data['setBool']]
            elif data['setBool'] == "doNotDisturbActive":
                team.do_not_disturb_enabled = data[data['setBool']]
            elif data['setBool'] == "scheduleActiveStatusEnabled":
                team.schedule_active_status_enabled = data[data['setBool']]
            elif data['setBool'] == "scheduleActiveNotificationsEnabled":
                team.schedule_active_notifications_enabled = data[data['setBool']]
            elif data['setBool'] == "scheduleInactiveStatusEnabled":
                team.schedule_inactive_status_enabled = data[data['setBool']]
            elif data['setBool'] == "scheduleInactiveNotificationsEnabled":
                team.schedule_inactive_notifications_enabled = data[data['setBool']]
            elif data['setBool'] == 'scheduleActiveSunday':
                team.schedule_active_sunday = data[data['setBool']]
            elif data['setBool'] == 'scheduleActiveMonday':
                team.schedule_active_monday = data[data['setBool']]
            elif data['setBool'] == 'scheduleActiveTuesday':
                team.schedule_active_tuesday = data[data['setBool']]
            elif data['setBool'] == 'scheduleActiveWednesday':
                team.schedule_active_wednesday = data[data['setBool']]
            elif data['setBool'] == 'scheduleActiveThursday':
                team.schedule_active_thursday = data[data['setBool']]
            elif data['setBool'] == 'scheduleActiveFriday':
                team.schedule_active_friday = data[data['setBool']]
            elif data['setBool'] == 'scheduleActiveSaturday':
                team.schedule_active_saturday = data[data['setBool']]
            elif data['setBool'] == 'team_is_deleted':
                team.team_is_deleted = False


        if data['exceptionNew'] or data['exceptionDelete'] or data['exceptionUpdate']:
            if data['exceptionUpdate']:
                exception = ExceptionModel.find_by_id(current_user.id, team.id, data['exceptionUpdate'])
                exception.start_date = data['exceptionStartDate']
                exception.end_date = data['exceptionEndDate']
                exception.save_to_db()

            elif data['exceptionDelete']:
                exception = ExceptionModel.find_by_id(current_user.id, team.id, data['exceptionDelete'])
                exception.delete_from_db()
            elif data['exceptionNew']:
                exception = ExceptionModel(
                    user = current_user.id,
                    team = team.id,
                    start_date = data['exceptionStartDate'],
                    end_date =data['exceptionEndDate'] ,
                )
                exception.save_to_db()
            
        else:
            team.date_updated = datetime.utcnow()
            team.save_to_db()
        teamed = TeamModel.find_by_slug_and_user(team_slug, current_user.id)
        # This is where I create a client
        # This is where I send the email
        

        resp = jsonify({'success': True, 'team': teamed, 'data': data})
        resp.status_code = 200
        return resp


        

class AllTeams(Resource):
    @jwt_required
    def get(self):
        identity = get_jwt_identity()
        current_user = UserModel.find_by_username(identity)
        teams = TeamModel.find_teams_for_user_raw(current_user.id)
        for team in teams:
            check_new_team_status_and_update(team, current_user)
        teams = TeamModel.find_teams_for_user(current_user.id)
        return teams

class AddTeamData(Resource):
    @jwt_required
    def get(self):
        identity = get_jwt_identity()
        current_user = UserModel.find_by_username(identity)
        text = "Your cost today will be $10"
        # I think all we need to remember is the last full invoice, and how many active teams...

        # if active teams == 0, then just reset invoice date
        # if active teams > 0, invoice date not set, then just reset it
        # else, go with last invoice date and prorrate
        
        # can delete "next invoice date" field, but we will need to calculate it for the account page
        addTeamData = {"text": text}
        
        
        return addTeamData

class AddTeam(Resource):
    @jwt_required
    def post(self):
        data = teamParser.parse_args()
        print(data)
        now = datetime.now()
        identity = get_jwt_identity()
        current_user = UserModel.find_by_username(identity)
        # if UserModel.find_by_username(data['username']):
        #     return {'message': 'User {} already exists'.format(data['username'])}
        print(current_user.id)
        
        new_team_slug = data['slug'].lower()
        
        print("spawn the machine, and make the status hammer interface wait")
        
        # USE THIS!
        # https://github.com/koalalorenzo/python-digitalocean
        droplet = digitalocean.Droplet(token=Globals.do_token,
                               name=new_team_slug + "-shclient-" + now.strftime("%Y%d%m-%H%M"),
                               region='nyc1', # New York 2
                               image=Globals.do_image, 
                               size_slug='s-1vcpu-1gb', 
                               ssh_keys=Globals.do_ssh_keys,
                               backups=True,
                               tags = Globals.do_tags,
                               user_data = "runcmd: - cd ~/../home/shclient/shclient - rm data.json"
                               )
        droplet.create()
        
        print(droplet.id)
        print("charge the user")
        print("save the data")
        

        paidInvoice = {'paid':False}
        messageFromStripe = 'Message from Stripe -  "'
        try:
            paidInvoice = stripe.Charge.create(
                amount=Globals.team_stripe_price,
                currency="usd",
                source=current_user.stripe_card,
                description="Team '" + new_team_slug + "'",
                customer=current_user.stripe_customer,
            )
        except Exception as e:
            capture_exception(e)
            messageFromStripe = messageFromStripe + str(e) + '" <a href="mailto:' + Globals.sh_email_address + '?subject=Message from Stripe - '+ str(e) + '" style="text-decoration:underline;">Email support with this error now</a>'

        if paidInvoice['paid']:

            if TeamModel.find_by_slug_and_username(new_team_slug, data['username']) is None:
                
                next_invoice = now + relativedelta(months=1)
                f = Fernet(Globals.fernet_key)
                token = f.encrypt(data['password'].encode("utf-8")).decode("utf-8")
                new_team = TeamModel(
                    slug = new_team_slug,
                    user = current_user.id,
                    uuid = uuid.uuid4().hex[:6].lower(),
                    date_created = now,
                    date_updated = now,
                    team_client_status = "none",
                    team_credentials_username = data['username'],
                    team_credentials_password = token,
                    team_timezone_offset = data['teamTimezone'],
                    team_is_deleted = False,
                    team_deleted_date = now,
                    current_presence = True,
                    current_status = None,
                    current_notifications_type = NotificationType.NONE,
                    current_notifications_keywords = None,
                    do_not_disturb_enabled = False,
                    do_not_disturb_start = 0,
                    do_not_disturb_end = 47,
                    schedule_enabled = False,
                    schedule_active_sunday = False,
                    schedule_active_monday = True,
                    schedule_active_tuesday = True,
                    schedule_active_wednesday = True,
                    schedule_active_thursday = True,
                    schedule_active_friday = True,
                    schedule_active_saturday = False,
                    schedule_active_start = now.replace(hour=9, minute=0),
                    schedule_active_end = now.replace(hour=17, minute=0),
                    schedule_active_status_enabled = False,
                    schedule_active_status = None,
                    schedule_active_notifications_enabled = False,
                    schedule_active_notifications_type = NotificationType.NONE,
                    schedule_active_notifications_keywords = None,
                    schedule_inactive_status_enabled = False,
                    schedule_inactive_status = None,
                    schedule_inactive_notifications_enabled = False,
                    schedule_inactive_notifications_type = NotificationType.NONE,
                    schedule_inactive_notifications_keywords = None,
                    last_invoice_date = now,
                    next_invoice_date = next_invoice,
                    next_invoice_notice_date = next_invoice.replace(day=(next_invoice.day - Globals.charge_notification_advance)),
                    last_stripe_charge = paidInvoice['id'],
                    client_id = droplet.id,
                    client_status = "in-progress",
                    client_last_update = now,
                    # last_stripe_invoice = paidInvoice['id']
                )
                print("Raw - " + data['password'])
                
                print("Decrypt - " + f.decrypt(token.encode('utf-8')).decode("utf-8"))
                print(Globals.sh_email_address)
                print(Globals.sh_email_name)
                print(data['slug'])
                print(current_user.username)
                

                try:
                    new_team.save_to_db()
                    
                    resp = jsonify({
                        'success': True,
                        'slug':data['slug'],
                    })
                    resp.status_code = 200
                    return resp
                except Exception as e:
                    capture_exception(e)
                    return {'message': 'Something went wrong', 'current_user': current_user.id, 'slug':data['slug'], 'date_updated':datetime.utcnow}, 500
             
            else:
                try:
                    return {'message': 'Team/Username combo cannot be registered at this time.'}
                except Exception as e:
                    capture_exception(e)
                    return {'message': 'Something went wrong', 'current_user': current_user.id, 'slug':data['slug'], 'date_updated':datetime.utcnow}, 500
        else:
            try:
                return {'message': messageFromStripe}
            except Exception as e:
                capture_exception(e)
                return {'message': 'Something went wrong'}, 500   
