# java -cp htmlunit-driver-2.36.0-jar-with-dependencies.jar:selenium-server-standalone-3.141.59.jar org.openqa.grid.selenium.GridLauncherV3

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time 
import datetime
from selenium.webdriver.common.keys import Keys

capabilities = {
    "browserName": "chrome",
    "version": "75.0",
    "enableVNC": True,
    "enableVideo": False,
    "handlesAlerts":True,
    'goog:chromeOptions': {'prefs': {'profile.default_content_setting_values.notifications': 1}, 'extensions': [], 'args': ['--disable-infobars', 'start-maximized', '--disable-extensions', '-timeout 1m']}
}

driver2 = webdriver.Remote(
    command_executor="http://159.203.163.8:4444/wd/hub",
    desired_capabilities=capabilities)
driver2.session_id = "402f337c3a0700218a8ad1799f68b4b9"
print(driver2.current_url)
print(driver2.session_id)
print(driver2.title)

