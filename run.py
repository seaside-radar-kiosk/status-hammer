import os
from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from datetime import timedelta
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration

sentry_sdk.init(
    dsn="https://743911de238f468799b5af8479916c38@sentry.io/1886056",
    integrations=[FlaskIntegration(),SqlalchemyIntegration()]
)

if os.environ.get('HEROKU_APP_NAME') is None:
    sentry_sdk.init(environment="local")
elif os.environ.get('HEROKU_APP_NAME') == "statushammer-dev":
    sentry_sdk.init(environment="dev")
elif os.environ.get('HEROKU_APP_NAME') == "statushammer-stage":
    sentry_sdk.init(environment="stage")
elif os.environ.get('HEROKU_APP_NAME') == "statushammer-prod":
    sentry_sdk.init(environment="prod")
else:
    sentry_sdk.init(environment="local")

# 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

app = Flask(__name__, static_url_path='', static_folder='frontend')

app.config.update(dict(
    DEBUG = True,
))

cors = CORS(app, resources={r"/*": {"origins": ["http://localhost:9000", "https://objective-babbage-c2bb99.netlify.com"], "supports_credentials": True}})
api = Api(app)

# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
POSTGRES = {
    'user': 'postgres',
    'pw': '',
    'db': 'statushammer-dev-2',
    'host': 'localhost',
    'port': '5432',
}
if os.environ.get('DATABASE_URL') is None:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
migrate = Migrate(app, db)

@app.before_first_request
def create_tables():
    db.create_all()

# Configure application to store JWTs in cookies
app.config['JWT_TOKEN_LOCATION'] = ['cookies']

# Only allow JWT cookies to be sent over https. In production, this
# should likely be True
app.config['JWT_COOKIE_SECURE'] = False

# Set the cookie paths, so that you are only sending your access token
# cookie to the access endpoints, and only sending your refresh token
# to the refresh endpoint. Technically this is optional, but it is in
# your best interest to not send additional cookies in the request if
# they aren't needed.
app.config['JWT_ACCESS_COOKIE_PATH'] = '/api/'
app.config['JWT_REFRESH_COOKIE_PATH'] = '/token/refresh'

app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(minutes=10)
app.config['JWT_REFRESH_TOKEN_EXPIRES'] = timedelta(minutes=60)
app.config['JWT_CSRF_METHODS'] = ['POST', 'PUT', 'PATCH', 'DELETE']

# Enable csrf double submit protection. See this for a thorough
# explanation: http://www.redotheweb.com/2015/11/09/api-security.html
app.config['JWT_COOKIE_CSRF_PROTECT'] = True

# Set the secret key to sign the JWTs with
app.config['JWT_SECRET_KEY'] = 'jwt-secret-string' # Change this!
jwt = JWTManager(app)

app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return models.RevokedTokenModel.is_jti_blacklisted(jti)

import views, models, resources

api.add_resource(resources.UserRegistration, '/api/register')
api.add_resource(resources.UserLogin, '/api/login')
api.add_resource(resources.UserForgot, '/api/forgot')
api.add_resource(resources.UserPasswordReset, '/api/reset')
api.add_resource(resources.UserLogoutAccess, '/api/logout/access')
api.add_resource(resources.UserLogoutRefresh, '/token/refresh/logout')
api.add_resource(resources.UserLogoutCookie, '/api/logout/cookie')
api.add_resource(resources.TokenRefresh, '/token/refresh')
# api.add_resource(resources.AllUsers, '/api/users')
api.add_resource(resources.SecretResource, '/api/secret')
api.add_resource(resources.ScheduleResource, '/api/schedule')
api.add_resource(resources.AllTeams, '/api/teams')
api.add_resource(resources.TeamSingle, '/api/team/<string:team_slug>')
api.add_resource(resources.AddTeam, '/api/add-team')
api.add_resource(resources.AddTeamData, '/api/add-team-data')
api.add_resource(resources.CurrentUser, '/api/account')
api.add_resource(resources.ShclientResponse, '/api/shclient')


if __name__ == "__main__":
    app.run(host='0.0.0.0')