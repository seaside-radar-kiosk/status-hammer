from run import db
from passlib.hash import pbkdf2_sha256 as sha256
from datetime import datetime
import enum
from globals import Globals
import stripe
from sqlalchemy import or_
from sentry_sdk import capture_exception


stripe.api_key = "STRIPEKEY"

class ShclientResponses(db.Model):
    __tablename__ = 'shclient_responses' 
    id = db.Column(db.Integer, primary_key = True)
    team = db.Column(db.Integer)
    address = db.Column(db.String(255))
    updateRequested = db.Column(db.Boolean)
    data = db.Column(db.String(1536))
    created = db.Column(db.DateTime)

    def add(self):
        db.session.add(self)
        db.session.commit()

class UserModel(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(120), unique = True, nullable = False)
    password = db.Column(db.String(120), nullable = False)
    privacy_policy = db.Column(db.Boolean)
    terms_of_service = db.Column(db.Boolean)
    registration_date = db.Column(db.DateTime)
    stripe_customer = db.Column(db.String(255))
    stripe_card = db.Column(db.String(255))
    password_reset_date = db.Column(db.DateTime)
    password_reset_code = db.Column(db.String(255))
    is_admin = db.Column(db.Boolean)
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}
    
    @classmethod
    def find_by_reset_code(cls, reset_code):
        print(reset_code)
        return cls.query.filter_by(password_reset_code = reset_code).first()

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id = id).first()

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username = username).first()

    @classmethod
    def get_account_info(cls,username):
        current_user = cls.query.filter_by(username = username).first().as_dict()
        del current_user['password']
        current_user['teams'] = TeamModel.find_teams_for_user(current_user['id'])
        current_user['teams_count'] = len(current_user['teams'])
        del current_user['password_reset_date']
        del current_user['password_reset_code']
        try:
            current_user['charges'] = []
            charges = stripe.Charge.list(customer=current_user['stripe_customer'], limit=100)['data']
            # print(invoices)
            # print(len(invoices))
            for charge in charges:
                created = datetime.fromtimestamp(charge['created']).strftime('%m/%d/%y')
                amount = charge['amount']/100
                amount_refunded = charge['amount_refunded']/100
                description = str(charge['description'])
                print(charge['status'])
                charge_dict = {
                    'date_paid': created,
                    'amount_paid':amount, 
                    'amount_refunded':amount_refunded, 
                    'paid': charge['paid'],
                    'description': description,
                    'receipt_url':charge['receipt_url'],
                    }
                if charge['status'] == "failed":
                    pass
                else:
                    current_user['charges'].append(charge_dict)
                
        except Exception as e:
            capture_exception(e)
        try:
            current_user['registration_date'] = current_user['registration_date'].isoformat()
        except Exception as e:
            capture_exception(e)

        if current_user['stripe_card']:
            stripe_card = stripe.PaymentMethod.retrieve(current_user['stripe_card'],)
            current_user['card_text'] = "Last 4 - " + stripe_card['card']['last4']
            current_user['has_card'] = True
        else:
            current_user['card_text'] = "No active card"
            current_user['has_card'] = False


        return current_user
    
    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'username': x.username,
                'password': x.password
            }
        return {'users': list(map(lambda x: to_json(x), UserModel.query.all()))}

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except Exception as e:
            capture_exception(e)
            return {'message': 'Something went wrong'}

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)
    
    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)

class RevokedTokenModel(db.Model):
    __tablename__ = 'revoked_tokens'
    id = db.Column(db.Integer, primary_key = True)
    jti = db.Column(db.String(120))
    
    def add(self):
        db.session.add(self)
        db.session.commit()
    
    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti = jti).first()
        return bool(query)

class ExceptionModel(db.Model):
    __tablename__ = 'exceptions'

    id = db.Column(db.Integer, primary_key = True)
    user = db.Column(db.Integer, nullable = False)
    team = db.Column(db.Integer, nullable = False)
    start_date = db.Column(db.DateTime, nullable = False)
    end_date = db.Column(db.DateTime, nullable = False)

    @classmethod
    def get_upcoming_exceptions(cls, user_id, team_id):
        return cls.query.filter_by(user = user_id).filter_by(team = team_id).filter(or_(ExceptionModel.start_date >= datetime.now(), ExceptionModel.end_date >= datetime.now())).order_by(cls.start_date).all()
    
    @classmethod
    def get_upcoming_exceptions_shclient(cls, team_id):
        return cls.query.filter_by(team = team_id).filter(or_(ExceptionModel.start_date >= datetime.now(), ExceptionModel.end_date >= datetime.now())).order_by(cls.start_date).all()
    
    @classmethod
    def find_by_id(cls, user_id, team_id, id):
        return cls.query.filter_by(user = user_id).filter_by(team = team_id).filter_by(id = id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

class NotificationType(enum.Enum):
    ALL = "All"
    KEYWORDS = "Keywords"
    NONE = "None"
class TeamModel(db.Model):
    __tablename__ = 'teams'

    id = db.Column(db.Integer, primary_key = True)
    slug = db.Column(db.String(120), nullable = False)
    uuid = db.Column(db.String(120), nullable = False)
    user = db.Column(db.Integer, nullable = False)
    client_ip = db.Column(db.String(120))
    client_id = db.Column(db.Integer)
    client_status = db.Column(db.String(255))
    client_key = db.Column(db.String(255), unique = True)
    client_connected = db.Column(db.Boolean)
    client_last_update = db.Column(db.DateTime)
    date_created = db.Column(db.DateTime)
    date_updated = db.Column(db.DateTime)
    team_credentials_username = db.Column(db.String(511))
    team_credentials_password = db.Column(db.String(511))
    team_timezone_offset = db.Column(db.Float(1))
    team_is_deleted = db.Column(db.Boolean)
    team_deleted_date = db.Column(db.DateTime)
    team_client_status = db.Column(db.String(255))
    current_presence = db.Column(db.Boolean)
    current_status = db.Column(db.String(500))
    current_status_emoji = db.Column(db.String(100))
    current_notifications_type = db.Column(db.Enum(NotificationType))
    current_notifications_keywords = db.Column(db.String(500))
    do_not_disturb_enabled = db.Column(db.Boolean)
    do_not_disturb_start = db.Column(db.Integer)
    do_not_disturb_end = db.Column(db.Integer)
    schedule_enabled = db.Column(db.Boolean)
    schedule_active_sunday = db.Column(db.Boolean)
    schedule_active_monday = db.Column(db.Boolean)
    schedule_active_tuesday = db.Column(db.Boolean)
    schedule_active_wednesday = db.Column(db.Boolean)
    schedule_active_thursday = db.Column(db.Boolean)
    schedule_active_friday = db.Column(db.Boolean)
    schedule_active_saturday = db.Column(db.Boolean)
    schedule_active_start = db.Column(db.DateTime)
    schedule_active_end = db.Column(db.DateTime)
    schedule_active_status_enabled = db.Column(db.Boolean)
    schedule_active_status = db.Column(db.String(100))
    schedule_active_status_emoji = db.Column(db.String(500))
    schedule_active_notifications_enabled = db.Column(db.Boolean)
    schedule_active_notifications_type = db.Column(db.Enum(NotificationType))
    schedule_active_notifications_keywords = db.Column(db.String(500))
    schedule_inactive_status_enabled = db.Column(db.Boolean)
    schedule_inactive_status = db.Column(db.String(500))
    schedule_inactive_status_emoji = db.Column(db.String(100))
    schedule_inactive_notifications_enabled = db.Column(db.Boolean)
    schedule_inactive_notifications_type = db.Column(db.Enum(NotificationType))
    schedule_inactive_notifications_keywords = db.Column(db.String(500))
    next_invoice_date = db.Column(db.DateTime)
    next_invoice_notice_date = db.Column(db.DateTime)
    last_invoice_date = db.Column(db.DateTime)
    last_stripe_charge = db.Column(db.String(256))
    last_stripe_invoice = db.Column(db.String(256))
        
    @classmethod
    def return_all(cls):
        return TeamModel.query.all()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    @classmethod
    def find_by_slug_and_username(cls, slug, username):
        return cls.query.filter_by(team_credentials_username = username).filter_by(uuid = slug).first()

    @classmethod
    def find_by_slug_and_user_for_update(cls, slug, user_id):
        return cls.query.filter_by(user = user_id).filter_by(uuid = slug).first()
    
    @classmethod
    def find_by_client_key(cls, client_key):
        return cls.query.filter_by(client_key = client_key).first()

    @classmethod
    def find_by_slug_and_user(cls, slug, user_id):
        try:
            team = cls.query.filter_by(user = user_id).filter_by(uuid = slug).first().as_dict()
            team['exceptions_raw'] =  ExceptionModel.get_upcoming_exceptions(user_id, team['id'])
        
            return format_team_dict_output(team)
        except ExceptionModel as e:
            capture_exception(e)
            return None
    
    @classmethod
    def find_by_id(cls, id):
        team = cls.query.filter_by(id = id).first().as_dict()
        team['exceptions_raw'] =  ExceptionModel.get_upcoming_exceptions(team['user'], team['id'])
        return format_team_dict_output(team)
    
    @classmethod
    def find_teams_for_user(cls, user_id):
        # all_user_teams = cls.query.filter_by(user = user_id).order_by(cls.date_updated.desc()).all()
        all_user_teams = cls.query.filter_by(user = user_id).order_by(cls.uuid).all()
        user_teams = []
        for team_raw in all_user_teams:
            team = team_raw.as_dict()
            

            user_teams.append(format_team_dict_output(team))
        return user_teams
    
    @classmethod
    def find_teams_for_user_raw(cls, user_id):
        # all_user_teams = cls.query.filter_by(user = user_id).order_by(cls.date_updated.desc()).all()
        all_user_teams = cls.query.filter_by(user = user_id).order_by(cls.uuid).all()
        return all_user_teams

timezones = {
    -12.0: "(GMT -12:00) Eniwetok, Kwajalein", 
    -11.0: "(GMT -11:00) Midway Island, Samoa", 
    -10.0: "(GMT -10:00) Hawaii", 
    -9.0: "(GMT -9:00) Alaska", 
    -8.0: "(GMT -8:00) Pacific Time (US &amp; Canada)", 
    -7.0: "(GMT -7:00) Mountain Time (US &amp; Canada)", 
    -6.0: "(GMT -6:00) Central Time (US &amp; Canada), Mexico City", 
    -5.0: "(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima", 
    -4.0: "(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz", 
    -3.5: "(GMT -3:30) Newfoundland", 
    -3.0: "(GMT -3:00) Brazil, Buenos Aires, Georgetown", 
    -2.0: "(GMT -2:00) Mid-Atlantic", 
    -1.0: "(GMT -1:00 hour) Azores, Cape Verde Islands", 
    0.0: "(GMT) Western Europe Time, London, Lisbon, Casablanca", 
    1.0: "(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris", 
    2.0: "(GMT +2:00) Kaliningrad, South Africa", 
    3.0: "(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg", 
    3.5: "(GMT +3:30) Tehran", 
    4.0: "(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi", 
    4.5: "(GMT +4:30) Kabul", 
    5.0: "(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent", 
    5.5: "(GMT +5:30) Bombay, Calcutta, Madras, New Delhi", 
    5.75: "(GMT +5:45) Kathmandu", 
    6.0: "(GMT +6:00) Almaty, Dhaka, Colombo", 
    7.0: "(GMT +7:00) Bangkok, Hanoi, Jakarta", 
    8.0: "(GMT +8:00) Beijing, Perth, Singapore, Hong Kong", 
    9.0: "(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk", 
    9.5: "(GMT +9:30) Adelaide, Darwin", 
    10.0: "(GMT +10:00) Eastern Australia, Guam, Vladivostok", 
    11.0: "(GMT +11:00) Magadan, Solomon Islands, New Caledonia", 
    12.0: "(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka", 
}

dnd_times = {    
    0: "12:00 AM",
    1: "12:30 AM",
    2: "1:00 AM",
    3: "1:30 AM",
    4: "2:00 AM",
    5: "2:30 AM",
    6: "3:00 AM",
    7: "3:30 AM",
    8: "4:00 AM",
    9: "4:30 AM",
    10: "5:00 AM",
    11: "5:30 AM",
    12: "6:00 AM",
    13: "6:30 AM",
    14: "7:00 AM",
    15: "7:30 AM",
    16: "8:00 AM",
    17: "8:30 AM",
    18: "9:00 AM",
    19: "9:30 AM",
    20: "10:00 AM",
    21: "10:30 AM",
    22: "11:00 AM",
    23: "11:30 AM",
    24: "12:00 PM",
    25: "12:30 PM",
    26: "1:00 PM",
    27: "1:30 PM",
    28: "2:00 PM",
    29: "2:30 PM",
    30: "3:00 PM",
    31: "3:30 PM",
    32: "4:00 PM",
    33: "4:30 PM",
    34: "5:00 PM",
    35: "5:30 PM",
    36: "6:00 PM",
    37: "6:30 PM",
    38: "7:00 PM",
    39: "7:30 PM",
    40: "8:00 PM",
    41: "8:30 PM",
    42: "9:00 PM",
    43: "9:30 PM",
    44: "10:00 PM",
    45: "10:30 PM",
    46: "11:00 PM",
    47: "11:30 PM",
}
def clamp_to_twelve(time_dt, midday_dt):
    clamp_dt = time_dt - midday_dt
    minutes, seconds = divmod(clamp_dt.seconds, 60)
    hours, minutes = divmod(minutes, 60)
    return hours, minutes

def to_twelve_hour_time_converter(time):
    midday_dt = datetime.strptime('12:00','%H:%M')
    time_dt = datetime.strptime(time, '%H:%M')

    if time_dt >= datetime.strptime('13:00', '%H:%M'):
        hours, minutes = clamp_to_twelve(time_dt, midday_dt)
        time = f'{hours}:{minutes} PM'

    elif time_dt >= midday_dt:
        time += ' PM'

    elif time_dt < datetime.strptime('10:00', '%H:%M'):
        time = f'{time[1:]} AM'

    elif is_midnight(time_dt):
        hours, minutes = clamp_to_twelve(time_dt, midday_dt)
        time = f'{hours}:{minutes} AM'

    else:
        time += ' AM'

    return time

def is_midnight(time_dt):
    return (time_dt >= datetime.strptime('00:00','%H:%M') and
        time_dt <= datetime.strptime('00:59','%H:%M'))

def to_twenty_four_hour_time_converter(time):
    if time[-2:] == "AM" and time[:2] == "12": 
        return "00" + time[2:-2] 
    elif time[-2:] == "AM": 
        return time[:-2] 
    elif time[-2:] == "PM" and time[:2] == "12": 
        return time[:-2] 
    else: 
        try:
            time_hrs = int(time[:2])
        except Exception as e:
            capture_exception(e)
            time = "0" + time
            time_hrs = int(time[:2])
        return str(int(time_hrs) + 12) + time[2:8] 


def format_team_dict_output(team):
    
    del team['team_credentials_username']
    del team['id']
    del team['client_ip']
    del team['client_key']
    del team['date_created']

    schedule_start = team['schedule_active_start'].time()
    schedule_end  = team['schedule_active_end'].time()
    now = datetime.now().time()

    team['schedule_active'] = False
    if schedule_end > now and schedule_start < now and team['schedule_enabled']:
        team['schedule_active'] = True

    team['exceptions'] = []
    try:
        for exception in team['exceptions_raw']:
            exception_dict = {
                'exception_id': exception.id,
                'start_date': exception.start_date.strftime('%m/%d/%y'),
                'start_date_formatted': exception.start_date.strftime('%Y-%m-%d'), 
                'end_date':exception.end_date.strftime('%m/%d/%y'), 
                'end_date_formatted': exception.end_date.strftime('%Y-%m-%d'), 
                }
            team['exceptions'].append(exception_dict)
        
        del team['exceptions_raw']
    except:
        pass

    try:
        team['next_invoice_date'] = team['next_invoice_date'].isoformat()
    except:
        pass

    try:
        team['client_last_update'] = team['client_last_update'].isoformat()
    except:
        pass

    try:
        team['last_invoice_date'] = team['last_invoice_date'].isoformat()
    except:
        pass

    try:
        team['date_created'] = team['date_created'].isoformat()
    except:
        pass
    
    try:
        team['date_updated'] = team['date_updated'].isoformat()
    except:
        pass
    
    if team['team_is_deleted']:
        try: 
            team['team_days_til_deleted'] = datetime.utcnow() - team['team_deleted_date']
            team['team_days_til_deleted'] = 7 - team['team_days_til_deleted'].days
        except:
            pass

    try:
        team['team_deleted_date'] = team['team_deleted_date'].isoformat()
    except:
        pass
    
    try:
        team['schedule_active_start'] = team['schedule_active_start'].strftime("%H:%M")
    except:
        pass
    
    

    try:
        team['schedule_active_end'] = team['schedule_active_end'].strftime("%H:%M")
    except:
        pass

    team['team_timezone_text'] = timezones.get(team['team_timezone_offset'], "UTC")
    
    team['do_not_disturb_start_string'] = dnd_times.get(team['do_not_disturb_start'])
    team['do_not_disturb_end_string'] = dnd_times.get(team['do_not_disturb_end'])

    if(team['current_presence']):
        team['current_presence_text'] = "Active"
    else:
        team['current_presence_text'] = "Inactive"

    if team['current_notifications_keywords'] is None:
        team['current_notifications_keywords'] = ""
    if team['schedule_active_notifications_keywords'] is None:
        team['schedule_active_notifications_keywords'] = ""
    if team['schedule_inactive_notifications_keywords'] is None:
        team['schedule_inactive_notifications_keywords'] = ""


    if(team['current_notifications_type']):
        if(team['current_notifications_type'] == NotificationType.KEYWORDS):
            team['current_notifications_type'] = "KEYWORDS"
            team['current_notifications_text'] = "Keywords - \"" +  team['current_notifications_keywords'] + "\""
        elif(team['current_notifications_type'] == NotificationType.NONE):
            team['current_notifications_type'] = "NONE"
            team['current_notifications_text'] = "None"
        elif(team['current_notifications_type'] == NotificationType.ALL):
            team['current_notifications_type'] = "ALL"
            team['current_notifications_text'] = "All"
    
    if(team['schedule_active_notifications_type']):
        if(team['schedule_active_notifications_type'] == NotificationType.KEYWORDS):
            team['schedule_active_notifications_type'] = "KEYWORDS"
            team['schedule_active_notifications_text'] = "Will change to \"" +  team['schedule_active_notifications_keywords'] + "\""
        elif(team['schedule_active_notifications_type'] == NotificationType.NONE):
            team['schedule_active_notifications_type'] = "NONE"
            team['schedule_active_notifications_text'] = "Will not change"
        elif(team['schedule_active_notifications_type'] == NotificationType.ALL):
            team['schedule_active_notifications_type'] = "ALL"
            team['schedule_active_notifications_text'] = "Will change to \"ALL\""

    if(team['schedule_inactive_notifications_type']):
        if(team['schedule_inactive_notifications_type'] == NotificationType.KEYWORDS):
            team['schedule_inactive_notifications_type'] = "KEYWORDS"
            team['schedule_inactive_notifications_text'] = "Will change to \"" +  team['schedule_inactive_notifications_keywords'] + "\""
        elif(team['schedule_inactive_notifications_type'] == NotificationType.NONE):
            team['schedule_inactive_notifications_type'] = "NONE"
            team['schedule_inactive_notifications_text'] = "Will not change"
        elif(team['schedule_inactive_notifications_type'] == NotificationType.ALL):
            team['schedule_inactive_notifications_type'] = "ALL"
            team['schedule_inactive_notifications_text'] = "Will change to \"ALL\""

    
    if not team['schedule_active_notifications_enabled']:
        team['schedule_active_notifications_text'] = "Will not change"    
    
    if not team['schedule_inactive_notifications_enabled']:
        team['schedule_inactive_notifications_text'] = "Will not change"

    if(team['schedule_active_status_enabled']):
        if(team['schedule_active_status'] == "" or team['schedule_active_status'] ==  None):
            team['schedule_active_status_text'] = "Will change to default (no status)"
        else:
            team['schedule_active_status_text'] = "Will change to \"" + team['schedule_active_status'] + "\""
    else:
        team['schedule_active_status_text'] = "Will not change"
    
    if(team['schedule_inactive_status_enabled']):
        if(team['schedule_inactive_status'] == "" or team['schedule_inactive_status'] ==  None):
            team['schedule_inactive_status_text'] = "Will change to default (no status)"
        else:
            team['schedule_inactive_status_text'] = "Will change to \"" + team['schedule_inactive_status'] + "\""
    else:
        team['schedule_inactive_status_text'] = "Will not change"
    
    
    del team['date_updated']
    del team['team_is_deleted']
    del team['team_deleted_date']
    del team['current_status']
    del team['current_status_emoji']
    del team['current_notifications_type']
    del team['current_notifications_keywords']
    del team['do_not_disturb_enabled']
    del team['do_not_disturb_start']
    del team['next_invoice_notice_date']


    return team
