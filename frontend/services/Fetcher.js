import Urls            from '../../services/Urls.js'
import Cookies            from '../../services/Cookies.js'

const genericServerError = {'message' : 'There was an error connecting to the server, please try again or contact support. Thank you.'};

const Fetcher = { 
    
    get : async(url, successCallback) => {
        const refreshOptions = {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
              'X-CSRF-TOKEN': Cookies.getCookie('csrf_refresh_token')
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
       };
    
        const getOptions = {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
              'X-CSRF-TOKEN': Cookies.getCookie('csrf_access_token')
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
       };
    
       try {
            const firstResponse = await fetch(url, getOptions);
            const firstJson = await firstResponse.json();
            if(firstResponse.status !== 200){
                // The access token is likely no longer good
                const refreshResponse = await fetch(Urls.apiRefresh, refreshOptions);
                const refreshJson = await refreshResponse.json();
                if(refreshResponse.status !== 200){
                    // Refresh token didn't work
                    Cookies.setCookie('desiredPreLoginPage',window.location.hash,7);
                    window.location.hash = Urls.appLogin;
                }else{
                    if(refreshJson['csrf_access_token'] && refreshJson['csrf_refresh_token']){
                        Cookies.eraseCookie('csrf_access_token');
                        Cookies.eraseCookie('csrf_refresh_token');
                        Cookies.setCookie('csrf_access_token', refreshJson['csrf_access_token'],7);
                        Cookies.setCookie('csrf_refresh_token', refreshJson['csrf_refresh_token'],7);
                    }
                    const secondResponse = await fetch(url, getOptions);
                    const secondJson = await secondResponse.json();
                    if(secondResponse.status !== 200){
                        if(secondResponse.status == 500){
                            Cookies.eraseCookie('desiredPreLoginPage');
                            window.location.hash = Urls.appHome;
                        }
                        console.log("There's an error, please contact support.")
                        Cookies.setCookie('desiredPreLoginPage',window.location.hash,7);
                        window.location.hash = Urls.appLogin;
                    }else{
                        successCallback(secondJson);
                        return secondJson;
                    }
                    //return secondJson
                }
            }else{
                // Its all good!
                successCallback(firstJson);
                return firstJson
     
            }
        } catch (err) {
           console.log('Error', err)
           Cookies.setCookie('desiredPreLoginPage',window.location.hash,7);
           window.location.hash = Urls.appLogin;
        }
    },
    delete : async(url, successCallback) => {
        const refreshOptions = {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
              'X-CSRF-TOKEN': Cookies.getCookie('csrf_refresh_token')
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
       };
    
        const getOptions = {
            method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
              'X-CSRF-TOKEN': Cookies.getCookie('csrf_access_token')
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
       };
    
       try {
            const firstResponse = await fetch(url, getOptions);
            const firstJson = await firstResponse.json();
            if(firstResponse.status !== 200){
                // The access token is likely no longer good
                const refreshResponse = await fetch(Urls.apiRefresh, refreshOptions);
                const refreshJson = await refreshResponse.json();
                if(refreshResponse.status !== 200){
                    // Refresh token didn't work
                    Cookies.setCookie('desiredPreLoginPage',window.location.hash,7);
                    window.location.hash = Urls.appLogin;
                }else{
                    if(refreshJson['csrf_access_token'] && refreshJson['csrf_refresh_token']){
                        Cookies.eraseCookie('csrf_access_token');
                        Cookies.eraseCookie('csrf_refresh_token');
                        Cookies.setCookie('csrf_access_token', refreshJson['csrf_access_token'],7);
                        Cookies.setCookie('csrf_refresh_token', refreshJson['csrf_refresh_token'],7);
                    }
                    const secondResponse = await fetch(url, getOptions);
                    const secondJson = await secondResponse.json();
                    if(secondResponse.status !== 200){
                        Cookies.eraseCookie('csrf_access_token');
                        Cookies.eraseCookie('csrf_refresh_token');
                        console.log("There's an error, please contact support.")
                        Cookies.setCookie('desiredPreLoginPage',window.location.hash,7);
                        window.location.hash = Urls.appLogin;
                    }else{
                        successCallback(secondJson);
                        return secondJson;
                    }
                    //return secondJson
                }
            }else{
                // Its all good!
                successCallback(firstJson);
                return firstJson
     
            }
        } catch (err) {
           console.log('Error', err);
           Cookies.setCookie('desiredPreLoginPage',window.location.hash,7);
           window.location.hash = Urls.appLogin;
        }
    },
    post : async(url, requestBody, successCallback) => {
        const refreshOptions = {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
              'X-CSRF-TOKEN': Cookies.getCookie('csrf_refresh_token')
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
       };
    
        const postOptions = {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
              'X-CSRF-TOKEN': Cookies.getCookie('csrf_access_token')
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: requestBody
       };
    
       try {
            const firstResponse = await fetch(url, postOptions);
            const firstJson = await firstResponse.json();
            console.log(firstResponse);
            console.log(firstJson);
            if(firstResponse.status !== 200){
                // The access token is likely no longer good
                const refreshResponse = await fetch(Urls.apiRefresh, refreshOptions);
                const refreshJson = await refreshResponse.json();
                console.log(refreshResponse);
                console.log(refreshJson);
                if(refreshResponse.status !== 200){
                    // Refresh token didn't work
                    Cookies.setCookie('desiredPreLoginPage',window.location.hash,7);
                    window.location.href = Urls.appLogin;
                }else{
                    if(refreshJson['csrf_access_token'] && refreshJson['csrf_refresh_token']){
                        Cookies.eraseCookie('csrf_access_token');
                        Cookies.eraseCookie('csrf_refresh_token');
                        Cookies.setCookie('csrf_access_token', refreshJson['csrf_access_token'],7);
                        Cookies.setCookie('csrf_refresh_token', refreshJson['csrf_refresh_token'],7);
                    }
                    const secondResponse = await fetch(url, postOptions);
                    const secondJson = await secondResponse.json();
                    console.log(secondResponse)
                    console.log(secondJson)
                    if(secondResponse.status !== 200){
                        Cookies.eraseCookie('csrf_access_token');
                        Cookies.eraseCookie('csrf_refresh_token');
                        console.log("There's an error, please contact support.")
                        Cookies.setCookie('desiredPreLoginPage',window.location.hash,7);
                        window.location.hash = Urls.appLogin;
                    }else{
                        successCallback(secondJson);
                        return secondJson;
                    }
                    //return secondJson
                }
            }else{
                // Its all good!
                successCallback(firstJson);
                return firstJson
     
            }
        } catch (err) {
           console.log('Error', err);
           Cookies.setCookie('desiredPreLoginPage',window.location.hash,7);
           window.location.hash = Urls.appLogin;
        }
    },
    
    getWithoutRefresh : async(url, successCallback) => {
        const getOptions = {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
       };
    
       try {
            const firstResponse = await fetch(url, getOptions);
            const firstJson = await firstResponse.json();
            if(firstResponse.status !== 200){
                console.log("There's an error, please contact support.");
                successCallback(genericServerError);
                return genericServerError
            }else{
                // Its all good!
                successCallback(firstJson);
                return firstJson
     
            }
        } catch (err) {
           console.log('Error', err);
           successCallback(genericServerError);
           return genericServerError
        }
    },
    postWithoutRefresh : async(url, requestBody, successCallback) => {    
        const postOptions = {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: requestBody
       };
    
       try {
            const firstResponse = await fetch(url, postOptions);
            const firstJson = await firstResponse.json();
            if(firstResponse.status !== 200){
                console.log("There's an error, please contact support.");
                successCallback(genericServerError);
                return genericServerError
            }else{
                // Its all good!
                successCallback(firstJson);
                return firstJson
            }
        } catch (err) {
           console.log('Error', err);
           successCallback(genericServerError);
           return genericServerError
        }
    },
    postLogoutRefresh: async(url, requestBody, successCallback) => {    
        const postOptions = {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
              'X-CSRF-TOKEN': Cookies.getCookie('csrf_refresh_token')
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: requestBody
       };
    
       try {
            const firstResponse = await fetch(url, postOptions);
            const firstJson = await firstResponse.json();
            if(firstResponse.status !== 200){
                console.log("There's an error, please contact support.");
                successCallback(genericServerError);
                return genericServerError
            }else{
                // Its all good!
                successCallback(firstJson);
                return firstJson
            }
        } catch (err) {
           console.log('Error', err)
           successCallback(genericServerError);
           return genericServerError
        }
    },
    postLogoutAccess: async(url, requestBody, successCallback) => {    
        const postOptions = {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
              'X-CSRF-TOKEN': Cookies.getCookie('csrf_access_token')
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: requestBody
       };
    
       try {
            const firstResponse = await fetch(url, postOptions);
            const firstJson = await firstResponse.json();
            if(firstResponse.status !== 200){
                console.log("There's an error, please contact support.")
                successCallback(genericServerError);
                return genericServerError
            }else{
                // Its all good!
                successCallback(firstJson);
                return firstJson
            }
        } catch (err) {
           console.log('Error', err)
           successCallback(genericServerError);
           return genericServerError
        }
    },
    getGeneric : async(url, successCallback) => {
        const getOptions = {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'no-cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'omit', // include, *same-origin, omit
            headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
       };
    
       try {
            const firstResponse = await fetch(url, getOptions);
            const firstJson = await firstResponse.json();
            if(firstResponse.status !== 200){
                console.log("There's an error, please contact support.");
                successCallback(genericServerError);
                return genericServerError
            }else{
                // Its all good!
                successCallback(firstJson);
                return firstJson
     
            }
        } catch (err) {
           console.log('Error', err);
           successCallback(genericServerError);
           return genericServerError
        }
    },
    

}

export default Fetcher;