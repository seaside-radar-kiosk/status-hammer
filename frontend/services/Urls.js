const appLocation = "/#";
// const apiRoot = "http://localhost:5000/";
const apiRoot = "/";

const Urls = { 
    appRoot : appLocation, 
    appLogin: "/login/",
    appLanding: "/",
    appPrivacy: "/privacy",
    appTOS: "/tos",
    appRegister: "/register",
    appHome: "/teams",
    appTeam: "/team/",
    appAddTeam: "/add-team/",
    appFirstTeam: "/first-team/",
    appEditTeam: "/edit-team/",
    appAccount: "/account/",
    appForgot : "/forgot/",

    apiLogin : apiRoot + "api/login",
    apiForgot : apiRoot + "api/forgot",
    apiReset : apiRoot + "api/reset",
    apiLogoutAccess : apiRoot + "api/logout/access",
    apiLogoutRefresh : apiRoot + "token/refresh/logout",
    apiRegister : apiRoot + "api/register",
    apiRefresh : apiRoot + "token/refresh",
    apiTest : apiRoot + "api/stripe",
    apiAllTeams : apiRoot + "api/teams",
    apiSingleTeam : apiRoot + "api/team/",
    apiSingleTeamStatus : apiRoot + "api/team-status/",
    apiAddTeam : apiRoot + "api/add-team",
    apiAddTeamData : apiRoot + "api/add-team-data",
    apiAccount : apiRoot + "api/account",
    apiSecret : apiRoot + "api/secret",

    appPriceFloat : 10,
    appPriceText : "$10",
    appTrialPeriodDays : 7,
    appDeleteTeamDays : 7,
    appSupportHref : "mailto:info@statushammer.com",
    appSupportCalendarly : "https://calendly.com/statushammer/tell-me-more"
}

export default Urls;