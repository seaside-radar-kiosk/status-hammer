"use strict";

import Error404             from '../views/pages/Error404.js'
import TeamAllTeams         from '../views/pages/TeamAllTeams.js'
import TeamAllTeamsMin         from '../views/pages/TeamAllTeamsMin.js'
import AddTeam              from '../views/pages/AddTeam.js'
import FirstTeam            from '../views/pages/FirstTeam.js'
import EditTeam             from '../views/pages/EditTeam.js'
import EditSchedule         from '../views/pages/EditSchedule.js'
import TeamSingleScheduleCombined           from '../views/pages/TeamSingleScheduleCombined.js'
import TeamSingle           from '../views/pages/TeamSingle.js'
import UserAccount          from '../views/pages/UserAccount.js'
import UserChangeEmail      from '../views/pages/UserChangeEmail.js'
import UserChangePassword   from '../views/pages/UserChangePassword.js'
import UserChangeCC         from '../views/pages/UserChangeCC.js'
import UserChangeBillingDate from '../views/pages/UserChangeBillingDate.js'
import UserLogin            from '../views/pages/UserLogin.js'
import UserRegister         from '../views/pages/UserRegister.js'
import UserResetPassword    from '../views/pages/UserResetPassword.js'
import UserForgotPassword   from '../views/pages/UserForgotPassword.js'
import Landing              from '../views/pages/Landing.js'
import TOS                  from '../views/pages/TOS.js'
import Privacy              from '../views/pages/Privacy.js'

import Bottombar            from '../views/components/Bottombar.js' 

import Utils                from './Utils.js'
import Cookies              from './Cookies.js'
import Urls                 from './Urls.js'


const Router = { 
    // List of supported routes. Any url other than these routes will throw a 404 error
    routes : {
        '/'                          : Landing,
        '/tos'                       : TOS,
        '/privacy'                   : Privacy,
        '/teams'                     : TeamAllTeamsMin,
        '/login'                     : UserLogin,
        '/reset'                     : UserResetPassword,
        '/reset/:slug'               : UserResetPassword,
        '/forgot'                    : UserForgotPassword,
        '/register'                  : UserRegister,
        '/account'                   : UserAccount,
        '/change-email'              : UserChangeEmail,
        '/change-password'           : UserChangePassword,
        '/change-cc'                 : UserChangeCC,
        '/change-billing-date'       : UserChangeBillingDate,
        '/team/:slug'                : TeamSingleScheduleCombined, //TeamSingle,
        '/edit-team/:slug'           : EditTeam,
        '/add-team'                  : AddTeam,
        '/first-team'                : FirstTeam,
        '/edit-schedule/:slug'       : EditSchedule,
    },

    capitalize : (s) => {
        if (typeof s !== 'string'){
            return ''
        }else{
            if(s.indexOf("-") > -1){
                return s.charAt(0).toUpperCase() + s.slice(1, s.indexOf("-")) +  " " + s.charAt(s.indexOf("-")+1).toUpperCase() + s.slice(s.indexOf("-") + 2);
            }else{
                return s.charAt(0).toUpperCase() + s.slice(1)
            }
        }
    },

    // The router code. Takes a URL, checks against the list of supported routes and then renders the corresponding content page.
    router : async () => {
        document.getElementById('loader').classList.remove('loaderFadeOutDown');
        document.getElementById('loader').classList.add('loaderFadeInUp');        
        document.getElementById('body').classList.remove('letsFadeInUp');
        document.getElementById('body').classList.add('letsFadeOutDown');  
        window.scrollTo(0,0); 

        // Hide grr-refresh
        document.getElementById('grrefresh').classList.remove("grr-show");
        document.getElementById('grrefresh').offsetWidth;
        document.getElementById('grrefresh').classList.add( "grr-hide");


        // Get the parsed URl from the addressbar
        let request = Utils.parseRequestURL()

        if(request.slug){
            if(request.slug.charAt(0) !== ":"){
                document.title = "Status Hammer" + " / " + Router.capitalize(request.resource) + " / " + request.slug.toUpperCase();
            }else{
                document.title = "Status Hammer" + " / " + Router.capitalize(request.resource);
            }
        }else if(request.resource){
            document.title = "Status Hammer" + " / " + Router.capitalize(request.resource);
        }else{
            document.title = "Status Hammer";
        }

        // Do we have the cookies we need for the app to work?
        if(request.resource !== "login" && 
            request.resource !== "register" && 
            request.resource !== "forgot" && 
            request.resource !== "reset" && 
            request.resource !== "tos" &&
            request.resource !== "privacy" &&
            request.resource !== "" &&
            !Cookies.getCookie('csrf_access_token') && 
            !Cookies.eraseCookie('csrf_refresh_token')){
            if(request.resource == "reset"){
                // we will need special protocols for reset
            }else{
                Cookies.setCookie('desiredPreLoginPage',window.location.hash,7);
                window.location.hash = Urls.appLogin;    
            }
        }else{
            // Parse the URL and if it has an id part, change it with the string ":id"
            let parsedURL = (request.resource ? '/' + request.resource : '/') + (request.slug ? '/:slug' : '') + (request.verb ? '/' + request.verb : '')

            

            
            // Lazy load view element:
            // const header = null || document.getElementById('header_container');
            const content = null || document.getElementById('page_container');
            const footer = null || document.getElementById('footer_container');
            
            // Render the Header and footer of the page
            // header.innerHTML = await Navbar.render();
            // await Navbar.after_render();
            footer.innerHTML = await Bottombar.render();
            await Bottombar.after_render();
            
            // Get the page from our hash of supported routes.
            // If the parsed URL is not in our list of supported routes, select the 404 page instead
            let page = Router.routes[parsedURL] ? Router.routes[parsedURL] : Error404
            content.innerHTML = await page.render();
            await page.after_render();


            document.getElementById('body').classList.remove('letsFadeOutDown');
            document.getElementById('body').classList.add('letsFadeInUp');   
            document.getElementById('loader').classList.remove('loaderFadeInUp');     
            document.getElementById('loader').classList.add('loaderFadeOutDown');        
        }
        
        

    }
}

export default Router;