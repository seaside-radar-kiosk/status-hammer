const Utils = { 
    // --------------------------------
    //  Parse a url and break it into resource, id and verb
    // --------------------------------
    parseRequestURL : (theLocation = location) => {

        let url = theLocation.hash.slice(1).toLowerCase() || '/';
        let r = url.split("/")
        let request = {
            resource    : null,
            slug        : null,
            verb        : null
        }
        request.resource    = r[1]
        request.slug        = r[2]
        request.verb        = r[3]

        return request
    }

    // --------------------------------
    //  Simple sleep implementation
    // --------------------------------
    , sleep: (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    , grr: (text, type="success") => {
        if(type == "hide"){
            document.getElementById('grr').classList.add('grr-hide');
        }else{
            document.getElementById('grr-tounge').innerHTML = `${text}<div id="grrclose" style="display:inline;" onclick="document.getElementById('grr').classList.add('grr-hide');"><img style="width: 12px;display: inline-block;top: 2px;position: relative;margin-left: 10px;" src="assets/img/grrclose.png"></div>`;
            if(type == "warning"){
                document.getElementById('grr-tounge').classList.remove("grr-error");
                document.getElementById('grr-tounge').classList.add("grr-warning");
            }else if(type == "error"){
                document.getElementById('grr-tounge').classList.add("grr-error");
                document.getElementById('grr-tounge').classList.remove("grr-warning");
            }else{
                document.getElementById('grr-tounge').classList.remove("grr-error");
                document.getElementById('grr-tounge').classList.remove("grr-warning");
            }
            document.getElementById('grr').classList.remove("grr-action");
            document.getElementById('grr').classList.remove("grr-show");
            document.getElementById('grr').classList.remove("grr-hide");
            document.getElementById('grr').offsetWidth;
            if(type == "warning"){
                document.getElementById('grr').classList.add( "grr-show");
            }else if(type == "error"){
                document.getElementById('grr').classList.add( "grr-show");
            }else{
                document.getElementById('grr').classList.add( "grr-action");
            }
        }


    }, timestampNow : () => {
        let options = {  
            weekday: "long", year: "numeric", month: "short",  
            day: "numeric", hour: "2-digit", minute: "2-digit", 
        };  
        let date = new Date();  
        return date.toLocaleTimeString("en-us");
    }

}

export default Utils;