// --------------------------------
//  Define Data Sources
// --------------------------------

import Urls            from '../../services/Urls.js'
import Utils            from '../../services/Utils.js'
import Fetcher        from '../../services/Fetcher.js'

function allTeamsCallback(json){
    return json
}

let getPostsList = async () => {
    
    try {
        const response = await Fetcher.get(Urls.apiAllTeams, allTeamsCallback);
        return response;
    } catch (err) {
        console.log('Error getting documents', err)
    }
}
let teams = ""
let TeamAllTeams = {
    render : async () => {
        teams = await getPostsList();
        let view =  /*html*/`
            <div class="header_container ">
                <div class="header_container_inner">
                    <div class="header_left" style="display: flex;align-items: center;">
                        <a href="${Urls.appRoot}${Urls.appLanding}" style="display: flex;align-items: center;">
                            <img src="/assets/img/icon.png" class="header-logo">
                        </a>
                    </div>
                    <div class="header_right">
                        <a href="${Urls.appRoot}${Urls.appAddTeam}" class=""><img class="header-icon mr-1"
                                src="assets/img/add.png"></a>
                        <a href="${Urls.appRoot}${Urls.appAccount}" class=""><img class="header-icon"
                                src="assets/img/account.png"></a>
                    </div>
                </div>
            </div>
            <style>
                .header_container_inner {}
            
                .scheduling_container_home {
                    border-bottom: 1px solid #E0E3E9;
                    margin-bottom: 25px;
                    width: 100%;
                    display: inline-block;
                    padding: 0px 0;
                    padding-bottom: 25px;
                }
            
                .scheduling_container_home:first-child {
                    // border-top: 1px solid #E0E3E9;
                }
            
                .scheduling_container_home:last-child {
                    border-bottom: none;
                }
            
                .scheduling_container_home {
            
            
                    padding: 25px 0;
            
                }
            
                .scheduling_container_home:first-child {
            
                    // margin-top: 25px;
                    padding: 25px 0;
            
                }
            
            
            
            
                .body_container {
            
                    background: transparent;
                    display: flex;
                    justify-content: space-evenly;
                    flex-flow: wrap;
                    // max-width:100px;
            
                    transform: scale(1);
                    margin: 0 auto;
                    padding-top: 0;
                }
            
                @media screen and (max-width: 1100px) {
                    .scheduling_container_home {
                        width: 100%;
            
                    }
            
                    .body_container {
                        transform: scale(1);
                    }
                }
            
                @media screen and (max-width: 970px) {
                    .body_container {
            
                        // max-width:560px;
                        transform: scale(1);
                    }
            
                    .scheduling_container_home {
                        width: 100%;
                    }
                }
            
                @media screen and (max-width: 600px) {
                    .scheduling_container_home {
                        width: 100%;
                    }
            
                    .body_container {
                        // max-width:410px;
                        justify-content: center;
            
                    }
                }
            
                @media screen and (max-width: 500px) {
                    .scheduling_container_home {
                        width: 100%;
                    }
            
                    .body_container {
                        // margin-top:0px;
                        justify-content: center;
                        transform: scale(1);
                    }
                }
            
                .col {
                    padding: 0px;
                    margin-bottom: 0;
                }
            
                .row {
                    padding-bottom: 0;
                }
            
                .scheduling_container_home.inactive {
                    // border: 1px solid #435264;
                    // background:#242949;
                }
            
                .inactive hr {
                    border-color: #D5D9DE;
                }
            
            
                .inactive a,
                .inactive,
                .inactive a:visited,
                .inactive a:hover {
                    color: #6B7787 !important;
                }
            
                hr {
                    opacity: 0;
                }
            
                .active {
            
                    // background:#fff;
                }
            
                .col.hidden {
                    display: none;
                }
            
            
                .col {
                    min-width: 100%;
                }
            
                .body_container {
                    max-width: 100%;
                }
            
                .header_container_inner {
                    max-width: 100%
                }
            </style>
            <div class="body_container" id="teams_container">
                ${ teams.map(team =>
                /*html*/`
                <a href="${Urls.appRoot}${Urls.appTeam}${team.slug}"
                    class="  ${team.team_is_deleted ? 'hidden' : ''} scheduling_container_home ${team.current_presence ? 'active' : 'inactive'}">
            
                    <div class="row all-team-row">
                        <div class="col truncate" style="width:100%;display:none;">
                            <h2 style="${team.current_presence ? 'color:#1b1b1b' : 'color:#6B7787'} " class="mb-1 team-slug">
                                ${team.slug} ${team.team_is_deleted ? '(DELETED)' : ''} </h2>
                        </div>
            
                        <div class="col" style="min-width:unset;">
                            <div class=" truncate ${team.schedule_enabled ? '' : `no-schedule mb-1`}">
                                <img class="icon" src="assets/img/${team.current_presence ? 'active' : 'inactive'}.png"> <span
                                    style="font-weight:bold;">${team.slug}.slack.com </span>${team.team_is_deleted ? '(DELETED)' :
                                ''} </td>
                            </div>
            
                            <div class=" truncate ${team.schedule_enabled ? 'mb-1' : 'hidden'}">
                                <span id="schedule-active-status" class="" style="font-size:14px;"><img
                                        style="margin-right: 5px;margin-left: 5px;width:12px;position:relative;top:2px;"
                                        src="assets/img/calendar${team.current_presence ? '' : '-inactive'}.png">
                                    ${team.schedule_active && team.current_presence ? "Inactive" : "Active"} @
                                    ${team.schedule_active && team.current_presence ? team.schedule_active_end :
                                    team.schedule_active_start} </span>
                            </div>
                        </div>
            
                        <div class="col" style="min-width:unset;font-size:18px;">
                            <div class=" truncate ${team.schedule_enabled ? '' : `no-schedule mb-1`}">
                                <img class="icon" style="margin-right: 5px;margin-left: 5px;width:20px;position:relative;top:4px;"
                                    src="assets/img/status${team.current_presence ? '' : '-inactive'}.png"> ${team.current_status}
                            </div>
            
                            <div class=" truncate ${team.schedule_enabled ? 'mb-1' : 'hidden'}">
                                <span id="schedule-active-status" class="" style="font-size:14px;"><img
                                        style="margin-right: 5px;margin-left: 5px;width:12px;position:relative;top:2px;"
                                        src="assets/img/calendar${team.current_presence ? '' : '-inactive'}.png">
                                    ${team.schedule_active_status_text}</span>
                            </div>
                        </div>
            
                        <div class="col" style="min-width:unset;font-size:18px;">
                            <div class=" truncate ${team.schedule_enabled ? '' : `no-schedule mb-1`}">
                                <img class="icon" style="margin-right: 5px;margin-left: 5px;width:20px;position:relative;top:4px;"
                                    src="assets/img/notifications${team.current_presence ? '' : '-inactive'}.png">
                                ${team.current_notifications_text}
                            </div>
            
                            <div class=" truncate ${team.schedule_enabled ? 'mb-1' : 'hidden'}">
                                <span id="schedule-active-notifications" class="" style="font-size:14px;"><img
                                        style="margin-right: 5px;margin-left: 5px;width:12px;position:relative;top:2px;"
                                        src="assets/img/calendar${team.current_presence ? '' : '-inactive'}.png">
                                    ${team.schedule_active_notifications_text}</span>
                            </div>
                        </div>
                    </div>
                </a>
                `).join('\n ')}
            </div>
            <p style="font-size:14px;color:#A6AFBA;" class="mb-2">Updated ${Utils.timestampNow()}</p>
        `
        return view
    }
    , after_render: async () => {
        setInterval(async function(){
            let new_teams = await getPostsList()
            if(JSON.stringify(new_teams) === JSON.stringify(teams)){
                console.log("SAME");
            }else{
                console.log("UPDATE");
                teams = new_teams;
                document.getElementById('teams_container').innerHTML = teams.map(team =>
                    /*html*/`
                    <a href="${Urls.appRoot}${Urls.appTeam}${team.slug}"
                        class="  ${team.team_is_deleted ? 'hidden' : ''} scheduling_container_home ${team.current_presence ? 'active' : 'inactive'}">
                
                        <div class="row all-team-row">
                            <div class="col truncate" style="width:100%;display:none;">
                                <h2 style="${team.current_presence ? 'color:#1b1b1b' : 'color:#6B7787'} " class="mb-1 team-slug">
                                    ${team.slug} ${team.team_is_deleted ? '(DELETED)' : ''} </h2>
                            </div>
                
                            <div class="col" style="min-width:unset;">
                                <div class=" truncate ${team.schedule_enabled ? '' : `no-schedule mb-1`}">
                                    <img class="icon" src="assets/img/${team.current_presence ? 'active' : 'inactive'}.png"> <span
                                        style="font-weight:bold;">${team.slug}.slack.com </span>${team.team_is_deleted ? '(DELETED)' :
                                    ''} </td>
                                </div>
                
                                <div class=" truncate ${team.schedule_enabled ? 'mb-1' : 'hidden'}">
                                    <span id="schedule-active-status" class="" style="font-size:14px;"><img
                                            style="margin-right: 5px;margin-left: 5px;width:12px;position:relative;top:2px;"
                                            src="assets/img/calendar${team.current_presence ? '' : '-inactive'}.png">
                                        ${team.schedule_active && team.current_presence ? "Inactive" : "Active"} @
                                        ${team.schedule_active && team.current_presence ? team.schedule_active_end :
                                        team.schedule_active_start} </span>
                                </div>
                            </div>
                
                            <div class="col" style="min-width:unset;font-size:18px;">
                                <div class=" truncate ${team.schedule_enabled ? '' : `no-schedule mb-1`}">
                                    <img class="icon" style="margin-right: 5px;margin-left: 5px;width:20px;position:relative;top:4px;"
                                        src="assets/img/status${team.current_presence ? '' : '-inactive'}.png"> ${team.current_status}
                                </div>
                
                                <div class=" truncate ${team.schedule_enabled ? 'mb-1' : 'hidden'}">
                                    <span id="schedule-active-status" class="" style="font-size:14px;"><img
                                            style="margin-right: 5px;margin-left: 5px;width:12px;position:relative;top:2px;"
                                            src="assets/img/calendar${team.current_presence ? '' : '-inactive'}.png">
                                        ${team.schedule_active_status_text}</span>
                                </div>
                            </div>
                
                            <div class="col" style="min-width:unset;font-size:18px;">
                                <div class=" truncate ${team.schedule_enabled ? '' : `no-schedule mb-1`}">
                                    <img class="icon" style="margin-right: 5px;margin-left: 5px;width:20px;position:relative;top:4px;"
                                        src="assets/img/notifications${team.current_presence ? '' : '-inactive'}.png">
                                    ${team.current_notifications_text}
                                </div>
                
                                <div class=" truncate ${team.schedule_enabled ? 'mb-1' : 'hidden'}">
                                    <span id="schedule-active-notifications" class="" style="font-size:14px;"><img
                                            style="margin-right: 5px;margin-left: 5px;width:12px;position:relative;top:2px;"
                                            src="assets/img/calendar${team.current_presence ? '' : '-inactive'}.png">
                                        ${team.schedule_active_notifications_text}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                    `).join('\n ');
            }
        }, 60000)
        
    }
}
export default TeamAllTeams;