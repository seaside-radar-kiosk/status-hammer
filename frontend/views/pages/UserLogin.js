import Cookies      from '../../services/Cookies.js'
import Urls         from '../../services/Urls.js'
import Fetcher      from '../../services/Fetcher.js'
import Helpers      from '../../services/Helpers.js'
import Utils        from '../../services/Utils.js'

let setCookiesAndForward = (json) => {
    if(json['csrf_access_token'] && json['csrf_refresh_token']){
        setTimeout(function(){
            Cookies.setCookie('csrf_access_token', json['csrf_access_token'],7);
            Cookies.setCookie('csrf_refresh_token', json['csrf_refresh_token'],7);
            if(Cookies.getCookie('desiredPreLoginPage')){
                let hash = Cookies.getCookie('desiredPreLoginPage');
                Cookies.eraseCookie('desiredPreLoginPage');
                window.location.hash = hash;
            }else{
                window.location.hash = Urls.appHome;
            }
    
        }, 500);

   }else{
       Cookies.eraseCookie('csrf_access_token');
       Cookies.eraseCookie('csrf_refresh_token');
       document.getElementById("login_submit_btn").classList.remove('action');
       Utils.grr(json.message, "error");
   }
};

let postCredentials = async (username, password) => {
    let body = JSON.stringify({
        'username':username,
        'password':password
    })
    Fetcher.postWithoutRefresh(Urls.apiLogin, body, setCookiesAndForward);
}

let UserLogin = {

    render: async () => {
        // let team = await getTeam()
        return /*html*/ `
        <style>
        .body_container{
            margin-bottom:0;
            padding-top:0;
        }
        .header_container{
            padding-bottom: 10px;
            // margin-left: 25px;  
        }
        #page_container{
            justify-content: center;
        }
        input[type="text"], input[type="email"], input[type="password"], input[type="time"]{
            width:calc(100% - 20px) !important
        }

        
        </style>
            <div class="header_container ">
                <div class="header_container_inner" style="">
                    <div class="header_left" style="display: flex;align-items: center;"> 
                        <div class="row">
                            <div class="col">
                                <a href="${Urls.appRoot}${Urls.appLanding}"><img src="/assets/img/icon.png" class="header-logo"></a>
                                
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
            <div class="body_container skinny very">
                <section class="page">
                    <div class="">
                        <form>
                            
                            <div class="row mb-1">
                                <div class="col">
                                <h1 class="">Login</h1>
                                </div>
                            </div>     
                            <div class="row ">
                                <div class="col">
                                    <h3 >Status Hammer Email</h3>
                                    <input class="input" id="email_input" type="email" placeholder="Enter your Email" value="">
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col">
                                    <h3 >Password</h3>
                                    <input class="input" id="pass_input" type="password" placeholder="Enter a Password" value="">
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col">
                                    <button class="btn btn-border is-primary" id="login_submit_btn">
                                    Login
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <a href="#/register" class="small">> Don't have an account? Click here to register.</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <a href="${Urls.appRoot}${Urls.appForgot}" class="small">> Forgot password? Click here to reset.</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        `
    }
    // All the code related to DOM interactions and controls go in here.
    // This is a separate call as these can be logined only after the DOM has been painted
    , after_render: async () => {
        document.getElementById("login_submit_btn").addEventListener ("click",  () => {
            Utils.grr(``, "hide");
            let email       = document.getElementById("email_input");
            let pass        = document.getElementById("pass_input");
            if (email.value =='' | pass.value == '') {
                Utils.grr('All fields are required', "warning");
            }else if (!Helpers.validateEmail(email.value)) {
                Utils.grr('Email not valid', "warning");
            }else {
                //alert(`User with email ${email.value} was successfully submitted!`)
                document.getElementById("login_submit_btn").classList.add('action');
                postCredentials(email.value, pass.value);
                
            }    
        })
    }
}

export default UserLogin;