import Fetcher        from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'
import Urls            from '../../services/Urls.js'

function singleTeamCallback(json){
    return json
}

let getAccount = async () => {
    try {
        const response = await Fetcher.get(Urls.apiAccount, singleTeamCallback);
        return response;
    } catch (err) {
        console.log('Error getting documents', err)
    }
}

let setCookiesAndForward = (json) => {
    if(json.success){
        Utils.grr("Password udpated")
        window.location.hash = Urls.appAccount
    }else{
        document.getElementById("save_btn").classList.remove('action');
        Utils.grr(json.message, "error");
    }
};

let postCCUpdate = async (username, newPassword, newPasswordCopy, password) => {
    let body = JSON.stringify({
        'new-password':newPassword,
        'new-password-copy':newPasswordCopy,
        'username':username,
        'password':password
    })
    Fetcher.post(Urls.apiAccount, body, setCookiesAndForward);
}

let UserChangeCC = {

    render : async () => {
        let request = Utils.parseRequestURL()
        let account = await getAccount()
        
        return /*html*/`
    
            <div class="header_container ">
                <div class="header_container_inner">
                    <div class="header_left" style="display: flex;align-items: center;"> 
                        <a href="${Urls.appRoot}${Urls.appAccount}" style="display: flex;align-items: center;">
                            <img class="back-arrow" src="assets/img/chevron.png">
                            <h1 class="">
                            <span class="">Change Password</span>
                                
                            </h1>
                        </a>
                    </div>
                </div>
            </div>
            <div class="body_container">
                <form>
                    <section class="page">
                        <div class="row ">
                            <div class="col">
                                <blockquote>Update your password here</blockquote>
                            </div>
                        </div>     
                        <div class="row">
                            <div class="col">
                                <h2>Current Password</h2>
                            </div>
                        </div>    
                        <div class="row ">
                            <div class="col">
                            <input type="password" id="password" value="" placeholder="Enter current password">
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col">
                                <h2>New Password</h2>
                            </div>
                        </div>    
                        <div class="row ">
                            <div class="col">
                                <input type="password" id="new-password-input" value="" placeholder="Enter new password">
                                <input type="hidden" id="username-input" value="${account.username}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h2>New Password Again</h2>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col">
                                <input type="password" id="new-password-input-copy" value="" placeholder="Enter new password again">
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col">
                                <button class="btn btn-border is-primary" id="save_btn">Save</button>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        `
    }
    , after_render: async () => {
        document.getElementById("save_btn").addEventListener ("click",  () => {
            Utils.grr(``, "hide");
            let username       = document.getElementById("username-input");
            let newpassword       = document.getElementById("new-password-input");
            let newpasswordcopy       = document.getElementById("new-password-input-copy");
            let password       = document.getElementById("password");
            if (newpassword.value != newpasswordcopy.value) {
                Utils.grr("The new password fields don't match", "warning");
            } else if (username.value =='' | password.value == '' | newpasswordcopy.value == '' | newpassword.value == '') {
                Utils.grr("The fields cannot be empty", "warning");
            }else {
                //alert(`User with email ${email.value} was successfully submitted!`)
                document.getElementById("save_btn").classList.add('action');
                postCCUpdate(username.value, newpassword.value, newpasswordcopy.value, password.value);
                
            }    
        })
    }
}

export default UserChangeCC;