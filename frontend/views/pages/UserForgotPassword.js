import Cookies        from '../../services/Cookies.js'
import Helpers      from '../../services/Helpers.js'
import Urls            from '../../services/Urls.js'
import Fetcher            from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'

let setCookiesAndForward = (json) => {
    if(json['success']){
        window.location.hash = Urls.appLogin;
        Utils.grr('Check your email to reset your password.');
   }else{
       Utils.grr(json['success'], 'danger');
   }
};

let postCredentials = async (username, password) => {
    let body = JSON.stringify({
        'username':username,
    })
    Fetcher.postWithoutRefresh(Urls.apiForgot, body, setCookiesAndForward);
}

let UserForgotPassword = {
    
    render: async () => {
        return /*html*/ `
        <style>
        .body_container{
            margin-bottom:0;
        }
        #page_container{
            justify-content: center;
        }
        input[type="text"], input[type="email"], input[type="password"], input[type="time"]{
            width:calc(100% - 20px) !important
        }
        </style>
        <div class="body_container skinny very">
            <section class="page">
                <div class="">
                    <form>
                        <div class="row">
                            <div class="col">
                                <a href="${Urls.appRoot}${Urls.appLanding}"><img src="/assets/img/icon.png" class="header-logo"></a>
                                
                            </div>
                        </div>  
                        <div class="row mb-1">
                            <div class="col" >
                                
                                <h1 class="team-slug">Password Reset</h1>
                            </div>
                        </div>      
                        <div class="row mb-1">
                            <div class="col">
                                
                                <blockquote>Enter your Status Hammer email address, and we'll send you a password reset link</blockquote>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col">
                                <h3 >Status Hammer Email</h3>
                                <input class="input" id="email_input" type="email" placeholder="Enter your Email" value="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button class="btn btn-border is-primary" id="login_submit_btn">
                                Email password reset
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="error small" id="warnings"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <a href="${Urls.appRoot}${Urls.appLogin}" class="small">> Nevermind, take me back to login.</a>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
        `
    }
    // All the code related to DOM interactions and controls go in here.
    // This is a separate call as these can be logined only after the DOM has been painted
    , after_render: async () => {
        document.getElementById("login_submit_btn").addEventListener ("click",  () => {
            let email       = document.getElementById("email_input");
            if (email.value =='') {
                Utils.grr('All fields required.', "warning");
            }else if (!Helpers.validateEmail(email.value)) {
                Utils.grr('Email not valid', "warning");
            }else {
                //alert(`User with email ${email.value} was successfully submitted!`)
                postCredentials(email.value);
                
            }    
        })
    }
}

export default UserForgotPassword;