import Fetcher        from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'
import Urls            from '../../services/Urls.js'

function singleTeamCallback(json){
    return json
}

let getAddTeamData = async () => {
    try {
        const response = await Fetcher.get(Urls.apiAddTeamData , singleTeamCallback);
        return response;
    } catch (err) {
        console.log('Error getting documents', err)
    }
}

let postResponse = (json) => {
    if(json['success']){
        window.location.hash = Urls.appTeam + json.slug
   }else{
        document.getElementById("add-team").classList.remove('action');
        document.getElementById('loader').classList.remove('loaderFadeInUp');     
        document.getElementById('loader').classList.add('loaderFadeOutDown');   
        console.log("ERROR!");
        Utils.grr(json.message, "error");
   }
};

let addTeam = async (username, password, team, timezone) => {
    let body = JSON.stringify({
        'username':username,
        'password':password,
        'slug':team,
        'teamTimezone': timezone
    })
    Fetcher.post(Urls.apiAddTeam, body, postResponse);
}

let AddTeam = {

    render : async () => {        
        let addTeamData = await getAddTeamData()

        return /*html*/`
            <div class="header_container ">
                <div class="header_container_inner" >
                    <div class="header_left" style="display: flex;align-items: center;"> 
                            <a href="${Urls.appRoot}${Urls.appHome}" style="display: flex;align-items: center;">
                                <img class="back-arrow" src="assets/img/chevron.png">
                                <h1 class="">
                                    
                                    <span class="">Add Team</span>
                                </h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="body_container">
                <form>
                    <section class="page">      
                        <div class="row">
                            <div class="col">
                                <h2>Team URL</h2>
                            </div>
                        </div>    
                        <div class="row mb-1">
                            <div class="col">
                                <input id="team-name" type="text" placeholder="Enter Team Url" style="width: calc(480px - 110px);max-width: calc(90% - 110px);"> .slack.com
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h2>Team Username</h2>
                            </div>
                        </div>    
                        <div class="row mb-1">
                            <div class="col">
                                <input id="email_input" type="text" placeholder="Enter Team Username">
                            </div>
                        </div>
                        <div class="row with-checkbox">
                            <div class="col">
                                <h2>Team Password</h2>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col">
                                <input id="pass_input" type="password" placeholder="Enter Team Password">
                            </div>
                        </div>
                        <div class="row with-checkbox">
                            <div class="col">
                                <h2>Team Timezone</h2>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col">
                                <select name="team-timezone" id="team-timezone" class="select-css">
                                    <option value="-12.0">(GMT -12:00) Eniwetok, Kwajalein</option>
                                    <option value="-11.0">(GMT -11:00) Midway Island, Samoa</option>
                                    <option value="-10.0">(GMT -10:00) Hawaii</option>
                                    <option value="-9.0">(GMT -9:00) Alaska</option>
                                    <option value="-8.0">(GMT -8:00) Pacific Time (US &amp; Canada)</option>
                                    <option value="-7.0">(GMT -7:00) Mountain Time (US &amp; Canada)</option>
                                    <option value="-6.0">(GMT -6:00) Central Time (US &amp; Canada), Mexico City</option>
                                    <option value="-5.0">(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
                                    <option value="-4.0">(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>
                                    <option value="-3.5">(GMT -3:30) Newfoundland</option>
                                    <option value="-3.0">(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>
                                    <option value="-2.0">(GMT -2:00) Mid-Atlantic</option>
                                    <option value="-1.0">(GMT -1:00 hour) Azores, Cape Verde Islands</option>
                                    <option value="0.0">(GMT) Western Europe Time, London, Lisbon, Casablanca</option>
                                    <option value="1.0">(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris</option>
                                    <option value="2.0">(GMT +2:00) Kaliningrad, South Africa</option>
                                    <option value="3.0">(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
                                    <option value="3.5">(GMT +3:30) Tehran</option>
                                    <option value="4.0">(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
                                    <option value="4.5">(GMT +4:30) Kabul</option>
                                    <option value="5.0">(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
                                    <option value="5.5">(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                                    <option value="5.75">(GMT +5:45) Kathmandu</option>
                                    <option value="6.0">(GMT +6:00) Almaty, Dhaka, Colombo</option>
                                    <option value="7.0">(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
                                    <option value="8.0">(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>
                                    <option value="9.0">(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
                                    <option value="9.5">(GMT +9:30) Adelaide, Darwin</option>
                                    <option value="10.0">(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>
                                    <option value="11.0">(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>
                                    <option value="12.0">(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col">
                                <blockquote class="mb-1">Your credit card will be $10.00 charged immediately</blockquote>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <a id="add-team" class="btn btn-border">Add Team</a>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        `
    }
    , after_render: async () => {
        document.getElementById("add-team").addEventListener ("click",  () => {
            Utils.grr(``, "hide");
            let team       = document.getElementById("team-name");
            let email       = document.getElementById("email_input");
            let pass        = document.getElementById("pass_input");
            if (email.value =='' | pass.value == '' | team.value) {
                warnings.classList.add("active");
                warnings.innerHTML = `The fields cannot be empty`;
            }else if(team.value.indexOf(' ') >= 0){
                warnings.classList.add("active");
                warnings.innerHTML = `Team name cannot have spaces`;
            }else {
                //alert(`User with email ${email.value} was successfully submitted!`)
                document.getElementById('loader').classList.remove('loaderFadeOutDown');
                document.getElementById('loader').classList.add('loaderFadeInUp');  
                document.getElementById("add-team").classList.add('action');
                let timezone = document.getElementById("team-timezone");
                addTeam(email.value, pass.value, team.value, timezone.value);
            }    
        });
    }
}

export default AddTeam;