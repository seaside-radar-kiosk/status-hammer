import Fetcher        from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'
import Urls            from '../../services/Urls.js'
import Router       from '../../services/Router.js'

function singleTeamCallback(json){
    return json
}

function selectElement(id, valueToSelect) {    
    let element = document.getElementById(id);
    element.value = valueToSelect;
}

function setScheduleElement(checkbox){
    if (checkbox) {
        document.getElementById('schedule-block').classList.remove('schedule-disabled');
        document.getElementById('exception-block').classList.remove('schedule-hide');
        document.getElementById('schedule-container').classList.remove('schedule-hide');        
    }else {
        document.getElementById('schedule-block').classList.add('schedule-disabled');  
        document.getElementById('exception-block').classList.add('schedule-hide');  
        document.getElementById('schedule-container').classList.add('schedule-hide');        
    }
}


let deleteResponse = (json) => {
    if(json['success']){
        Utils.grr("Team deactivated")
        window.location.hash = Urls.appHome
   }else{
    Utils.grr(json.message, "error");
   }
};


let deleteTeam = async (team) => {
    Fetcher.delete(Urls.apiSingleTeam + team, deleteResponse);
}


let restoreResponse = (json) => {
    if(json['success']){
        Utils.grr("Team was restored")
        hideRestore();
   }else{
        Utils.grr(json.message, "error");
   }
};
let restoreTeam = async (team) => {
    let body = JSON.stringify({
        'setBool':'team_is_deleted',
        'slug':team,
    })
    Fetcher.post(Urls.apiSingleTeam + team, body, restoreResponse);
}

let getTeam = async (slug) => {
    try {
        const response = await Fetcher.get(Urls.apiSingleTeam + slug, singleTeamCallback);
        return response;
    } catch (err) {
        console.log('Error getting documents', err)
    }
}

let updatePage = async (json) => {
    const content = null || document.getElementById('page_container');
    content.innerHTML = await TeamSingleScheduleCombined.render();
    await TeamSingleScheduleCombined.after_render();
};

let postPresenceActiveUpdateCallback = (json) => {
    if(json.success){
        let body = document.getElementById('body_container');
        let checkbox       = document.getElementById("presence-checkbox");
        checkbox.checked = json.data.presenceActive;
        if(checkbox.checked){
            body.classList.remove('inactive');
        }else{
            body.classList.add('inactive');
        }
        setTimeout(function(){
            updatePage();
        },500);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postPresenceActiveUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        'presenceActive': checkbox,
        'slug':slug,
        'setBool': 'presenceActive'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postPresenceActiveUpdateCallback);
}

let postDoNotDisturbActiveUpdateCallback = (json) => {
    if(json.success){
        let checkbox       = document.getElementById("donotdisturb-checkbox");
        checkbox.checked = json.data.doNotDisturbActive;
        setTimeout(function(){
            updatePage();
        },500);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};


let postDoNotDisturbActiveUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        'doNotDisturbActive': checkbox,
        'slug':slug,
        'setBool': 'doNotDisturbActive'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDoNotDisturbActiveUpdateCallback);
}

let postScheduleActiveUpdateCallback = (json) => {
    if(json.success){
        let checkbox       = document.getElementById("schedule-checkbox");
        checkbox.checked = json.data.scheduleActive;
        setScheduleElement(checkbox.checked)
        setTimeout(function(){
            updatePage();
        },500);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postScheduleActiveUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        'scheduleActive': checkbox,
        'slug':slug,
        'setBool': 'scheduleActive'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postScheduleActiveUpdateCallback);
}

let postCurrentNotificationsTypeUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postCurrentNotificationsTypeUpdate = async (slug, radio) => {
    let body = JSON.stringify({
        'currentNotificationsType': radio,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postCurrentNotificationsTypeUpdateCallback);
}


let postTeamTimezoneUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postTeamTimezoneUpdate = async (slug, select) => {
    let body = JSON.stringify({
        'teamTimezone': select,
        'setTimezone': 'teamTimezone',
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postTeamTimezoneUpdateCallback);
}

let postDoNotDisturbStartUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postDoNotDisturbStartUpdate = async (slug, select) => {
    let body = JSON.stringify({
        'doNotDisturbStart': select,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDoNotDisturbStartUpdateCallback);
}


let postDoNotDisturbEndUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postDoNotDisturbEndUpdate = async (slug, select) => {
    let body = JSON.stringify({
        'doNotDisturbEnd': select,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDoNotDisturbEndUpdateCallback);
}

function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
}

const setExceptionDate = (s) => {
    var d = new Date(s +":59");
    // parts = s.match(/(\d+)-(\d+)-(\d+)/),
    // year = parseInt(parts[1], 10),
    // month = parseInt(parts[2], 10) - 1,
    // day = parseInt(parts[3], 10),
    // hours = 23,
    // minutes = 59;
    // d.setFullYear(year);
    // d.setMonth(month);
    // d.setDate(day);
    
    // d.setMinutes(minutes);
    if(isValidDate(d)){
        d.setHours(d.getHours() - (d.getTimezoneOffset()/60))
        return d
    }else{
        return false
    }

}

let postExceptionCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postExceptionNew = async (slug, start_date, end_date) => {
    let body = JSON.stringify({
        'exceptionNew': true,
        'exceptionStartDate': start_date,
        'exceptionEndDate': end_date,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postExceptionCallback);
}

let postExceptionUpdate = async (slug, exception_id, start_date, end_date) => {
    let body = JSON.stringify({
        'exceptionUpdate': exception_id,
        'exceptionStartDate': start_date,
        'exceptionEndDate': end_date,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postExceptionCallback);
}

let postExceptionDelete = async (slug, exception_id) => {
    let body = JSON.stringify({
        'exceptionDelete': exception_id,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postExceptionCallback);
}

let postCurrentStatusUpdateCallback = (json) => {
    if(json.success){
        Utils.grr("Status Updated @ " + timestampNow());
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        Utils.grr(json.message, "error");
    }
};

let postCurrentStatusUpdate = async () => {
    let textarea = document.getElementById("current-status-textarea");
    let slug       = document.getElementById("slug-input").value;
    if (slug.value =='') {
        alert (`The fields cannot be empty`)
    }else {
        let body = JSON.stringify({
            'currentStatus': textarea.value,
            'slug':slug,
        })
        Fetcher.post(Urls.apiSingleTeam + slug, body, postCurrentStatusUpdateCallback);
    }
    
}

let postCurrentNotificationKeywordsUpdateCallback = (json) => {
    if(json.success){
        Utils.grr("Keywords updated @ " + timestampNow());
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        Utils.grr(json.message, "error");
    }
};

let postCurrentNotificationKeywordsUpdate = async () => {
    let textarea = document.getElementById("current-notifications-keywords-textarea");
    let slug       = document.getElementById("slug-input").value;
    if (slug.value =='') {
        alert (`The fields cannot be empty`)
    }else {
        let body = JSON.stringify({
            'currentNotificationsKeywords': textarea.value,
            'slug':slug,
        })
        Fetcher.post(Urls.apiSingleTeam + slug, body, postCurrentNotificationKeywordsUpdateCallback);
    }
    
}

const timestampNow = () => {
    let options = {  
        weekday: "long", year: "numeric", month: "short",  
        day: "numeric", hour: "2-digit", minute: "2-digit", 
    };  
    let date = new Date();  
    return date.toLocaleTimeString("en-us");
}

let delayIntervalsKeywords = [];
let lastDelaySavedKeywords = "";

const delaySaveKeywords = (action) => {
    for (let i = 0; i < delayIntervalsKeywords.length; i++){
        clearInterval(delayIntervalsKeywords[i]);
    }
    delayIntervalsKeywords = [];
    let intervalId = setTimeout(() => {
        lastDelaySavedKeywords = timestampNow()
        action();
    }, 2000);
    delayIntervalsKeywords.push(intervalId);    
}

let delayIntervalsStatus = [];
let lastDelaySavedStatus = "";

const delaySaveStatus = (action) => {
    for (let i = 0; i < delayIntervalsStatus.length; i++){
        clearInterval(delayIntervalsStatus[i]);
    }
    delayIntervalsStatus = [];
    let intervalId = setTimeout(() => {
        lastDelaySavedStatus = timestampNow()
        action();
    }, 2000);
    delayIntervalsStatus.push(intervalId);    
}

let showModal = () => {
    document.getElementById("are-you-sure").classList.remove("hidden");
}

let hideModal = () => {
    document.getElementById("are-you-sure").classList.add("hidden");
}

let hideRestore = () => {
    document.getElementById("restore-team").classList.add("hidden");
}

let postDayUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postActiveSundayUpdate = async (slug, checkbox) => {
    
    let body = JSON.stringify({
        "scheduleActiveSunday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveSunday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveMondayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveMonday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveMonday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveTuesdayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveTuesday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveTuesday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveWednesdayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveWednesday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveWednesday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveThursdayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveThursday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveThursday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveFridayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveFriday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveFriday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveSaturdayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveSaturday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveSaturday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}
const hours12 = (date) => { 
    return (date.getHours() + 24) % 12 || 12; 
}
const setStartEndTimes = (s) => {
    var d = new Date(),
    parts = s.match(/(\d+)\:(\d+)/),
    hours = parseInt(parts[1], 10),
    minutes = parseInt(parts[2], 10);
    d.setHours(hours - (d.getTimezoneOffset()/60));
    d.setMinutes(minutes);
    return d
}

const setStartEndTimesGET = (s) => {
    var d = new Date(),
    parts = s.match(/(\d+)\:(\d+)/),
    hours = parseInt(parts[1], 10),
    minutes = parseInt(parts[2], 10);
    
    d.setHours(hours);
    d.setMinutes(minutes);
    return d
}

let postActiveStartTimeUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postActiveStartTimeUpdate = async (slug, time) => {
    let body = JSON.stringify({
        'scheduleActiveStart': time,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postActiveStartTimeUpdateCallback);
}

let postActiveEndTimeUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postActiveEndTimeUpdate = async (slug, time) => {
    let body = JSON.stringify({
        'scheduleActiveEnd': time,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postActiveEndTimeUpdateCallback);
}

let team = {"slug":'', "current_presence":true}

let TeamSingleScheduleCombined = {

    render : async () => {
        let request = Utils.parseRequestURL()
        let teamResponse = await getTeam(request.slug)
        if(!teamResponse){
            window.location.hash = Urls.appHome;
        }else{
            team = teamResponse;
        }
        return /*html*/`
        <style>
            .mb-2 {
                border-bottom: 1px solid #E0E3E9;
                padding-bottom: 25px !important;
                margin-bottom: 25px !important;
            }
        
            .no-border td {
                border-bottom: none;
            }
        
            .body_container {
                // background-color:rgba(255,255,255,0.95);
            }
        
            .scheduling_container {
                margin-left: -20px;
                margin-right: -20px;
            }
        
            body {
                overflow-x: hidden;
            }
        
            #body {
                width: calc(100% - 20px);
            }
            .badge{
                font-size: 12px;
                background:
                #0060BF;
                padding: 5px 10px;
                border-radius: 24px;
                color:
                #fff;
                font-weight: bold;
            }
            .badge.big{
                font-size:16px;
            }
            .badge.errored{
                background:#FD2D55;
            }
        </style>
        <div class="header_container ">
            <div class="header_container_inner" style="">
                <div class="header_left" style="display: flex;align-items: center;">
                    <a href="${Urls.appRoot}${Urls.appHome}" style="display: flex;align-items: center;">
                        <img class="back-arrow" src="assets/img/chevron.png">
                        <h1 class="truncate ">
                            <span class="">${team.slug}</span>
                        </h1>
                    </a>
        
                </div>
                <div class="header_right">
                    <a href="${Urls.appRoot}${Urls.appEditTeam}${team.uuid}" class="" style="margin-left:20px"><img
                            class="header-icon" src="assets/img/settings.png"></a>
                </div>
            </div>
        </div>
        <div id="body_container" class="body_container" style="${team.client_status == 'errored' || team.client_status == 'in-progress' || team.client_status == 'xcompleted' ? '' : 'display:none;'}">
            <section class="page" style="border-bottom:none;">
                
                <div class="row">
        
                    <div class="col">
                        ${team.client_status == 'errored' || team.client_status == 'xcompleted' ? `  <blockquote class=""> <span class="badge errored">Server Error.</span> An email has been sent to our support team and we're on it, but contact us if you want more info!</blockquote>` : ''} ${team.client_status == 'in-progress' || team.client_status == 'xcompleted' ? `<span class="badge big">Server Provisioning in Progress<div class="spinner_small"><div class="right-side_small"><div class="bar_small"></div></div><div class="left-side_small"><div class="bar_small"></div></div></div></span>` : ''}${team.team_is_deleted ? '(DELETED)' : ''} 
                    </div>
                </div>
            </section>
        </div>
        <div id="body_container" class="body_container ${team.current_presence ? '' : 'inactive'}">
            <section class="page mb-1" style="border-bottom:none;">
                
                <div class="row with-checkbox">
                    <div class="col">
                        <span style="display: flex;align-items: center;"><img class="team-icon "
                                src="assets/img/${team.current_presence ? 'active' : 'inactive'}.png">
                            <h2>Presence</h2>
                        </span>
                        <input type="hidden" id="slug-input" value="${team.uuid}">
                    </div>
                    <div class="col text-right  no-grow">
                        <label id="presence-switch" class="switch">
                            <input id="presence-checkbox" type="checkbox" ${team.current_presence ? 'checked' : '' }>
                            <span class="slider presence round"></span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col ${team.current_presence_text? '' : 'hidden'}">
                        <span class=""> ${team.current_presence_text}</span> <span
                            class=" ${team.schedule_enabled ? '' : 'hidden'}">until @ ${team.schedule_enabled &&
                            team.current_presence ? team.schedule_active_end : team.schedule_active_start} <img
                                style="width:16px;position:relative;top:2px;" src="assets/img/calendar.png"></span>
                    </div>
                </div>
            </section>
            <section class="page mb-2 d-none" style="display:none;">
                <div class="row with-checkbox">
                    <div class="col">
                        <span style="display: flex;align-items: center;"><img class="team-icon " src="assets/img/status.png">
                            <h2>Status</h2>
                        </span>
                    </div>
                </div>
                <div class="row" style="margin-bottom:0;">
                    <div class="col" style="margin-bottom:0;">
                        <p class="">
                            <input class="status-input" type="text" id="current-status-textarea" value="${team.current_status}">
                        </p>
                        <p class="mt-1 ${team.schedule_enabled ? '' : 'hidden'}"><img
                                style="width:16px;position:relative;top:2px;" src="assets/img/calendar.png"> @
                            ${team.schedule_active ? team.schedule_active_end : team.schedule_active_start} -
                            ${team.schedule_active ? team.schedule_inactive_status_text : team.schedule_active_status_text}</p>
                    </div>
                </div>
                <div class="row  mb-1">
                    <div class="col">
                        <blockquote>If you start your status with an emoji, this will be used to set the emoji next to your name. Otherwise, your status emoji will be 💬.</blockquote>
                    </div>
                </div>
            </section>
            
            <div class="scheduling_container">
                <section class="page schedule schedule-disabled" id="schedule-block">
                    <div class="row with-checkbox">
                        <div class="col">
                            <a href="#/edit-schedule/${team.slug}" style="display: flex;align-items: center;"><img
                                    class="team-icon " src="assets/img/calendar.png">
                                <h2>Schedule </h2>
                            </a>
                        </div>
                        <div class="col text-right  no-grow">
                            <label id="schedule-switch" class="switch">
                                <input id="schedule-checkbox" type="checkbox" ${team.schedule_enabled ? 'checked' : '' }>
                                <span class="slider schedule round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="row schedule-hide" id="schedule-container">
                        <div class="col">
                        <div class="row ">
                        <div class="col">
                            <h2><span class="highlight-active">Active</span> Days</h2>
                        </div>
                    </div>
                    <div class="row mb-1">
                        <div class="col">
                            <input type="checkbox" id="weekday-sunday" name="weekday-sunday" ${team.schedule_active_sunday ? 'checked' : ''}>   
                            <label for="weekday-sunday">Sunday</label>
                            <input type="checkbox" id="weekday-monday" name="weekday-monday" ${team.schedule_active_monday ? 'checked' : ''}>   
                            <label for="weekday-monday">Monday</label>
                            <input type="checkbox" id="weekday-tuesday" name="weekday-tuesday" ${team.schedule_active_tuesday ? 'checked' : ''}>   
                            <label for="weekday-tuesday">Tuesday</label>
                            <input type="checkbox" id="weekday-wednesday" name="weekday-wednesday" ${team.schedule_active_wednesday ? 'checked' : ''}>   
                            <label for="weekday-wednesday">Wednesday</label>
                            <input type="checkbox" id="weekday-thursday" name="weekday-thursday" ${team.schedule_active_thursday ? 'checked' : ''}>   
                            <label for="weekday-thursday">Thursday</label>
                            <input type="checkbox" id="weekday-friday" name="weekday-friday" ${team.schedule_active_friday ? 'checked' : ''}>   
                            <label for="weekday-friday">Friday</label>
                            <input type="checkbox" id="weekday-saturday" name="weekday-saturday" ${team.schedule_active_saturday ? 'checked' : ''}>   
                            <label for="weekday-saturday">Saturday</label>    
                        </div>
                    </div>    
                    <div class="row mt-2">
                        <div class="col">
                            <h2><span class="highlight-active">Active</span> Hours</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <table>
                                <tr>
                                    <td>
                                        Start: 
                                    </td>
                                    <td >
                                        <select id="schedule-active-start-hour" class="select-css"> 
                                            <option value="1">01</option>
                                            <option value="2">02</option>
                                            <option value="3">03</option>
                                            <option value="4">04</option>
                                            <option value="5">05</option>
                                            <option value="6">06</option>
                                            <option value="7">07</option>
                                            <option value="8">08</option>
                                            <option value="9">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                        </select> :
                                        <select id="schedule-active-start-minute" class="select-css"> 
                                            <option value="0">00</option>
                                            <option value="5">05</option>
                                            <option value="10">10</option>
                                            <option value="15">15</option>
                                            <option value="20">20</option>
                                            <option value="25">25</option>
                                            <option value="30">30</option>
                                            <option value="35">35</option>
                                            <option value="40">40</option>
                                            <option value="45">45</option>
                                            <option value="50">50</option>
                                            <option value="55">55</option>
                                        </select> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        End: 
                                    </td>
                                    <td >
                                        <select id="schedule-active-end-hour" class="select-css"> 
                                            <option value="0">00</option>
                                            <option value="1">01</option>
                                            <option value="2">02</option>
                                            <option value="3">03</option>
                                            <option value="4">04</option>
                                            <option value="5">05</option>
                                            <option value="6">06</option>
                                            <option value="7">07</option>
                                            <option value="8">08</option>
                                            <option value="9">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                        </select> :
                                        <select id="schedule-active-end-minute" class="select-css"> 
                                            <option value="0">00</option>
                                            <option value="5">05</option>
                                            <option value="10">10</option>
                                            <option value="15">15</option>
                                            <option value="20">20</option>
                                            <option value="25">25</option>
                                            <option value="30">30</option>
                                            <option value="35">35</option>
                                            <option value="40">40</option>
                                            <option value="45">45</option>
                                            <option value="50">50</option>
                                            <option value="55">55</option>
                                        </select> 
                                    </td>
                            </tr>
                            <tr>
                                    <td>
                                        Team Timezone: 
                                    </td>
                                    <td>
                                        <select name="team-timezone" id="team-timezone" class="select-css">
                                            <option value="-12.0">(GMT -12:00) Eniwetok, Kwajalein</option>
                                            <option value="-11.0">(GMT -11:00) Midway Island, Samoa</option>
                                            <option value="-10.0">(GMT -10:00) Hawaii</option>
                                            <option value="-9.0">(GMT -9:00) Alaska</option>
                                            <option value="-8.0">(GMT -8:00) Pacific Time (US &amp; Canada)</option>
                                            <option value="-7.0">(GMT -7:00) Mountain Time (US &amp; Canada)</option>
                                            <option value="-6.0">(GMT -6:00) Central Time (US &amp; Canada), Mexico City</option>
                                            <option value="-5.0">(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
                                            <option value="-4.0">(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>
                                            <option value="-3.5">(GMT -3:30) Newfoundland</option>
                                            <option value="-3.0">(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>
                                            <option value="-2.0">(GMT -2:00) Mid-Atlantic</option>
                                            <option value="-1.0">(GMT -1:00 hour) Azores, Cape Verde Islands</option>
                                            <option value="0.0">(GMT) Western Europe Time, London, Lisbon, Casablanca</option>
                                            <option value="1.0">(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris</option>
                                            <option value="2.0">(GMT +2:00) Kaliningrad, South Africa</option>
                                            <option value="3.0">(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
                                            <option value="3.5">(GMT +3:30) Tehran</option>
                                            <option value="4.0">(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
                                            <option value="4.5">(GMT +4:30) Kabul</option>
                                            <option value="5.0">(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
                                            <option value="5.5">(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                                            <option value="5.75">(GMT +5:45) Kathmandu</option>
                                            <option value="6.0">(GMT +6:00) Almaty, Dhaka, Colombo</option>
                                            <option value="7.0">(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
                                            <option value="8.0">(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>
                                            <option value="9.0">(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
                                            <option value="9.5">(GMT +9:30) Adelaide, Darwin</option>
                                            <option value="10.0">(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>
                                            <option value="11.0">(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>
                                            <option value="12.0">(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                            
                        </div>
                    </div>
                </section>
                <section class="page exceptions schedule-hide" id="exception-block">
                    <div class="row with-checkbox">
                        <div class="col">
                            <h2>Exception Days</h2>
                        </div>
                    </div>
                    <div class="row" id="exception-container">
                        <div class="col">
                            <blockquote>Vacations, time off or other days where we should ignore the schedule and leave the team
                                in whatever state it is in when the day starts.</blockquote>
                            <table class="mb-1">
                                <tbody>
                                    ${team.exceptions.map(exception=> 
                                                                /*html*/`<tr>
                                        <td><em>${exception.start_date !== exception.end_date ? exception.start_date + " - " +
                                                exception.end_date : exception.start_date}</em></td>
                                        <td><a id="exception-edit-btn-${exception.exception_id}"
                                                data-id="${exception.exception_id}" data-startdate="${exception.start_date}"
                                                data-enddate="${exception.end_date}"
                                                data-startdateformatted="${exception.start_date}"
                                                data-enddateformatted="${exception.end_date}"
                                                data-title="${exception.start_date !== exception.end_date ? exception.start_date + " - " + exception.start_date : exception.start_date}" class="btn-tiny">EDIT</a>
                                        </td>
                                    </tr>`).join('')}
                                </tbody>
                            </table>
                            <div class="row hidden" id="edit-exception-input">
                                <div class="col">
                                    <h3 id="edit-exception-title">New Exception</h3>
                                    <table class="">
                                        <tbody>
        
        
                                            <tr>
                                                <td>Start Date</td>
                                                <td>
                                                    <p class="error small" id="start-date-warnings"
                                                        style="display:inline;margin-right:10px;"></p><input
                                                        id="edit-exception-start-date-input" type="text" readonly="readonly">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>End Date</td>
                                                <td>
                                                    <p class="error small" id="end-date-warnings"
                                                        style="display:inline;margin-right:10px;"></p><input
                                                        id="edit-exception-end-date-input" type="text" readonly="readonly">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="mt-1 edit-exception-buttons">
                                        <div><span class="btn btn-border" id="save-exception">Save</span></div>
                                        <div><span class="btn btn-border" id="cancel-exception">Cancel</span></div>
                                        <div class="exception-delete"><span class="btn btn-border hidden"
                                                style="border-color:#D32121 !important;color:#D32121;"
                                                id="delete-exception">Delete</span></div>
                                    </div>
                                </div>
                            </div>
                            <span class="btn btn-border mt-2 " id="add-exception">Add Day/Range</span>
                        </div>
                    </div>
                </section>
            </div>
            <section class="page mt-1" style="display:none;">
                <div class="row">
                    <div class="col">
                        <h2>Team Timezone</h2>
                        <blockquote class="mt-1">This is the timezone set in slack for this team, to change it just change the dropdown
                        </blockquote>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col">
                        ${team.team_timezone_text}
                        

                    </div>
                </div>
            </section>
            <div class="modal-container ${team.team_is_deleted ? '' : 'hidden'}" id="restore-team">
                <div class="modal">
                    <div class="modal-header" style="display:none;">
                        <div class="header_left">
                            <h1>Delete "${team.slug}"</h1>
                        </div>
                        <div class="header_right">
                            <a id="close-modal" class="btn btn-link">Close</a>
                        </div>
                    </div>
                    <div class="modal-body">
                        <section class="page">
                            <div class="row">
                                <div class="col">
                                    <p style="text-align:center;"><em>Team "${team.slug}" is currently inactive and will be
                                            deleted in ${team.team_days_til_deleted} days(s). Would you like to restore it for
                                            ${Urls.appPriceText} per month?</em></p>
                                </div>
                            </div>
                            <div class="row" style="margin-top:25px;text-align:center;">
                                <div class="modal-col col">
                                    <a id="restore-team-for-real" class="btn btn-border modal-btn">Yes</a>
        
                                    <a href="${Urls.appRoot}${Urls.appHome}" class="btn btn-border modal-btn">No</a>
                                </div>
                            </div>
                            <div class="row" style="margin-top:25px;text-align:center;">
                                <div class="modal-col col">
                                    <a id='delete-team-for-real'><em>... OR click here to delete it now!</em></a>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        
        `
    }
    , after_render: async () => {
        if(team.client_status == "in-progress" || team.team_client_status !== "logged-in"){
            let interval = setTimeout(function(){
                console.log("TeamSingle - Interval")
                if(team.client_status !== "in-progress" && team.team_client_status == "logged-in"){
                    clearInterval(interval)
                }
                if(window.location.hash.includes(Urls.appTeam)){
                    console.log("TeamSingle - Update")
                    updatePage()
                }
            }, 7000)
        }

        let startTime = setStartEndTimesGET(team.schedule_active_start)
        document.getElementById("schedule-active-start-hour").value= startTime.getHours();
        document.getElementById("schedule-active-start-minute").value= startTime.getMinutes();
        selectElement('team-timezone', team.team_timezone_offset.toFixed(1));

        document.getElementById("team-timezone").addEventListener ("change",  (e) => {
            let select = document.getElementById("team-timezone").value;
            let slug = document.getElementById("slug-input").value;
            postTeamTimezoneUpdate(slug, select);
        });

        
        let endTime = setStartEndTimesGET(team.schedule_active_end)
        document.getElementById("schedule-active-end-hour").value= endTime.getHours();
        document.getElementById("schedule-active-end-minute").value= endTime.getMinutes();
       
        let exceptionStart = rome(document.getElementById("edit-exception-start-date-input"), {
            dateValidator: rome.val.beforeEq(document.getElementById("edit-exception-end-date-input")),
            inputFormat: "MM/DD/YY",
            time: false
          });
          
        let exceptionEnd = rome(document.getElementById("edit-exception-end-date-input"), {
            dateValidator: rome.val.afterEq(document.getElementById("edit-exception-start-date-input")),
            inputFormat: "MM/DD/YY",
            time: false,
          });

        
        
        
        setScheduleElement(team.schedule_enabled);

        
        
        for (var i = 0; i < team.exceptions.length; i++) {
            document.getElementById("exception-edit-btn-" + team.exceptions[i].exception_id).addEventListener ("click",  (e) => {
                exceptionStart.setValue(e.srcElement.dataset.startdateformatted);
                exceptionEnd.setValue(e.srcElement.dataset.enddateformatted);
                document.getElementById("edit-exception-start-date-input").value = e.srcElement.dataset.startdateformatted;
                document.getElementById("edit-exception-end-date-input").value = e.srcElement.dataset.enddateformatted;
                document.getElementById("edit-exception-title").innerHTML = `Edit Exception - ${e.srcElement.dataset.title}`;
                document.getElementById("save-exception").dataset.exception_id = e.srcElement.dataset.id;
                document.getElementById("edit-exception-input").classList.remove('hidden');
                document.getElementById("add-exception").classList.add('hidden');
                document.getElementById("delete-exception").classList.remove('hidden');
                
                
            });
        }

        document.getElementById("add-exception").addEventListener ("click",  (e) => {
            document.getElementById("save-exception").dataset.exception_id = "";
            document.getElementById("edit-exception-title").innerHTML = "New Exception";
            document.getElementById("edit-exception-input").classList.remove('hidden');
            document.getElementById("cancel-exception").classList.remove('hidden');
            document.getElementById("save-exception").classList.remove('hidden');
            document.getElementById("add-exception").classList.add('hidden');
            document.getElementById("delete-exception").classList.add('hidden');
            document.getElementById("edit-exception-start-date-input").value = "";
            document.getElementById("edit-exception-end-date-input").value = "";
        });

        document.getElementById("cancel-exception").addEventListener ("click",  (e) => {
            document.getElementById("edit-exception-title").innerHTML = "New Exception";
            document.getElementById("edit-exception-input").classList.add('hidden');
            document.getElementById("add-exception").classList.remove('hidden');
            document.getElementById("delete-exception").classList.add('hidden');
            document.getElementById("end-date-warnings").innerHTML = "";
            document.getElementById("end-date-warnings").classList.remove("active");
            document.getElementById("start-date-warnings").innerHTML = "";
            document.getElementById("start-date-warnings").classList.remove("active");
        });
        
        document.getElementById("edit-exception-start-date-input").addEventListener("change", (e) => {
            document.getElementById("end-date-warnings").innerHTML = "";
            document.getElementById("end-date-warnings").classList.remove("active");
            document.getElementById("start-date-warnings").innerHTML = "";
            document.getElementById("start-date-warnings").classList.remove("active");
            let end_date = document.getElementById("edit-exception-end-date-input").value;
            let start_date = document.getElementById("edit-exception-start-date-input").value;
            if(setExceptionDate(start_date)){
                if(!setExceptionDate(end_date)){
                    document.getElementById("edit-exception-end-date-input").value = start_date;
                }else if(end_date < start_date){
                    document.getElementById("edit-exception-end-date-input").value = start_date;
                }
            }
            if(!setExceptionDate(document.getElementById("edit-exception-start-date-input").value)){
                document.getElementById("start-date-warnings").innerHTML = "Oh no! Invalid Date!";
                document.getElementById("start-date-warnings").classList.add("active");
            }

            if(!setExceptionDate(document.getElementById("edit-exception-end-date-input").value)){
                document.getElementById("end-date-warnings").innerHTML = "Oh no! Invalid Date!";
                document.getElementById("end-date-warnings").classList.add("active");
            }
        });

        document.getElementById("edit-exception-end-date-input").addEventListener("change", (e) => {
            document.getElementById("end-date-warnings").innerHTML = "";
            document.getElementById("end-date-warnings").classList.remove("active");
            document.getElementById("start-date-warnings").innerHTML = "";
            document.getElementById("start-date-warnings").classList.remove("active");
            let end_date = document.getElementById("edit-exception-end-date-input").value;
            let start_date = document.getElementById("edit-exception-start-date-input").value;
            if(end_date < start_date){
                if(setExceptionDate(start_date)){
                    document.getElementById("edit-exception-end-date-input").value = start_date;
                }
            }
            if(!setExceptionDate(document.getElementById("edit-exception-start-date-input").value)){
                document.getElementById("start-date-warnings").innerHTML = "Oh no! Invalid Date!";
                document.getElementById("start-date-warnings").classList.add("active");
            }

            if(!setExceptionDate(document.getElementById("edit-exception-end-date-input").value)){
                document.getElementById("end-date-warnings").innerHTML = "Oh no! Invalid Date!";
                document.getElementById("end-date-warnings").classList.add("active");
            }
        });

        document.getElementById("save-exception").addEventListener ("click",  (e) => {
            
            document.getElementById("end-date-warnings").innerHTML = "";
            document.getElementById("end-date-warnings").classList.remove("active");
            document.getElementById("start-date-warnings").innerHTML = "";
            document.getElementById("start-date-warnings").classList.remove("active");
            let slug       = document.getElementById("slug-input");
            let exception_id = document.getElementById("save-exception").dataset.exception_id;
            let start_date = exceptionStart.getDate();
            let end_date = exceptionEnd.getDate();
            if(start_date && end_date){
                if(exception_id){
                    postExceptionUpdate(slug.value, exception_id, start_date, end_date)
                }else{
                    postExceptionNew(slug.value, start_date, end_date)
                }    
            }else{
                if(!start_date){
                    document.getElementById("start-date-warnings").innerHTML = "Oh no! Invalid Date!";
                    document.getElementById("start-date-warnings").classList.add("active");
                }

                if(!end_date){
                    document.getElementById("end-date-warnings").innerHTML = "Oh no! Invalid Date!";
                    document.getElementById("end-date-warnings").classList.add("active");
                }
            }
        });

        document.getElementById("delete-exception").addEventListener ("click",  (e) => {
            document.getElementById("end-date-warnings").innerHTML = "";
            document.getElementById("end-date-warnings").classList.remove("active");
            document.getElementById("start-date-warnings").innerHTML = "";
            document.getElementById("start-date-warnings").classList.remove("active");
            let slug       = document.getElementById("slug-input");
            let exception_id = document.getElementById("save-exception").dataset.exception_id;
            
             postExceptionDelete(slug.value, exception_id)
            
        });

       
        

        document.getElementById("presence-switch").addEventListener ("click",  (e) => {
            e.preventDefault()
            let checkbox = document.getElementById("presence-checkbox");
            let slug       = document.getElementById("slug-input");
            if (slug.value =='') {
                alert (`The fields cannot be empty`)
            }else {
                postPresenceActiveUpdate(slug.value, !checkbox.checked);
            }
        });

        document.getElementById("schedule-switch").addEventListener ("click",  (e) => {
            e.preventDefault()
            let checkbox       = document.getElementById("schedule-checkbox");
            let slug       = document.getElementById("slug-input");
            if (slug.value =='') {
                alert (`The fields cannot be empty`)
            }else {
                postScheduleActiveUpdate(slug.value, !checkbox.checked);
            }
        });

       
       

        document.getElementById("delete-team-for-real").addEventListener ("click",  () => {
            let team       = document.getElementById("slug-input");
            deleteTeam(team.value);
            
        });
        document.getElementById("restore-team-for-real").addEventListener ("click",  () => {
            let team       = document.getElementById("slug-input");
            restoreTeam(team.value);
            
        });

        document.getElementById("weekday-sunday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-sunday");
            let slug = document.getElementById("slug-input").value;
            postActiveSundayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-monday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-monday");
            let slug = document.getElementById("slug-input").value;
            postActiveMondayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-tuesday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-tuesday");
            let slug = document.getElementById("slug-input").value;
            postActiveTuesdayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-wednesday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-wednesday");
            let slug = document.getElementById("slug-input").value;
            postActiveWednesdayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-thursday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-thursday");
            let slug = document.getElementById("slug-input").value;
            postActiveThursdayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-friday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-friday");
            let slug = document.getElementById("slug-input").value;
            postActiveFridayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-saturday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-saturday");
            let slug = document.getElementById("slug-input").value;
            postActiveSaturdayUpdate(slug, checkbox.checked)
        });

        
        document.getElementById("schedule-active-start-hour").addEventListener ("change",  (e) => {
            let hour = document.getElementById("schedule-active-start-hour").options[document.getElementById("schedule-active-start-hour").selectedIndex].text;
            let minute = document.getElementById("schedule-active-start-minute").options[document.getElementById("schedule-active-start-minute").selectedIndex].text;
            let timestring =  hour + ":" + minute;
            let slug = document.getElementById("slug-input").value;
            let theTime = setStartEndTimes(timestring);
            postActiveStartTimeUpdate(slug, theTime);
        });

        document.getElementById("schedule-active-start-minute").addEventListener ("change",  (e) => {
            let hour = document.getElementById("schedule-active-start-hour").options[document.getElementById("schedule-active-start-hour").selectedIndex].text;
            let minute = document.getElementById("schedule-active-start-minute").options[document.getElementById("schedule-active-start-minute").selectedIndex].text;
            let timestring =  hour + ":" + minute;
            let slug = document.getElementById("slug-input").value;
            let theTime = setStartEndTimes(timestring);
            postActiveStartTimeUpdate(slug, theTime);
        });

        document.getElementById("schedule-active-end-hour").addEventListener ("change",  (e) => {
            let hour = document.getElementById("schedule-active-end-hour").options[document.getElementById("schedule-active-end-hour").selectedIndex].text;
            let minute = document.getElementById("schedule-active-end-minute").options[document.getElementById("schedule-active-end-minute").selectedIndex].text;
            let timestring =  hour + ":" + minute;
            let slug = document.getElementById("slug-input").value;
            let theTime = setStartEndTimes(timestring);
            postActiveEndTimeUpdate(slug, theTime);
        });

        document.getElementById("schedule-active-end-minute").addEventListener ("change",  (e) => {
            let hour = document.getElementById("schedule-active-end-hour").options[document.getElementById("schedule-active-end-hour").selectedIndex].text;
            let minute = document.getElementById("schedule-active-end-minute").options[document.getElementById("schedule-active-end-minute").selectedIndex].text;
            let timestring =  hour + ":" + minute;
            let slug = document.getElementById("slug-input").value;
            let theTime = setStartEndTimes(timestring);
            postActiveEndTimeUpdate(slug, theTime);
        });
        
    }
}

export default TeamSingleScheduleCombined;