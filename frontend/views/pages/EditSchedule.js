import Fetcher        from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'
import Urls            from '../../services/Urls.js'

function singleTeamCallback(json){
    return json
}

let getTeam = async (slug) => {
    try {
        const response = await Fetcher.get(Urls.apiSingleTeam + slug, singleTeamCallback);
        return response;
    } catch (err) {
        console.log('Error getting documents', err)
    }
}

let updatePage = async (json) => {
    const content = null || document.getElementById('page_container');
    content.innerHTML = await EditSchedule.render(json);
    await EditSchedule.after_render();
};

let postActiveStatusUpdateCallback = (json) => {
    if(json.success){
        let checkbox       = document.getElementById("active-status-checkbox");
        checkbox.checked = json.data.scheduleActiveStatusEnabled;
        setTimeout(function(){
            updatePage();
        },500);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postActiveStatusUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        'scheduleActiveStatusEnabled': checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveStatusEnabled'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postActiveStatusUpdateCallback);
}

let postActiveNotificationsUpdateCallback = (json) => {
    if(json.success){
        let checkbox       = document.getElementById("active-notifications-checkbox");
        checkbox.checked = json.data.scheduleActiveNotificationsEnabled;
        setTimeout(function(){
            updatePage();
        },500);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postActiveNotificationsUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        'scheduleActiveNotificationsEnabled': checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveNotificationsEnabled'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postActiveNotificationsUpdateCallback);
}

let postInactiveStatusUpdateCallback = (json) => {
    if(json.success){
        let checkbox       = document.getElementById("inactive-status-checkbox");
        checkbox.checked = json.data.scheduleInactiveStatusEnabled;
        setTimeout(function(){
            updatePage();
        },500);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postInactiveStatusUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        'scheduleInactiveStatusEnabled': checkbox,
        'slug':slug,
        'setBool': 'scheduleInactiveStatusEnabled'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postInactiveStatusUpdateCallback);
}

let postInactiveNotificationsUpdateCallback = (json) => {
    if(json.success){
        let checkbox       = document.getElementById("inactive-notifications-checkbox");
        checkbox.checked = json.data.scheduleInactiveNotificationsEnabled;
        setTimeout(function(){
            updatePage();
        },500);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postInactiveNotificationsUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        'scheduleInactiveNotificationsEnabled': checkbox,
        'slug':slug,
        'setBool': 'scheduleInactiveNotificationsEnabled'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postInactiveNotificationsUpdateCallback);
}











let postActiveStatusTextUpdateCallback = (json) => {
    if(json.success){
        updatePage(json.team);
        
        setTimeout(function(){
        let saveText = document.getElementById("active-status-input-saved-text");
        saveText.innerHTML = "Saved @ " + timestampNow();
        saveText.classList.add("saved-text");    
        },100);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postActiveStatusTextUpdate = async () => {
    let input = document.getElementById("active-status-input");
    let slug       = document.getElementById("slug-input").value;
    if (slug.value =='') {
        alert (`The fields cannot be empty`)
    }else {
        let body = JSON.stringify({
            'scheduleActiveStatus': input.value,
            'slug':slug,
        })
        Fetcher.post(Urls.apiSingleTeam + slug, body, postActiveStatusTextUpdateCallback);
    }
}

let delayIntervalsActiveStatus = [];
let lastDelayActiveStatus = "";

const delayActiveStatus = (action) => {
    for (let i = 0; i < delayIntervalsActiveStatus.length; i++){
        clearInterval(delayIntervalsActiveStatus[i]);
    }
    delayIntervalsActiveStatus = [];
    let intervalId = setTimeout(() => {
        lastDelayActiveStatus = timestampNow()
        action();
    }, 2000);
    delayIntervalsActiveStatus.push(intervalId);    
}










let postInactiveStatusTextUpdateCallback = (json) => {
    if(json.success){
        updatePage(json.team);
        
        setTimeout(function(){
        let saveText = document.getElementById("inactive-status-input-saved-text");
        saveText.innerHTML = "Saved @ " + timestampNow();
        saveText.classList.add("saved-text");    
        },100);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postInactiveStatusTextUpdate = async () => {
    let input = document.getElementById("inactive-status-input");
    let slug       = document.getElementById("slug-input").value;
    if (slug.value =='') {
        alert (`The fields cannot be empty`)
    }else {
        let body = JSON.stringify({
            'scheduleInactiveStatus': input.value,
            'slug':slug,
        })
        Fetcher.post(Urls.apiSingleTeam + slug, body, postInactiveStatusTextUpdateCallback);
    }
}

let delayIntervalsInactiveStatus = [];
let lastDelayInactiveStatus = "";

const delayInactiveStatus = (action) => {
    for (let i = 0; i < delayIntervalsInactiveStatus.length; i++){
        clearInterval(delayIntervalsInactiveStatus[i]);
    }
    delayIntervalsInactiveStatus = [];
    let intervalId = setTimeout(() => {
        lastDelayInactiveStatus = timestampNow()
        action();
    }, 2000);
    delayIntervalsInactiveStatus.push(intervalId);    
}









let postActiveNotificationsTextUpdateCallback = (json) => {
    if(json.success){
        updatePage(json.team);
        
        setTimeout(function(){
        let saveText = document.getElementById("active-notifications-input-saved-text");
        saveText.innerHTML = "Saved @ " + timestampNow();
        saveText.classList.add("saved-text");    
        },100);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postActiveNotificationsTextUpdate = async () => {
    let input = document.getElementById("active-notifications-input");
    let slug       = document.getElementById("slug-input").value;
    if (slug.value =='') {
        alert (`The fields cannot be empty`)
    }else {
        let body = JSON.stringify({
            'scheduleActiveNotificationsKeywords': input.value,
            'slug':slug,
        })
        Fetcher.post(Urls.apiSingleTeam + slug, body, postActiveNotificationsTextUpdateCallback);
    }
}

let delayIntervalsActiveNotifications = [];
let lastDelayActiveNotifications = "";

const delayActiveNotifications = (action) => {
    for (let i = 0; i < delayIntervalsActiveNotifications.length; i++){
        clearInterval(delayIntervalsActiveNotifications[i]);
    }
    delayIntervalsActiveNotifications = [];
    let intervalId = setTimeout(() => {
        lastDelayActiveNotifications = timestampNow()
        action();
    }, 2000);
    delayIntervalsActiveNotifications.push(intervalId);    
}










let postInactiveNotificationsTextUpdateCallback = (json) => {
    if(json.success){
        updatePage(json.team);
        
        setTimeout(function(){
        let saveText = document.getElementById("inactive-notifications-input-saved-text");
        saveText.innerHTML = "Saved @ " + timestampNow();
        saveText.classList.add("saved-text");    
        },100);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postInactiveNotificationsTextUpdate = async () => {
    let input = document.getElementById("inactive-notifications-input");
    let slug       = document.getElementById("slug-input").value;
    if (slug.value =='') {
        alert (`The fields cannot be empty`)
    }else {
        let body = JSON.stringify({
            'scheduleInactiveNotificationsKeywords': input.value,
            'slug':slug,
        })
        Fetcher.post(Urls.apiSingleTeam + slug, body, postInactiveNotificationsTextUpdateCallback);
    }
}

let delayIntervalsInactiveNotifications = [];
let lastDelayInactiveNotifications= "";

const delayInactiveNotifications = (action) => {
    for (let i = 0; i < delayIntervalsInactiveNotifications.length; i++){
        clearInterval(delayIntervalsInactiveNotifications[i]);
    }
    delayIntervalsInactiveNotifications = [];
    let intervalId = setTimeout(() => {
        lastDelayInactiveNotifications = timestampNow()
        action();
    }, 2000);
    delayIntervalsInactiveNotifications.push(intervalId);    
}








let postActiveNotificationsTypeUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postActiveNotificationsTypeUpdate = async (slug, radio) => {
    let body = JSON.stringify({
        'scheduleActiveNotificationsType': radio,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postActiveNotificationsTypeUpdateCallback);
}

let postInactiveNotificationsTypeUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postInactiveNotificationsTypeUpdate = async (slug, radio) => {
    let body = JSON.stringify({
        'scheduleInactiveNotificationsType': radio,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postInactiveNotificationsTypeUpdateCallback);
}

let postDayUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postActiveSundayUpdate = async (slug, checkbox) => {
    
    let body = JSON.stringify({
        "scheduleActiveSunday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveSunday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveMondayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveMonday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveMonday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveTuesdayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveTuesday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveTuesday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveWednesdayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveWednesday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveWednesday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveThursdayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveThursday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveThursday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveFridayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveFriday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveFriday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}

let postActiveSaturdayUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        "scheduleActiveSaturday" : checkbox,
        'slug':slug,
        'setBool': 'scheduleActiveSaturday'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDayUpdateCallback);
}
const hours12 = (date) => { 
    return (date.getHours() + 24) % 12 || 12; 
}
const setStartEndTimes = (s) => {
    var d = new Date(),
    parts = s.match(/(\d+)\:(\d+)/),
    hours = parseInt(parts[1], 10),
    minutes = parseInt(parts[2], 10);
    d.setHours(hours - (d.getTimezoneOffset()/60));
    d.setMinutes(minutes);
    return d
}

const setStartEndTimesGET = (s) => {
    var d = new Date(),
    parts = s.match(/(\d+)\:(\d+)/),
    hours = parseInt(parts[1], 10),
    minutes = parseInt(parts[2], 10);
    
    d.setHours(hours);
    d.setMinutes(minutes);
    return d
}
const timestampNow = () => {
    let options = {  
        weekday: "long", year: "numeric", month: "short",  
        day: "numeric", hour: "2-digit", minute: "2-digit", 
    };  
    let date = new Date();  
    return date.toLocaleTimeString("en-us");
}

let postActiveStartTimeUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postActiveStartTimeUpdate = async (slug, time) => {
    let body = JSON.stringify({
        'scheduleActiveStart': time,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postActiveStartTimeUpdateCallback);
}

let postActiveEndTimeUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postActiveEndTimeUpdate = async (slug, time) => {
    let body = JSON.stringify({
        'scheduleActiveEnd': time,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postActiveEndTimeUpdateCallback);
}


let team = ""

let EditSchedule = {

    render : async (team_data = false) => {
        if(!team_data){
            let request = Utils.parseRequestURL()
            team = await getTeam(request.slug)    
        }else{
            team = team_data
        }
        
        return /*html*/`
            <style>
                .border{
                    border-color: ${team.current_presence ? '#5FD321' : '#E0E0E0;'};
                }
                #body{
                    // background: #F6F2D5;
                }
                td{
                    border-bottom:none;
                }
                .page.schedule, .page.exceptions{
                    margin: 0 0;
                }
            </style>
            <div class="header_container ">
                <div class="header_container_inner">
                    <div class="header_left" style="display: flex;align-items: center;"> 
                        <a href="${Urls.appRoot}${Urls.appTeam}${team.slug}" style="display: flex;align-items: center;">
                        <img class="back-arrow" src="assets/img/chevron.png"><img class="back-icon" src="assets/img/calendar.png" style="margin-right:5px;top:0;"><h1 class="">${team.slug}</h1>
                        </a>
                    </div>
                </div>
            </div>
            <div class="body_container" style="">
                <form>
                    <section class="page"  style="border-radius:8px 8px 0 0;">
                        
                        <div class="row ">
                            <div class="col">
                                <h2><span class="highlight-active">Active</span> Days</h2>
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col">
                                <input type="checkbox" id="weekday-sunday" name="weekday-sunday" ${team.schedule_active_sunday ? 'checked' : ''}>   
                                <label for="weekday-sunday">Sunday</label>
                                <input type="checkbox" id="weekday-monday" name="weekday-monday" ${team.schedule_active_monday ? 'checked' : ''}>   
                                <label for="weekday-monday">Monday</label>
                                <input type="checkbox" id="weekday-tuesday" name="weekday-tuesday" ${team.schedule_active_tuesday ? 'checked' : ''}>   
                                <label for="weekday-tuesday">Tuesday</label>
                                <input type="checkbox" id="weekday-wednesday" name="weekday-wednesday" ${team.schedule_active_wednesday ? 'checked' : ''}>   
                                <label for="weekday-wednesday">Wednesday</label>
                                <input type="checkbox" id="weekday-thursday" name="weekday-thursday" ${team.schedule_active_thursday ? 'checked' : ''}>   
                                <label for="weekday-thursday">Thursday</label>
                                <input type="checkbox" id="weekday-friday" name="weekday-friday" ${team.schedule_active_friday ? 'checked' : ''}>   
                                <label for="weekday-friday">Friday</label>
                                <input type="checkbox" id="weekday-saturday" name="weekday-saturday" ${team.schedule_active_saturday ? 'checked' : ''}>   
                                <label for="weekday-saturday">Saturday</label>    
                            </div>
                        </div>    
                        <div class="row mt-2">
                            <div class="col">
                                <h2><span class="highlight-active">Active</span> Hours</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <table>
                                    <tr>
                                        <td>
                                            Start: 
                                        </td>
                                        <td >
                                            <select id="schedule-active-start-hour" class="select-css"> 
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                <option value="8">08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                            </select> :
                                            <select id="schedule-active-start-minute" class="select-css"> 
                                                <option value="0">00</option>
                                                <option value="5">05</option>
                                                <option value="10">10</option>
                                                <option value="15">15</option>
                                                <option value="20">20</option>
                                                <option value="25">25</option>
                                                <option value="30">30</option>
                                                <option value="35">35</option>
                                                <option value="40">40</option>
                                                <option value="45">45</option>
                                                <option value="50">50</option>
                                                <option value="55">55</option>
                                            </select> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            End: 
                                        </td>
                                        <td >
                                            <select id="schedule-active-end-hour" class="select-css"> 
                                                <option value="0">00</option>
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                <option value="8">08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                            </select> :
                                            <select id="schedule-active-end-minute" class="select-css"> 
                                                <option value="0">00</option>
                                                <option value="5">05</option>
                                                <option value="10">10</option>
                                                <option value="15">15</option>
                                                <option value="20">20</option>
                                                <option value="25">25</option>
                                                <option value="30">30</option>
                                                <option value="35">35</option>
                                                <option value="40">40</option>
                                                <option value="45">45</option>
                                                <option value="50">50</option>
                                                <option value="55">55</option>
                                            </select> 
                                        </td>
                                </tr>
                                <tr>
                                        <td>
                                            Team Timezone: 
                                        </td>
                                        <td>
                                            ${team.team_timezone_text}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col">
                                <h2><span class="highlight-active">Active</span> Status</h2>
                            </div>
                            <div class="col text-right">
                                <label id="active-status-switch"  class="switch">
                                    <input id="active-status-checkbox" type="checkbox" ${team.schedule_active_status_enabled ? 'checked' : ''}>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        
                        <div class="row schedule-hide" id="active-status-container">
                            <div class="col">
                                <input class="status-input" type="text" id="active-status-input" value="${team.schedule_active_status}"> <em class="small" id="active-status-input-saved-text">&nbsp;</em>
                            </div>
                        </div> 
                        <div class="row ${team.schedule_active_status_enabled ? 'hidden' : ''}">
                            <div class="col">
                                <blockquote>Status settings will not be changed when presence is <strong>active</strong>, enable this to change when inactive</blockquote>
                            </div>
                        </div>
                        <div class="row  mb-1">
                            <div class="col">
                                <blockquote>If you start your status with an emoji, this will be used to set the emoji next to your name. Otherwise, your status emoji will be 💬.</blockquote>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col">
                                <h2><span class="highlight-active">Active</span> Notifications</h2>
                            </div>
                            <div class="col text-right">
                                <label id="active-notifications-switch"  class="switch">
                                    <input id="active-notifications-checkbox" type="checkbox" ${team.schedule_active_notifications_enabled ? 'checked' : ''}>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        
                        <div class="row schedule-hide" id="active-notifications-container">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <div class="radio radio-group">    
                                            <input type="radio" id="active-notifications-all" name="notifications" value="ALL"> 
                                            <label  class="radio-label" for="active-notifications-all"><div>All new messages</div></label>
                                        </div>
                                    
                                        <div class="radio radio-group">
                                            <input type="radio" id="active-notifications-keywords" name="notifications" value="KEYWORDS"> 
                                            <label  class="radio-label" for="active-notifications-keywords">
                                                <div>
                                                    Direct messages, mentions & keywords (below)
                                                    <textarea class="" id="active-notifications-input">${team.schedule_active_notifications_keywords}</textarea>
                                                    <p class="small"><em>Use commas to separate each keyword. Keywords are not case sensitive.</em> <em class="small" id="active-notifications-input-saved-text">&nbsp;</em></p>
                                                </div>
                                            </label>
                                        </div>
                                    
                                        <div class="radio radio-group">    
                                            <input type="radio" id="active-notifications-none" name="notifications" value="NONE">  
                                            <label  class="radio-label" for="active-notifications-none"><div>None</div></label>
                                        </div>
                                    </div>
                                </div>         
                            </div>
                        </div>
                        <div class="row  ${team.schedule_active_notifications_enabled ? 'hidden' : ''}">
                            <div class="col">
                                <blockquote>Notifications settings will not be changed when presence is <strong>active</strong>, enable this to change when inactive</blockquote>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <blockquote>These control mobile notifications, not desktop.</blockquote>
                            </div>
                        </div>
                    </section>
                    <section class="page " style="border-radius:0 0 8px 8px;">
                        <div class="row">
                            <div class="col">
                                <hr>
                            </div>
                        </div> 
                        <div class="row mt-2">
                            <div class="col">
                                <h2><span class="highlight-inactive">Inactive</span> Status</h2>
                            </div>
                            <div class="col text-right  no-grow">
                                <label id="inactive-status-switch"  class="switch">
                                    <input id="inactive-status-checkbox" type="checkbox" ${team.schedule_inactive_status_enabled ? 'checked' : ''}>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        
                        <div class="row schedule-hide" id="inactive-status-container">
                            <div class="col">
                                <input class="status-input" type="text" id="inactive-status-input" value="${team.schedule_inactive_status}"> <em class="small" id="inactive-status-input-saved-text">&nbsp;</em>
                            </div>
                        </div> 
                        <div class="row ${team.schedule_inactive_status_enabled ? 'hidden' : ''}">
                            <div class="col">
                                <blockquote>Status will not be changed when presence is <strong>inactive</strong>, enable this to change when inactive</blockquote>
                            </div>
                        </div>
                        <div class="row  mb-1">
                            <div class="col">
                                <blockquote>If you start your status with an emoji, this will be used to set the emoji next to your name. Otherwise, your status emoji will be 💬.</blockquote>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col">
                                <h2><span class="highlight-inactive">Inactive</span> Notifications</h2>
                            </div>
                            <div class="col text-right  no-grow">
                                <label id="inactive-notifications-switch"  class="switch">
                                    <input id="inactive-notifications-checkbox" type="checkbox" ${team.schedule_inactive_notifications_enabled ? 'checked' : ''}>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        
                        <div class="row schedule-hide mb-1" id="inactive-notifications-container">
                            <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <div class="radio radio-group">    
                                            <input type="radio" id="inactive-notifications-all" name="notifications-inactive"  value="ALL"> 
                                            <label  class="radio-label" for="inactive-notifications-all"><div>All new messages</div></label>
                                        </div>
                                    
                                        <div class="radio radio-group">
                                            <input type="radio" id="inactive-notifications-keywords" name="notifications-inactive"  value="KEYWORDS"> 
                                            <label  class="radio-label" for="inactive-notifications-keywords">
                                                <div>
                                                    Direct messages, mentions & keywords (below)
                                                    <textarea class="" id="inactive-notifications-input">${team.schedule_inactive_notifications_keywords}</textarea>
                                                    <p class="small"><em>Use commas to separate each keyword. Keywords are not case sensitive.</em> <em class="small" id="inactive-notifications-input-saved-text">&nbsp;</em></p>
                                                </div>
                                            </label>
                                        </div>
                                    
                                        <div class="radio radio-group">    
                                            <input type="radio" id="inactive-notifications-none" name="notifications-inactive"  value="NONE">  
                                            <label  class="radio-label" for="inactive-notifications-none"><div>None</div></label>
                                        </div>
                                    </div>
                                </div>         
                            </div>
                        </div>
                        <div class="row ${team.schedule_inactive_notifications_enabled ? 'hidden' : ''}">
                            <div class="col">
                                <blockquote>Notifications settings will not be changed when presence is <strong>inactive</strong>, enable this to change when inactive</blockquote>
                            </div>
                        </div>
                        <div class="row  mb-1">
                            <div class="col">
                                <blockquote>These control mobile notifications, not desktop.</blockquote>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        `
    }
    , after_render: async () => {

        let startTime = setStartEndTimesGET(team.schedule_active_start)
        document.getElementById("schedule-active-start-hour").value= startTime.getHours();
        document.getElementById("schedule-active-start-minute").value= startTime.getMinutes();
        
        let endTime = setStartEndTimesGET(team.schedule_active_end)
        document.getElementById("schedule-active-end-hour").value= endTime.getHours();
        document.getElementById("schedule-active-end-minute").value= endTime.getMinutes();
        


        document.getElementById("inactive-notifications-" + team.schedule_inactive_notifications_type.toLowerCase()).checked = true;
        document.getElementById("active-notifications-" + team.schedule_active_notifications_type.toLowerCase()).checked = true;

        if (document.getElementById("inactive-status-checkbox").checked) {
            document.getElementById('inactive-status-container').classList.remove('schedule-hide');        
        }else {
            document.getElementById('inactive-status-container').classList.add('schedule-hide');        
        }
        
        if (document.getElementById("inactive-notifications-checkbox").checked) {
            document.getElementById('inactive-notifications-container').classList.remove('schedule-hide');        
        }else {
            document.getElementById('inactive-notifications-container').classList.add('schedule-hide');        
        }
        
        if (document.getElementById("active-status-checkbox").checked) {
            document.getElementById('active-status-container').classList.remove('schedule-hide');        
        }else {
            document.getElementById('active-status-container').classList.add('schedule-hide');        
        }
        
        if (document.getElementById("active-notifications-checkbox").checked) {
            document.getElementById('active-notifications-container').classList.remove('schedule-hide');        
        }else {
            document.getElementById('active-notifications-container').classList.add('schedule-hide');        
        }

        document.getElementById("inactive-status-switch").addEventListener ("click",  (e) => {
            let checkbox       = document.getElementById("inactive-status-checkbox");
            let slug       = document.getElementById("slug-input");
            postInactiveStatusUpdate(slug.value, !checkbox.checked);
        });
        
        document.getElementById("inactive-notifications-switch").addEventListener ("click",  (e) => {
            let checkbox       = document.getElementById("inactive-notifications-checkbox");
            let slug       = document.getElementById("slug-input");
            postInactiveNotificationsUpdate(slug.value, !checkbox.checked);
        });

        document.getElementById("active-status-switch").addEventListener ("click",  (e) => {
            let checkbox       = document.getElementById("active-status-checkbox");
            let slug       = document.getElementById("slug-input");
            postActiveStatusUpdate(slug.value, !checkbox.checked);
        });

        document.getElementById("active-notifications-switch").addEventListener ("click",  (e) => {
            let checkbox       = document.getElementById("active-notifications-checkbox");
            let slug       = document.getElementById("slug-input");
            postActiveNotificationsUpdate(slug.value, !checkbox.checked);
        });
        
        document.getElementById("active-status-input").addEventListener ("keydown",  (e) => {
            delayActiveStatus(postActiveStatusTextUpdate);
        });

        document.getElementById("inactive-status-input").addEventListener ("keydown",  (e) => {
            delayInactiveStatus(postInactiveStatusTextUpdate);
        });

        document.getElementById("active-notifications-input").addEventListener ("keydown",  (e) => {
            delayActiveNotifications(postActiveNotificationsTextUpdate);
        });

        document.getElementById("inactive-notifications-input").addEventListener ("keydown",  (e) => {
            delayInactiveNotifications(postInactiveNotificationsTextUpdate);
        });

        document.getElementById("active-notifications-keywords").addEventListener ("click",  (e) => {
            let radio = document.getElementById("active-notifications-keywords").value;
            let slug = document.getElementById("slug-input").value;
            postActiveNotificationsTypeUpdate(slug, radio);
        });

        document.getElementById("active-notifications-all").addEventListener ("click",  (e) => {
            let radio = document.getElementById("active-notifications-all").value;
            let slug = document.getElementById("slug-input").value;
            postActiveNotificationsTypeUpdate(slug, radio);
        });

        document.getElementById("active-notifications-none").addEventListener ("click",  (e) => {
            let radio = document.getElementById("active-notifications-none").value;
            let slug = document.getElementById("slug-input").value;
            postActiveNotificationsTypeUpdate(slug, radio);
        });

        document.getElementById("inactive-notifications-keywords").addEventListener ("click",  (e) => {
            let radio = document.getElementById("inactive-notifications-keywords").value;
            let slug = document.getElementById("slug-input").value;
            postInactiveNotificationsTypeUpdate(slug, radio);
        });

        document.getElementById("inactive-notifications-all").addEventListener ("click",  (e) => {
            let radio = document.getElementById("inactive-notifications-all").value;
            let slug = document.getElementById("slug-input").value;
            postInactiveNotificationsTypeUpdate(slug, radio);
        });

        document.getElementById("inactive-notifications-none").addEventListener ("click",  (e) => {
            let radio = document.getElementById("inactive-notifications-none").value;
            let slug = document.getElementById("slug-input").value;
            postInactiveNotificationsTypeUpdate(slug, radio);
        });

        document.getElementById("weekday-sunday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-sunday");
            let slug = document.getElementById("slug-input").value;
            postActiveSundayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-monday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-monday");
            let slug = document.getElementById("slug-input").value;
            postActiveMondayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-tuesday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-tuesday");
            let slug = document.getElementById("slug-input").value;
            postActiveTuesdayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-wednesday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-wednesday");
            let slug = document.getElementById("slug-input").value;
            postActiveWednesdayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-thursday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-thursday");
            let slug = document.getElementById("slug-input").value;
            postActiveThursdayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-friday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-friday");
            let slug = document.getElementById("slug-input").value;
            postActiveFridayUpdate(slug, checkbox.checked)
        });

        document.getElementById("weekday-saturday").addEventListener ("click",  (e) => {
            e.preventDefault();
            let checkbox       = document.getElementById("weekday-saturday");
            let slug = document.getElementById("slug-input").value;
            postActiveSaturdayUpdate(slug, checkbox.checked)
        });

        
        document.getElementById("schedule-active-start-hour").addEventListener ("change",  (e) => {
            let hour = document.getElementById("schedule-active-start-hour").options[document.getElementById("schedule-active-start-hour").selectedIndex].text;
            let minute = document.getElementById("schedule-active-start-minute").options[document.getElementById("schedule-active-start-minute").selectedIndex].text;
            let timestring =  hour + ":" + minute;
            let slug = document.getElementById("slug-input").value;
            let theTime = setStartEndTimes(timestring);
            postActiveStartTimeUpdate(slug, theTime);
        });

        document.getElementById("schedule-active-start-minute").addEventListener ("change",  (e) => {
            let hour = document.getElementById("schedule-active-start-hour").options[document.getElementById("schedule-active-start-hour").selectedIndex].text;
            let minute = document.getElementById("schedule-active-start-minute").options[document.getElementById("schedule-active-start-minute").selectedIndex].text;
            let timestring =  hour + ":" + minute;
            let slug = document.getElementById("slug-input").value;
            let theTime = setStartEndTimes(timestring);
            postActiveStartTimeUpdate(slug, theTime);
        });

        document.getElementById("schedule-active-end-hour").addEventListener ("change",  (e) => {
            let hour = document.getElementById("schedule-active-end-hour").options[document.getElementById("schedule-active-end-hour").selectedIndex].text;
            let minute = document.getElementById("schedule-active-end-minute").options[document.getElementById("schedule-active-end-minute").selectedIndex].text;
            let timestring =  hour + ":" + minute;
            let slug = document.getElementById("slug-input").value;
            let theTime = setStartEndTimes(timestring);
            postActiveEndTimeUpdate(slug, theTime);
        });

        document.getElementById("schedule-active-end-minute").addEventListener ("change",  (e) => {
            let hour = document.getElementById("schedule-active-end-hour").options[document.getElementById("schedule-active-end-hour").selectedIndex].text;
            let minute = document.getElementById("schedule-active-end-minute").options[document.getElementById("schedule-active-end-minute").selectedIndex].text;
            let timestring =  hour + ":" + minute;
            let slug = document.getElementById("slug-input").value;
            let theTime = setStartEndTimes(timestring);
            postActiveEndTimeUpdate(slug, theTime);
        });

    }
}

export default EditSchedule;