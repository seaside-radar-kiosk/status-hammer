import Urls            from '../../services/Urls.js'

let Error404 = {

    render : async () => {
        let view =  /*html*/`
            <section class="four-o-four-container">
                    <h1 class="mb-1"> 404 Error  🤷🏻‍</h1>
                    <p class="mb-2">Mercy, there's nothing here... that's weird, how did you get here? If you feel like something is really wrong, please contact us. Otherwise...</p>
                    <p class=""><a href="${Urls.appRoot}${Urls.appHome}" class="btn btn-border">Let's Go Home</a></p>

                    
            </section>
        `
        return view
    }
    , after_render: async () => {
    }
}
export default Error404;