import Fetcher        from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'
import Urls            from '../../services/Urls.js'

function selectElement(id, valueToSelect) {    
    let element = document.getElementById(id);
    element.value = valueToSelect;
}

function singleTeamCallback(json){
    return json
}

let getAccount = async () => {
    try {
        const response = await Fetcher.get(Urls.apiAccount, singleTeamCallback);
        return response;
    } catch (err) {
        console.log('Error getting documents', err)
    }
}

let setCookiesAndForward = (json) => {
    if(json.success){
        window.location.hash = Urls.appAccount
    }else{
        console.log("ERROR")
    }
};

let postDateUpdateCallback = (json) => {
    if(json.success){
        Utils.grr(json.message)
        window.location.hash = Urls.appAccount
   }else{
        Utils.grr(json.message, "error");
   }
};

let postDateUpdate = async (username, newBillingDate, password) => {
    let body = JSON.stringify({
        'new-billing-date':newBillingDate,
        'username':username,
        'password':password
    })
    Fetcher.post(Urls.apiAccount, body, postDateUpdateCallback);
}

let account = ""
var months = {0:'January', 1:'Feburary', 2:'March', 3:'April', 4:'May', 5:'June', 6:'July', 7:'August', 8:'September', 9:'October', 10:'November', 11:'December'};

let UserChangeBillingDate = {

    render : async () => {
        let request = Utils.parseRequestURL()
        account = await getAccount()
        
        return /*html*/`
     
            <div class="header_container ">
                <div class="header_container_inner">
                    <div class="header_left" style="display: flex;align-items: center;"> 
                        <a href="${Urls.appRoot}${Urls.appAccount}" style="display: flex;align-items: center;">
                            <img class="back-arrow" src="assets/img/chevron.png">
                            <h1 class="">
                            <span class="">Change Billing Date</span>
                                
                            </h1>
                        </a>
                    </div>
                </div>
            </div>
            <div class="body_container">
                <form>
                    <section class="page">
                        <div class="row">
                            <div class="col">
                                <blockquote>Update your billign date here. At this time you can only select days 1-28 as these are the only days that are common to all of the months. </blockquote>
                            </div>
                        </div>     
                        <div class="row ${account.has_card ? '' : 'hidden'}">
                            <div class="col">
                                <h2>Billing Day of Month</h2>
                            </div>
                        </div>    
                        <div class="row " style="flex-wrap:unset;">
                            <div class="col" style="min-width:unset;flex-grow:0;padding-right:10px;">
                                <input type="hidden" id="username-input" value="${account.username}">
                                <select id="new-billing-date" class="select-css">
                                    <option value="1">01</option>
                                    <option value="2">02</option>
                                    <option value="3">03</option>
                                    <option value="4">04</option>
                                    <option value="5">05</option>
                                    <option value="6">06</option>
                                    <option value="7">07</option>
                                    <option value="8">08</option>
                                    <option value="9">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                </select>
                            </div>
                            <div class="col" style="min-width:unset;flex-grow:0;padding-right:10px;">
                                <p id="response"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h2>Current Password (for security)</h2>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col">
                                <input type="password" id="password" value="" placeholder="Enter password">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button class="btn btn-border is-primary" id="save_btn">Change Billing Date <span id="button-extra"></span></button>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        `
    }
    , after_render: async () => {

        selectElement('new-billing-date', account.invoice_day_of_month);
        let month = new Date().getMonth();
        let nextBill = `Your next invoice will be due ${months[month]} ${account.invoice_day_of_month}.`;
        let response       = document.getElementById("response");
        response.innerHTML = `${nextBill}`;

        document.getElementById("save_btn").addEventListener ("click",  () => {
            Utils.grr(``, "hide");
            let username       = document.getElementById("username-input");
            let newBillingDate       = document.getElementById("new-billing-date");
            let password       = document.getElementById("password");
            if (username.value =='' || newBillingDate.value =='' || password.value =='') {
                Utils.grr(`The fields cannot be empty`, 'warning');
            }else {
                //alert(`User with email ${email.value} was successfully submitted!`)
                postDateUpdate(username.value, newBillingDate.value, password.value);
                
            }    
        })

        document.getElementById("new-billing-date").addEventListener("change", (e) => {
            let newBillingDate       = document.getElementById("new-billing-date").value;
            let response       = document.getElementById("response");
            let buttonExtra       = document.getElementById("button-extra");
            let diff = account.invoice_day_of_month - newBillingDate;
            let today = new Date().getDate();
            let diff_today = today - newBillingDate;
            buttonExtra.innerHTML = ``;
        
            if(diff == 0){
                let nextBill = `Your next invoice will be due ${months[month]} ${newBillingDate}.`;
                response.innerHTML = `${nextBill}`;
            }else if(diff > 0 && diff_today >= 0){
                let credit = Urls.appPriceFloat - (Urls.appPriceFloat/30) * diff;
                let nextBill = `If you save this choice, your next invoice will be due ${months[month + 1]} ${newBillingDate}.`;
                if(account.account_credit){
                    let credit_total = credit;
                    credit = credit - account.account_credit
                    if(credit <= 0){
                        credit = 0;
                    }
                    response.innerHTML = `${nextBill} An invoice of $${credit.toFixed(2)} ($${credit_total.toFixed(2)} - $${account.account_credit.toFixed(2)} Account Credit) will be charged today when you save.`;
                }else{
                    response.innerHTML = `${nextBill} An invoice of $${credit.toFixed(2)} will be charged today when you save.`;
                }
                if(credit > 0){
                    buttonExtra.innerHTML = `& Pay $${credit.toFixed(2)}`
                }
            }else if(diff > 0 ){
                let credit = (Urls.appPriceFloat/30) * diff;
                let nextBill = `If you save this choice, your next invoice will be due ${months[month]} ${newBillingDate}.`;
                response.innerHTML = `${nextBill} A credit of $${credit.toFixed(2)} will be added to your account.`;
            }else{
                let credit = (Urls.appPriceFloat/30) * diff;
                credit = -credit;
                let nextBill = `If you save this choice, your next invoice will be due ${months[month]} ${newBillingDate}.`;
                if(account.account_credit){
                    let credit_total = credit;
                    credit = credit - account.account_credit
                    if(credit <= 0){
                        credit = 0;
                    }
                    response.innerHTML = `${nextBill} An invoice of $${credit.toFixed(2)} ($${credit_total.toFixed(2)} - $${account.account_credit.toFixed(2)} Account Credit) will be charged today when you save.`;
                }else{
                    response.innerHTML = `${nextBill} An invoice of $${credit.toFixed(2)} will be charged today when you save.`;
                }
                if(credit > 0){
                    buttonExtra.innerHTML = `& Pay $${credit.toFixed(2)}`
                }
            }
        })
    }
}

export default UserChangeBillingDate;