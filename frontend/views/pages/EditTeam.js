import Fetcher        from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'
import Urls            from '../../services/Urls.js'

function singleTeamCallback(json){
    return json
}

let getTeam = async (slug) => {
    try {
        const response = await Fetcher.get(Urls.apiSingleTeam + slug, singleTeamCallback);
        return response;
    } catch (err) {
        console.log('Error getting documents', err)
    }
}

let postResponse = (json) => {
    if(json['success']){
        Utils.grr("Team URL udpated")
        window.location.hash = Urls.appTeam + json.data.new_slug

   }else{
        document.getElementById("update-team").classList.remove('action');
        document.getElementById('loader').classList.remove('loaderFadeInUp');     
        document.getElementById('loader').classList.add('loaderFadeOutDown');   
        Utils.grr(json.message, "error");
   }
};

let deleteResponse = (json) => {
    if(json['success']){
        Utils.grr("Team deleted")
        window.location.hash = Urls.appHome
   }else{
    document.getElementById("delete-team-for-real").classList.remove('action');
    document.getElementById('loader').classList.remove('loaderFadeInUp');     
    document.getElementById('loader').classList.add('loaderFadeOutDown');   
    Utils.grr(json.message, "error");
   }
};


let deleteTeam = async (team) => {
    Fetcher.delete(Urls.apiSingleTeam + team, deleteResponse);
}

let updateTeam = async (username, password, team, newSlug) => {
    let body = JSON.stringify({
        'username':username,
        'password':password,
        'slug':team,
        'new_slug':newSlug
    })
    Fetcher.post(Urls.apiSingleTeam + team, body, postResponse);
}

let showModal = () => {
    document.getElementById("are-you-sure").classList.remove("hidden");
}

let hideModal = () => {
    document.getElementById("are-you-sure").classList.add("hidden");
}

let formatDate = (date) => {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [month, day, year].join('/');
}

let EditTeam = {

    render : async () => {
        let request = Utils.parseRequestURL()
        let team = await getTeam(request.slug)
        if(!team){
            window.location.hash = Urls.appHome;
        }
        return /*html*/`
            <style>
                .border{
                    border-color: ${team.current_presence ? '#5FD321' : '#E0E0E0;'};
                }
            </style>
            <div class="header_container ">
                <div class="header_container_inner" style="">
                    <div class="header_left" style="display: flex;align-items: center;"> 
                        
                        <a href="${Urls.appRoot}${Urls.appTeam}${team.slug}" style="display: flex;align-items: center;">
                            <img class="back-arrow" src="assets/img/chevron.png"><img class="back-icon" src="assets/img/settings.png" style="margin-right:5px;top:0;"><h1 class="">${team.slug}</h1>
                        </a>
                    </div>

                </div>
            </div>
            <div class="body_container">
                <form>
                    <section class="page">
                    <div class="row mb-1">
                    <div class="col">
                        <h2>Team Billing</h2>
                        <p class="mt-1 mb-1">Your next invoice for '${team.slug}' team is <strong>${formatDate(team.next_invoice_date)}</strong></p>
                        <blockquote>If you need to change your billing date, <a style="text-decoration:underline" href="${Urls.appSupportHref}">contact support</a>.</blockquote>
                        
                    </div>
                </div>  
                             
                        <div class="row">
                            <div class="col">
                                <h2>Team Login</h2>    
                                <blockquote class="mt-1 mb-1">If you’ve changed your teamname, username or password, please enter all of that information here for security purposes.</blockquote>
                                <h3>Team URL</h3>
                            </div>
                        </div>    
                        <div class="row mb-1">
                            <div class="col">
                            <input id="new-team-name" type="text" value="${team.slug}" placeholder="Enter Team Url" style="width: calc(480px - 110px);max-width: calc(90% - 110px);"> .slack.com
                                <input type="hidden" id="team-name" value="${team.uuid}">
                            </div>
                        </div>
                        
                    
                        
                        <div class="row">
                            <div class="col">
                                <h3>Team Username</h3>
                            </div>
                        </div>    
                        <div class="row mb-1">
                            <div class="col">
                                <input id="email_input" type="text" placeholder="Enter Team Username">
                            </div>
                        </div>
                    </section>
                    <section class="page">
                        <div class="row with-checkbox">
                            <div class="col">
                                <h3>Team Password</h3>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col">
                                <input id="pass_input" type="text" placeholder="Enter Team Password">
                            </div>
                        </div>
                        <div class="row mb-2" style="margin-top:25px;">
                            <div class="col">
                                <a id="update-team" class="btn btn-border">Save</a>
                            </div>
                        </div>
                        

                        
                    </section>
                </form>
                <section class="page " style="border-top: 1px dashed #D5D9DE;">
                <div class="row mb-1 mt-2" style="">
                            <div class="col">
                                <blockquote>If you're finished with your team, delete it now. When a team is deleted it will be deleted from our servers immediately and any remaining credit will be refunded.</blockquote>
                                <blockquote>Refunds are calculated by subtracting the Stripe fee ($10 * 2.9% )+ $0.30 = $0.59 minus days used, rounded to the nearest 1 day. For example, if you subscribed and immediately canceled you’d be charged for 1 day and the Stripe fee. </blockquote>
                            </div>
                        </div>
                    <div class="row mt-1">
                        <div class="col">
                            <a id="delete-team" class="btn btn-border delete" style="">Delete Team</a>
                        </div>
                    </div>
                    
                </section>
                <div class="modal-container hidden" id="are-you-sure">
                    <div class="modal">
                        <div class="modal-header" style="display:none;">
                            <div class="header_left"><h1>Delete "${team.slug}"</h1></div>
                            <div class="header_right">
                                <a id="close-modal" class="btn btn-link">Close</a>
                            </div>
                        </div>
                        <div class="modal-body">
                            <section class="page">
                                <div class="row" >
                                    <div class="col">
                                        <h2 class="mb-1" style="text-align:center;display:block;">Are you sure you want to delete "${team.slug}"? </h2>
                                        <blockquote>The team will be permanately deleted immediately and any remaining credit will be refunded to the payment method used to with this account.</blockquote>
                                        <blockquote>Refunds are calculated by subtracting the Stripe fee ($10 * 2.9% )+ $0.30 = $0.59 minus days used, rounded to the nearest 1 day. For example, if you subscribed and immediately canceled you’d be charged for 1 day and the Stripe fee. </blockquote>
                                        
                                    </div>
                                </div>  
                                <div class="row" style="margin-top:25px;text-align:center;">
                                    <div class="modal-col col">
                                        <a id="delete-team-for-real" class="btn btn-border modal-btn">Yes</a>
                                   
                                        <a id="close-modal-2" class="btn btn-border modal-btn">No</a>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>

            </div>
        `
    }
    , after_render: async () => {
        document.getElementById("update-team").addEventListener ("click",  () => {
            Utils.grr(``, "hide");
            let team       = document.getElementById("team-name");
            let newSlug       = document.getElementById("new-team-name");
            let email       = document.getElementById("email_input");
            let pass        = document.getElementById("pass_input");
            if (email.value =='' | pass.value == '') {
                Utils.grr(`The fields cannot be empty`, "warning");
            }else {
                document.getElementById("update-team").classList.add('action');
                document.getElementById('loader').classList.remove('loaderFadeOutDown');
                document.getElementById('loader').classList.add('loaderFadeInUp');  
                updateTeam(email.value, pass.value, team.value, newSlug.value);
                
            }    
        });
        
        document.getElementById("delete-team").addEventListener ("click",  () => {
            // let team       = document.getElementById("team-name");
            // deleteTeam(team.value);
            showModal();
        });
        
        document.getElementById("close-modal").addEventListener ("click",  () => {
            // let team       = document.getElementById("team-name");
            // deleteTeam(team.value);
            hideModal();
        });
        
        document.getElementById("close-modal-2").addEventListener ("click",  () => {
            // let team       = document.getElementById("team-name");
            // deleteTeam(team.value);
            hideModal();
        });
        
        document.getElementById("delete-team-for-real").addEventListener ("click",  () => {
            let team       = document.getElementById("team-name");
            document.getElementById("delete-team-for-real").classList.add('action');
            document.getElementById('loader').classList.remove('loaderFadeOutDown');
            document.getElementById('loader').classList.add('loaderFadeInUp');
            deleteTeam(team.value);
            
        });

    }
}

export default EditTeam;