import Urls            from '../../services/Urls.js'

let Privacy = {

    render : async () => {
        let view =  /*html*/`
        <style>
        .hide-mobile{
            display: table-cell;
        }
        @media screen and (max-width: 719px) {
            .hide-mobile{
                display: none !important;
            }
        }
        .body_container{
            background:transparent;
            
            // color:#D5D9DE;
        }
        section a, section a:hover, section a:visited{
            border-bottom:2px solid #0060BF;color:#0060BF
        }

        .body_container.inactive{
            // border: 1px solid #435264;
            // background:#242949;
            // color:#D5D9DE;

        }
        
        </style>
        <div class="header_container ">
        <div class="header_container_inner" style="">
        <div class="header_left " style="display: flex;align-items: center;"> 
        <a onclick="history.back()" style="display: flex;align-items: center;">
            <img class="back-arrow" src="assets/img/chevron.png">
            <h1 class="truncate ">
                
                <span class="">Privacy Policy</span>
            </h1>
        </a>
    </div>
        </div>
    </div>
        <div class="body_container inactive">
        <section class="page">
            <div class="">
                <div class="row">
                    <div class="col">
                        <blockquote class="mb-1">Updated January 15, 2020</blockquote>
                        <ul class="register-list">
                            <li>By default we only store the following information personal information in our database:
                            <ul>
                            <li>email address for status hammer (not teams)</li>
                            <li>hashed password</li>
                            <li>team urls</li>
                            <li>All data you create via the interface (we have to so we can use it)</li>
                            </ul></li>
                            <li>On the cloud machine we securely store the following:

                            <ul>
                            <li>email address for team</li>
                            <li>password for team</li>
                            </ul></li>
                            <li>We store the following cookies to manage login

                            <ul>
                            <li>access token - used for accessing the server</li>
                            <li>refresh token - used for getting new access token</li>
                            <li>crsf access token - (manage security communication between the interface and the server)</li>
                            <li>crsf refresh token - (manage security communication between the interface and the server)</li>
                            <li>desiredPreLoginPage - basically the page you were trying to see before we realized you needed to login first</li>
                            </ul></li>
                            <li>3rd party libraries and services we use

                            <ul>
                            <li>Stripe's api on our server (not in the browser) - <a href="https://stripe.com/docs/api">https://stripe.com/docs/api</a></li>
                            <li>Zendesk for support request management - <a href="https://www.zendesk.com/support/">https://www.zendesk.com/support/</a></li>
                            <li>SendGrid for email notifications - <a href="https://sendgrid.com/solutions/email-api/">https://sendgrid.com/solutions/email-api/</a></li>
                            <li>Sentry.io for code error monitoring - <a href="https://sentry.io/">https://sentry.io/</a></li>
                            <li>Rome Datepicker - <a href="https://github.com/bevacqua/rome">https://github.com/bevacqua/rome</a></li>
                            <li>Swipe Slider - <a href="https://github.com/lyfeyaj/swipe">https://github.com/lyfeyaj/swipe</a></li>
                            </ul></li>
                            <li>What we DO NOT do

                            <ul>
                            <li>track anything you do on the site</li>
                            <li>use any 3rd party software for tracking</li>
                            <li>use any 3rd party javascript libraries at all</li>
                            <li>And while we do monitor what the server and cloud machines are doing, we're not reviewing anything other than uptime, and successful correlation between data in the server and the values set in the managed instances</li>
                            </ul></li>
                            <li>If you have any questions or would like to request a copy of any data we have <a href="mailto:support@statushammer.com">please contact us</a>.</li>
                        </ul>
                    </div>
                </div> 
            </div>
        </section>
    </div>
        `
        return view
    }
    , after_render: async () => {
    }
}
export default Privacy;