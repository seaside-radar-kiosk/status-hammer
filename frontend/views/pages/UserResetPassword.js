import Cookies        from '../../services/Cookies.js'
import Urls            from '../../services/Urls.js'
import Fetcher            from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'


let setCookiesAndForward = (json) => {
    if(json['success']){
        Utils.grr('Successfully reset, use your new password to login.');
        window.location.hash = Urls.appLogin;
   }else{
        Utils.grr(json['message'], 'warning');
   }
};

let postCredentials = async (password, reset_code) => {
    let body = JSON.stringify({
        'password': password,
        'reset_code':reset_code
    })
    Fetcher.postWithoutRefresh(Urls.apiReset, body, setCookiesAndForward);
}

let parseRequestURL2 = (theLocation = location) => {

    let url = theLocation.hash.slice(1) || '/';
    let r = url.split("/")
    let request = {
        resource    : null,
        slug        : null,
        verb        : null
    }
    request.resource    = r[1]
    request.slug        = r[2]
    request.verb        = r[3]

    return request
}

let UserResetPassword = {

    render: async () => {
        let request = parseRequestURL2()

        return /*html*/ `
        <style>
        .body_container{
            margin-bottom:0;
        }
        #page_container{
            justify-content: center;
        }
        input[type="text"], input[type="email"], input[type="password"], input[type="time"]{
            width:calc(100% - 20px) !important
        }
        </style>
        <div class="body_container skinny very">
            <section class="page">
                <div class="">
                    <form>
                    <div class="row">
                    <div class="col">
                        <a href="${Urls.appRoot}${Urls.appLanding}"><img src="/assets/img/icon.png" class="header-logo"></a>
                        
                    </div>
                </div>  
                <div class="row mb-1">
                    <div class="col" >
                        
                        <h1 class="team-slug">Password Reset</h1>
                    </div>
                </div>       
                        <div class="row">
                            <div class="col">
                                <h3 >New Password</h3>
                                <input class="input" id="pass_input" type="password" placeholder="New password">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h3 >Repeat New Password</h3>
                                <input class="input" id="repeat_pass_input" type="password" placeholder="New password again">
                                <input class="input" id="reset_code" type="hidden" value="${request.slug}">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col">
                                <button class="btn btn-border is-primary" id="login_submit_btn">
                                Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
        `
    }
    // All the code related to DOM interactions and controls go in here.
    // This is a separate call as these can be logined only after the DOM has been painted
    , after_render: async () => {
        document.getElementById("login_submit_btn").addEventListener ("click",  () => {
            let pass        = document.getElementById("pass_input");
            let pass_repeat        = document.getElementById("repeat_pass_input");
            let reset_code        = document.getElementById("reset_code");
            if (pass.value =='' | pass_repeat.value == '' | reset_code.value == '') {
                Utils.grr('The fields cannot be empty', 'warning');
            }else {
                //alert(`User with email ${email.value} was successfully submitted!`)
                postCredentials(pass.value, reset_code.value);
                
            }    
        })
    }
}

export default UserResetPassword;