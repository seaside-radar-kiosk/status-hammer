import Cookies        from '../../services/Cookies.js'
import Urls            from '../../services/Urls.js'
import Fetcher            from '../../services/Fetcher.js'
import Helpers            from '../../services/Helpers.js'
import Utils        from '../../services/Utils.js'

let setCookiesAndForward = (json) => {
    if(json['csrf_access_token'] && json['csrf_refresh_token']){
        Cookies.setCookie('csrf_access_token', json['csrf_access_token'],7);
        Cookies.setCookie('csrf_refresh_token', json['csrf_refresh_token'],7);
        window.location.hash = Urls.appFirstTeam;
   }else{
        document.getElementById("register_submit_btn").classList.remove('action');
        Utils.grr(json.message, "error");
        Cookies.eraseCookie('csrf_access_token');
        Cookies.eraseCookie('csrf_refresh_token');
        document.getElementById('loader').classList.remove('loaderFadeInUp');     
        document.getElementById('loader').classList.add('loaderFadeOutDown');   
   }
};

let postCredentials = async (username, password, ccnumber, ccmonth, ccyear, cccvc) => {
    let body = JSON.stringify({
        'new-cc-number':ccnumber,
        'new-cc-exp-month':ccmonth,
        'new-cc-exp-year':ccyear,
        'new-cc-cvc':cccvc,
        'username':username,
        'password':password
    })
    Fetcher.postWithoutRefresh(Urls.apiRegister, body, setCookiesAndForward);
}

let checkValue = (element, checker = false) => {
    element.classList.remove('input_error');
    if(element.value == ''){
        element.classList.add('input_error');
        return true;
    }else{
        return checker;
    }
}

let cc_format = (value) => {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    var matches = v.match(/\d{4,16}/g);
    var match = matches && matches[0] || ''
    var parts = []
    for (let i=0, len=match.length; i<len; i+=4) {
      parts.push(match.substring(i, i+4))
    }
    if (parts.length) {
      return parts.join(' ')
    } else {
      return value
    }
  }

let UserRegister = {

    render: async () => {
        return /*html*/ `
            <style>
                .display{
                    font-size:1.25em;
                    font-weight:normal;
                }
                input[type="text"], input[type="email"], input[type="password"], input[type="time"]{
                    width:calc(100% - 20px) !important
                }
                .body_container{
                    margin-bottom:0;
                    padding-top:0;
                }
                .header_container{
                    padding-bottom: 10px;
                    // margin-left: 25px;  
                }
                .input.input_error, .select-css.input_error, input[type="checkbox"].input_error + label::before{
                    background: #FDBC16 !important;
                }
            </style>
            <div class="header_container ">
                <div class="header_container_inner" style="">
                    <div class="header_left" style="display: flex;align-items: center;"> 
                        <div class="row">
                            <div class="col">
                                <a href="${Urls.appRoot}${Urls.appLanding}"><img src="/assets/img/icon.png" class="header-logo"></a>
                                
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
            <div class="body_container skinny">
                <section class="page "> 
                    <div class="row mb-1">
                        <div class="col">
                        <h1 class="">Registration</h1>
                        <blockquote class="mb-1">All fields are required</blockquote>
                        </div>
                    </div> 
                            <div class="row">
                                <div class="col">
                                    <h3 >Email for Status Hammer (not your slack username)</h3>
                                    <input class="input" id="email_input" type="email" placeholder="Enter email" value="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <h3 >Password for Status Hammer (not your slack username)</h3>
                                    <input class="input" id="pass_input" type="password" placeholder="Enter password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <h3 >Repeat Password</h3>
                                    <input class="input" id="repeat_pass_input" type="password" placeholder="Enter password again">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <h2>Credit Card #</h2>
                                    <blockquote class="mb-1">Will not be charged until you create a team in the next step</blockquote>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col">
                                    <input type="text" id="new-cc-number" value="" placeholder="1234 1234 1234 1234" class="input">
                                    <input type="hidden" id="username-input" value="">
                                </div>
                            </div>
                            <div class="row " style="flex-wrap:unset;">
                                <div class="col" style="min-width:unset;flex-grow:1;padding-right:10px;">
                                    <select id="new-cc-exp-month" class="input select-css" style="width:100%;">
                                        <option value="1">01</option>
                                        <option value="2">02</option>
                                        <option value="3">03</option>
                                        <option value="4">04</option>
                                        <option value="5">05</option>
                                        <option value="6">06</option>
                                        <option value="7">07</option>
                                        <option value="8">08</option>
                                        <option value="9">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </div>
                                <div class="col" style="min-width:unset;flex-grow:1;padding-right:10px;">
                                    <select id="new-cc-exp-year" class="input select-css"  style="width:100%;">
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                        <option value="2031">2031</option>
                                        <option value="2032">2032</option>
                                        <option value="2033">2033</option>
                                        <option value="2034">2034</option>
                                        <option value="2035">2035</option>
                                        <option value="2036">2036</option>
                                        <option value="2037">2037</option>
                                        <option value="2038">2038</option>
                                        <option value="2039">2039</option>
                                        <option value="2040">2040</option>
                                    </select>
                                </div>
                                <div class="col" style="min-width:unset;flex-grow:1;">
                                    <input type="text" id="new-cc-cvc" value="" placeholder="CVC"  style="width:100%;" class="input">
                                </div>
                            </div>
                            
                            <div class="row mt-2">
                                <div class="col mb-1">
                                <blockquote class="mb-1">Its short if you actually wanna read it: <a href="${Urls.appRoot}${Urls.appPrivacy}" class="btn-tiny">Read Privacy Policy</a> - but  gist is we only store what we have to to bill, manage account and communicate with you. We won't ever sell your data to anyone, or use it for weird stuff.</blockquote>    
                                <input type="checkbox" id="privacy-policy" name="privacy-policy">   
                                    <label for="privacy-policy"style="display:flex;"><span>I agree to the <strong>Privacy Policy</strong></span></label>
                                    
                                </div>
                            </div>
                            <div class="row mb-1 mt-2">
                                <div class="col ">
                                <blockquote class="mb-1">Its short if you actually wanna read it: <a href="${Urls.appRoot}${Urls.appTOS}" class="btn-tiny">Read Terms of Service</a> - but like the privacy policy, we only store what we have to, we will update the softwrae, we'll let you know if we update the agreement, and we can cancel accounts at any time for any reason (for like jerks and stuff).</blockquote>    
                                <input type="checkbox" id="terms-of-service" name="terms-of-service">   
                                    <label for="terms-of-service" style="display:flex;"><span>I agree to the <strong>Terms of Service</strong></span></label>
                                    
                                </div>
                            </div>
                            
                            <div class="row mb-2">
                                <div class="col">
                                    <button class="btn btn-border is-primary" id="register_submit_btn">Register</button>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:100px;">
                                <div class="col">
                                    <a href="#/login" class="small">> Nevermind, I already have an account take me back to login.</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        `
    }
    // All the code related to DOM interactions and controls go in here.
    // This is a separate call as these can be registered only after the DOM has been painted
    , after_render: async () => {
        document.getElementById('new-cc-number').addEventListener ("keyup",(e) => {
            document.getElementById('new-cc-number').value = cc_format(document.getElementById('new-cc-number').value);
        });

        document.getElementById("register_submit_btn").addEventListener ("click",  (e) => {
            Utils.grr(``, "hide");
            e.preventDefault();
            let email       = document.getElementById("email_input");
            let pass        = document.getElementById("pass_input");
            let repeatPass  = document.getElementById("repeat_pass_input");
            let newCcNumber       = document.getElementById("new-cc-number");
            let newCcCvc       = document.getElementById("new-cc-cvc");
            let newCcExpMonth       = document.getElementById("new-cc-exp-month");
            let newCcExpYear       = document.getElementById("new-cc-exp-year");
            
            let privacyPolicy       = document.getElementById("privacy-policy");
            let termsOfService       = document.getElementById("terms-of-service");

            let requiredFields = false;

            requiredFields = checkValue(email, requiredFields);
            requiredFields = checkValue(pass, requiredFields);
            requiredFields = checkValue(repeatPass, requiredFields);
            requiredFields = checkValue(newCcNumber, requiredFields);
            requiredFields = checkValue(newCcCvc, requiredFields);
            
            if (!Helpers.validateEmail(email.value)) {
                email.classList.add("input_error");
                requiredFields = true;
            }
            privacyPolicy.classList.remove('input_error');
            if (!privacyPolicy.checked){
                privacyPolicy.classList.add('input_error');
                requiredFields = true;
            }
            
        
            termsOfService.classList.remove('input_error');
            if (!termsOfService.checked){
                termsOfService.classList.add('input_error');
                requiredFields = true;
            }
            if(requiredFields){
                
            }else {
                if (pass.value != repeatPass.value) {
                    Utils.grr(`Please the passwords don't match.`, "warning");
                }else{
                    document.getElementById('loader').classList.remove('loaderFadeOutDown');
                    document.getElementById('loader').classList.add('loaderFadeInUp');  
                    document.getElementById("register_submit_btn").classList.add('action');
                    postCredentials(email.value, pass.value, newCcNumber.value, newCcExpMonth.value, newCcExpYear.value, newCcCvc.value);    
                }
            }    
        })
    }
}

export default UserRegister;