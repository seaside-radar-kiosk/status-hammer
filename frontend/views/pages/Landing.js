import Urls            from '../../services/Urls.js'
import Cookies        from '../../services/Cookies.js'

let Landing = {

    render : async () => {
        let login_link = `<a href="${Urls.appRoot}${Urls.appLogin}" class="btn btn-border login-btn" style="">Login</a>`;
        if(Cookies.getCookie('csrf_access_token')){
            login_link = `<a href="${Urls.appRoot}${Urls.appHome}" class="btn btn-border login-btn" style="">Teams</a>`;
        }
        let view =  /*html*/`
        <style>
        #body{
            padding:0;
            max-width: 100%;
        }
        .header_container{
            padding:40px  ;
            padding-bottom:0;
            font-size:14px;
        }
        .btn{
            font-size:14px !important;
        }
        #footer_container{
            padding:40px;
        }
        .row.half{
            flex-wrap:nowrap;
            align-items:center;
        }
        .page1{
            max-width:720px;
            margin: 0 auto;
            padding: 0 20px;
            
        }
        .page2{
            max-width:1200px;
            margin: 0 auto;
            padding: 20px;
            padding-top:0;
        }
        .page3{
            // max-width:1240px;
            // border: 1px solid #E9ECF0;
            background: #E9ECF0;
            // padding: 0 40px;
            // margin: 25px auto;
            //animation: colorchange 7s infinite; /* animation-name followed by duration in seconds*/
               /* you could also use milliseconds (ms) or something like 2.5s */
            //-webkit-animation: colorchange 7s infinite; /* Chrome and Safari */
        }
        .body_container h1, .body_container h2{
            display:block;
        }
        h1{
            text-align:center;font-size:54px !important;line-height: 64px;
            margin-top:50px;
                margin-bottom:50px;
        }
        h2{
            text-align:center;font-size:48px !important;line-height: 62px;
            margin-top:100px;
            margin-bottom:50px;
        }
        h3{
            text-align:center;font-size:36px !important;line-height: 45px;
            margin-top:50px;
            margin-bottom:50px;
        }

        h1 img{
            width:55px;
            margin-right:50px;
        }
        h1 img:last-child{
            margin-right:0;
        }
        .btn-center{
            display: inline-block;
            padding: 15px 45px;
            text-center;
            margin:0 auto;
            min-width:220px;
            margin-bottom:25px;
        }
        .header-text{
            width:120px;margin-left:10px
        }
        @media screen and (max-width:780px){
            .header-text{
                width:80px;margin-left:6px
            }
            h1{
                text-align:center;font-size:28px !important;line-height: 38px;
                margin-top:25px;
                margin-bottom:25px;
            }
            h2{
                text-align:center;font-size:24px !important;line-height: 34px;
                margin-top:50px;
                margin-bottom:50px;
            }
            h3{
                text-align:center;font-size:22px !important;line-height: 32px;
                margin-top:50px;
                margin-bottom:50px;
            }
            .page1{
                max-width:680px;
                margin: 0 auto;
            }
            h1 img{
                width:66px;
                margin-right:35px;
            }
            h1 img:last-child{
                margin-right:0;
            }
        }
        .show-mobile{
            display:none;
        }
        .hide-mobile{
            display:inline-block;
        }
        #slide-indicators{
            text-align:center;width:100%;
            padding-bottom:10px;
            position: relative;top: -22px;
        }
        
        @media screen and (max-width:767px){
            .header-text{
                width:60px;margin-left:6px
            }
            
            #slide-indicators{
                top:0;
            }
            .show-mobile{
                display:inline-block;
            }
            .hide-mobile{
                display:none;
            }
        }
        
        .login-btn{
            background:#A6AFBA;
        }
        
        @media screen and (max-width:560px){
            .login-btn{
                padding:6px 10px;
            }
            .header_container{
                padding:10px ;
                padding-bottom:0;
            }
            h1 img{
                width:44px;
                margin-right:25px;
            }
            h1 img:last-child{
                margin-right:0;
            }
        }
      
          @keyframes colorchange
          {
            0%   {background: #E9ECF0;}
            50%  {background: #5FD321;}
            100% {background: #E9ECF0;}
          }
      
          @-webkit-keyframes colorchange /* Safari and Chrome - necessary duplicate */
          {
            0%   {background: #E9ECF0;}
            50%  {background: #5FD321;}
            100% {background: #E9ECF0;}
          }
          .swipe {
            overflow: hidden;
            visibility: hidden;
            position: relative;
            width:100%;
          }
          .swipe-wrap {
            overflow: hidden;
            position: relative;
          }
          .swipe-wrap > div {
            float: left;
            width: 100%;
            position: relative;
            overflow: hidden;
          }
          
          .indicator{
              border-radius:100px;
              width:8px;
              height:8px;
              display:inline-block;
              background:transparent;
              border:1px solid #1b1b1b;
              margin-right:12px;
              transition:all 0.25s ease;
            //   box-shadow: 0 4px 12px rgba(0,0,0,0.25)
          }
          .indicator:last-child{
              margin-right:0;
          }
          .indicator.active{
            background:#1b1b1b !important;
        }
        .header_container_inner{
            max-width:100%
        }
        .body_container{
            max-width:100%;
            border-radius:0;
        }
        .btn-center{
            font-size:18px !important;
            padding:10px 20px !important;
            min-width:auto !important;
        }
        </style>
        <div class="header_container " style="background: #E9ECF0;">
                <div class="header_container_inner">
                    <div class="header_left" style="display: flex;align-items: center;"> 
                        <a href="${Urls.appRoot}${Urls.appHome}" style="display: flex;align-items: center;">
                            <img src="/assets/img/icon.png" class="header-logo"><img src="/assets/img/icon-text.png" class="header-text" style="display:none;"> 
                        </a>
                    </div>
                    <div class="header_right">
                       
                        
                        ${login_link}   
                    </div>
                </div>
            </div>
            
            
            <div class="body_container" style="margin-top:0;padding-top:0;background: #E9ECF0;">
            <section class="page1">
                <div class="">
                    <div class="row half">
                        <div class="col" style="">
                                <!-- <div style="overflow:visible;margin-bottom:-125px;margin-top:-150px;position:relative;z-index: -1;"><img src="/marketing/pesant2.png" style="width:100%;"><img src="/assets/img/logo.svg" style="display:none;opacity:0;width:100%;max-width:400px;"></div> -->
                                <div>
                                
                                
                                    <h1 style="" class="mb-1">Set and <span style="color:#FD2D55">schedule</span> your <span style="color:#5FD321">presence</span> for Slack.</h1>
                                    
                                    <p style="text-align:center;margin-bottom:0;padding-bottom:0;" class=""><a href="${Urls.appRoot}${Urls.appRegister}" class="btn-center btn btn-border is-primary" id="register_submit_btn" style="margin-bottom:0;">Register Now</a></p><p style="text-align:center;"><span style="font-size:14px;text-align:center;">$10 per team per month </span></p>
                                    
                                </div>
                                
                        </div>
                        
                    </div>    
                </div>
            </section>
            <section class="page3 half" >
                            
                            
                            <div id="slider" class="swipe">
                                <div class="swipe-wrap" id="slider-items">
                                    
                                    <div style="padding:0 40px;padding-top:40px;text-align:center;">
                                        <img class="hide-mobile" src="marketing/screenshots.png" style="width:100%;max-width:920px;">
                                        <img class="show-mobile" src="marketing/screenshot_2.png" style="width:100%;max-width:920px;">
                                    </div>
                                    <div style="padding:0 40px;padding-top:40px;text-align:center;">
                                        <img class="hide-mobile" src="marketing/screenshot_1.png" style="width:100%;max-width:920px;">
                                        <img class="show-mobile" src="marketing/screenshot_1.png" style="width:100%;max-width:920px;">
                                    </div>
                                </div>
                            </div>
                            <div id="slide-indicators" style=""></div>
                        
                    
                </section>
            
                </div>
                <div class="body_container" style="margin-top:0;padding-top:0;">
                
                
            
                    <section class="page1">
                        <div class="">
                            <div class="row" style="flex-wrap: unset;">
                                <div class="col" >
                                
                                <h2 class="mb-1">Control how others see you on slack (and schedule it)</h2>
                                
                                <p class="mb-2 mt-2" style="">Slack can be useful, but unless you regularly interact with the browser or keep the desktop software running your <span style="color:#5FD321">presense</span> - the <span style="color:#5FD321">green dot</span> next to your name - will be set to <span style="color:#A6AFBA">inactive</span>, showing bosses and clients you're not availble.</p> 
                                <p class="mb-2" style="font-size:1.3em;font-weight:bold;text-align:center;"><span>We’ve solved that.</span></p>
                                
                                <p class="mb-1">With Status Hammer you can...</p>
                                <ul class="mb-2">
                                    <li>Stay active all day without having slack open and running on your computer</li>
                                    <li><span style="color:#FD2D55">Schedule</span> when you want to be active and/or when you want to be inactive
                                    <li>even <span style="color:#FD2D55">schedule</span> holidays</li>
                                </ul>
                                
                                
                                
                                
                                
                                        
                                <p style="text-align:center;margin-bottom:0;padding-bottom:0;" class=""><a href="${Urls.appRoot}${Urls.appRegister}" class="btn-center btn btn-border is-primary" id="register_submit_btn" style="margin-bottom:0;">Register Now</a></p><p style="text-align:center;"><span style="font-size:14px;text-align:center;">$10 per team per month </span></p>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top:50px;">
                                <div class="col">
                                
                                    <h2 class="mb-1">How it works</h2>
                                    <p class="register-list mb-1">For each of your teams we create a custom cloud machine so we can interact with Slack on your behalf. </p>
                                    <p class="mb-1">We securely store your login information with the cloud machine (not in our database) and we don’t read your messages.</p>
                                    <p class="mb-1">We periodically update the management software so we can stay current with changes to Slack so no matter what changes are made, Status Hammer will continue to work for you. <p>
                                    
                                
                                    <blockquote class="mb-1" style="">If that doesn't make any sense, or if you have questions or concerns, Austin (creator &amp; developer) would be happy to explain and answer anything about the system that isn’t proprietary.<br><br>
                                    Feel free to <a style="border-bottom:2px solid #0060BF;color:#0060BF" href="${Urls.appSupportHref}">email us</a> or <a style="border-bottom:2px solid #0060BF;color:#0060BF" href="${Urls.appSupportCalendarly}" target="_blank">schedule a call</a>. </blockquote>
                                    
                                </div>
                            </div> 

                            <div class="row" style="margin-top:50px;">
                                <div class="col">
                                    
                                    <h2 class="mb-1">Follow Us</h2>
                                    <p class="register-list mb-1">We post periodic updates and marketing cartoons on <a style="border-bottom:2px solid #0060BF;color:#0060BF" href="https://twitter.com/statushammer">Twitter</a> and <a style="border-bottom:2px solid #0060BF;color:#0060BF" href="https://instagram.com/statushammer">Instagram</a>, follow us!<p>
                                    
                                    
                                </div>
                            </div> 

                            

                </section>
            </div>
        
        `
        return view
    }
    , after_render: async () => {
        let elem = document.getElementById('slider');
        let elem_inside = document.getElementById('slider-items');
    let count = elem_inside.getElementsByTagName('div').length;
    
    let indicators = "";
    for(let i = 0; i < count; i++){
        if(i == 0){
            indicators += `<div class="indicator active"></div>`;
        }else{
            indicators += `<div class="indicator"></div>`;            
        }

    }
    let theIndicators = document.getElementById('slide-indicators');
    theIndicators.innerHTML = indicators
        window.mySwipe = new Swipe(elem, {
            startSlide: 0,
            speed: 1000,
            auto: 5000,
            draggable: true,
            continuous: true,
            disableScroll: false,
            stopPropagation: false,
            callback: function(index, elem, dir) {
                for(let i = 0; i < count; i++){
                    theIndicators.getElementsByTagName('div')[i].classList.remove("active");   
                }
                theIndicators.getElementsByTagName('div')[index].classList.add("active");
                
            },
            transitionEnd: function(index, elem) {}
          });
       
    }
}
export default Landing;