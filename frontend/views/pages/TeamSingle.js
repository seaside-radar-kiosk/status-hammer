import Fetcher        from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'
import Urls            from '../../services/Urls.js'
import Router       from '../../services/Router.js'

function singleTeamCallback(json){
    return json
}

function selectElement(id, valueToSelect) {    
    let element = document.getElementById(id);
    element.value = valueToSelect;
}

function setScheduleElement(checkbox){
    if (checkbox) {
        document.getElementById('schedule-block').classList.remove('schedule-disabled');
        document.getElementById('exception-block').classList.remove('schedule-hide');
        document.getElementById('schedule-container').classList.remove('schedule-hide');        
    }else {
        document.getElementById('schedule-block').classList.add('schedule-disabled');  
        document.getElementById('exception-block').classList.add('schedule-hide');  
        document.getElementById('schedule-container').classList.add('schedule-hide');        
    }
}


let deleteResponse = (json) => {
    if(json['success']){
        Utils.grr("Team deactivated")
        window.location.hash = Urls.appHome
   }else{
    Utils.grr(json.message, "error");
   }
};


let deleteTeam = async (team) => {
    Fetcher.delete(Urls.apiSingleTeam + team, deleteResponse);
}


let restoreResponse = (json) => {
    if(json['success']){
        Utils.grr("Team was restored")
        hideRestore();
   }else{
        Utils.grr(json.message, "error");
   }
};
let restoreTeam = async (team) => {
    let body = JSON.stringify({
        'setBool':'team_is_deleted',
        'slug':team,
    })
    Fetcher.post(Urls.apiSingleTeam + team, body, restoreResponse);
}

let getTeam = async (slug) => {
    try {
        const response = await Fetcher.get(Urls.apiSingleTeam + slug, singleTeamCallback);
        return response;
    } catch (err) {
        console.log('Error getting documents', err)
    }
}

let updatePage = async (json) => {
    const content = null || document.getElementById('page_container');
    content.innerHTML = await TeamSingle.render();
    await TeamSingle.after_render();
};

let postPresenceActiveUpdateCallback = (json) => {
    if(json.success){
        let body = document.getElementById('body_container');
        let checkbox       = document.getElementById("presence-checkbox");
        checkbox.checked = json.data.presenceActive;
        if(checkbox.checked){
            body.classList.remove('inactive');
        }else{
            body.classList.add('inactive');
        }
        setTimeout(function(){
            updatePage();
        },500);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postPresenceActiveUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        'presenceActive': checkbox,
        'slug':slug,
        'setBool': 'presenceActive'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postPresenceActiveUpdateCallback);
}

let postDoNotDisturbActiveUpdateCallback = (json) => {
    if(json.success){
        let checkbox       = document.getElementById("donotdisturb-checkbox");
        checkbox.checked = json.data.doNotDisturbActive;
        setTimeout(function(){
            updatePage();
        },500);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};


let postDoNotDisturbActiveUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        'doNotDisturbActive': checkbox,
        'slug':slug,
        'setBool': 'doNotDisturbActive'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDoNotDisturbActiveUpdateCallback);
}

let postScheduleActiveUpdateCallback = (json) => {
    if(json.success){
        let checkbox       = document.getElementById("schedule-checkbox");
        checkbox.checked = json.data.scheduleActive;
        setScheduleElement(checkbox.checked)
        setTimeout(function(){
            updatePage();
        },500);
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postScheduleActiveUpdate = async (slug, checkbox) => {
    let body = JSON.stringify({
        'scheduleActive': checkbox,
        'slug':slug,
        'setBool': 'scheduleActive'
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postScheduleActiveUpdateCallback);
}

let postCurrentNotificationsTypeUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postCurrentNotificationsTypeUpdate = async (slug, radio) => {
    let body = JSON.stringify({
        'currentNotificationsType': radio,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postCurrentNotificationsTypeUpdateCallback);
}


let postTeamTimezoneUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postTeamTimezoneUpdate = async (slug, select) => {
    let body = JSON.stringify({
        'teamTimezone': select,
        'setTimezone': 'teamTimezone',
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postTeamTimezoneUpdateCallback);
}

let postDoNotDisturbStartUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postDoNotDisturbStartUpdate = async (slug, select) => {
    let body = JSON.stringify({
        'doNotDisturbStart': select,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDoNotDisturbStartUpdateCallback);
}


let postDoNotDisturbEndUpdateCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postDoNotDisturbEndUpdate = async (slug, select) => {
    let body = JSON.stringify({
        'doNotDisturbEnd': select,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postDoNotDisturbEndUpdateCallback);
}

function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
}

const setExceptionDate = (s) => {
    var d = new Date(s +":59");
    // parts = s.match(/(\d+)-(\d+)-(\d+)/),
    // year = parseInt(parts[1], 10),
    // month = parseInt(parts[2], 10) - 1,
    // day = parseInt(parts[3], 10),
    // hours = 23,
    // minutes = 59;
    // d.setFullYear(year);
    // d.setMonth(month);
    // d.setDate(day);
    
    // d.setMinutes(minutes);
    if(isValidDate(d)){
        d.setHours(d.getHours() - (d.getTimezoneOffset()/60))
        return d
    }else{
        return false
    }

}

let postExceptionCallback = (json) => {
    if(json.success){
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        console.log("ERROR")
    }
};

let postExceptionNew = async (slug, start_date, end_date) => {
    let body = JSON.stringify({
        'exceptionNew': true,
        'exceptionStartDate': start_date,
        'exceptionEndDate': end_date,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postExceptionCallback);
}

let postExceptionUpdate = async (slug, exception_id, start_date, end_date) => {
    let body = JSON.stringify({
        'exceptionUpdate': exception_id,
        'exceptionStartDate': start_date,
        'exceptionEndDate': end_date,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postExceptionCallback);
}

let postExceptionDelete = async (slug, exception_id) => {
    let body = JSON.stringify({
        'exceptionDelete': exception_id,
        'slug':slug,
    })
    Fetcher.post(Urls.apiSingleTeam + slug, body, postExceptionCallback);
}

let postCurrentStatusUpdateCallback = (json) => {
    if(json.success){
        Utils.grr("Status Updated @ " + timestampNow());
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        Utils.grr(json.message, "error");
    }
};

let postCurrentStatusUpdate = async () => {
    let textarea = document.getElementById("current-status-textarea");
    let slug       = document.getElementById("slug-input").value;
    if (slug.value =='') {
        alert (`The fields cannot be empty`)
    }else {
        let body = JSON.stringify({
            'currentStatus': textarea.value,
            'slug':slug,
        })
        Fetcher.post(Urls.apiSingleTeam + slug, body, postCurrentStatusUpdateCallback);
    }
    
}

let postCurrentNotificationKeywordsUpdateCallback = (json) => {
    if(json.success){
        Utils.grr("Keywords updated @ " + timestampNow());
        updatePage();
        // window.location.hash = Urls.appTeam + json.slug
    }else{
        Utils.grr(json.message, "error");
    }
};

let postCurrentNotificationKeywordsUpdate = async () => {
    let textarea = document.getElementById("current-notifications-keywords-textarea");
    let slug       = document.getElementById("slug-input").value;
    if (slug.value =='') {
        alert (`The fields cannot be empty`)
    }else {
        let body = JSON.stringify({
            'currentNotificationsKeywords': textarea.value,
            'slug':slug,
        })
        Fetcher.post(Urls.apiSingleTeam + slug, body, postCurrentNotificationKeywordsUpdateCallback);
    }
    
}

const timestampNow = () => {
    let options = {  
        weekday: "long", year: "numeric", month: "short",  
        day: "numeric", hour: "2-digit", minute: "2-digit", 
    };  
    let date = new Date();  
    return date.toLocaleTimeString("en-us");
}

let delayIntervalsKeywords = [];
let lastDelaySavedKeywords = "";

const delaySaveKeywords = (action) => {
    for (let i = 0; i < delayIntervalsKeywords.length; i++){
        clearInterval(delayIntervalsKeywords[i]);
    }
    delayIntervalsKeywords = [];
    let intervalId = setTimeout(() => {
        lastDelaySavedKeywords = timestampNow()
        action();
    }, 2000);
    delayIntervalsKeywords.push(intervalId);    
}

let delayIntervalsStatus = [];
let lastDelaySavedStatus = "";

const delaySaveStatus = (action) => {
    for (let i = 0; i < delayIntervalsStatus.length; i++){
        clearInterval(delayIntervalsStatus[i]);
    }
    delayIntervalsStatus = [];
    let intervalId = setTimeout(() => {
        lastDelaySavedStatus = timestampNow()
        action();
    }, 2000);
    delayIntervalsStatus.push(intervalId);    
}

let showModal = () => {
    document.getElementById("are-you-sure").classList.remove("hidden");
}

let hideModal = () => {
    document.getElementById("are-you-sure").classList.add("hidden");
}

let hideRestore = () => {
    document.getElementById("restore-team").classList.add("hidden");
}

let team = ""

let TeamSingle = {

    render : async () => {
        let request = Utils.parseRequestURL()
        team = await getTeam(request.slug)
        if(!team){
            window.location.hash = Urls.appHome;
        }
        return /*html*/`
        <style>
            .mb-2 {
                border-bottom: 1px solid #E0E3E9;
                padding-bottom: 25px !important;
                margin-bottom: 25px !important;
            }
        
            .no-border td {
                border-bottom: none;
            }
        
            .body_container {
                // background-color:rgba(255,255,255,0.95);
            }
        
            .scheduling_container {
                margin-left: -20px;
                margin-right: -20px;
            }
        
            body {
                overflow-x: hidden;
            }
        
            #body {
                width: calc(100% - 20px);
            }
        </style>
        <div class="header_container ">
            <div class="header_container_inner" style="">
                <div class="header_left" style="display: flex;align-items: center;">
                    <a href="${Urls.appRoot}${Urls.appHome}" style="display: flex;align-items: center;">
                        <img class="back-arrow" src="assets/img/chevron.png">
                        <h1 class="truncate ">
                            <span class="">${team.slug}</span>
                        </h1>
                    </a>
        
                </div>
                <div class="header_right">
                    <a href="${Urls.appRoot}${Urls.appEditTeam}${team.slug}" class="" style="margin-left:20px"><img
                            class="header-icon" src="assets/img/settings.png"></a>
                </div>
            </div>
        </div>
        <div id="body_container" class="body_container ${team.current_presence ? '' : 'inactive'}">
            <section class="page mb-2">
                <div class="row with-checkbox">
                    <div class="col">
                        <span style="display: flex;align-items: center;"><img class="team-icon "
                                src="assets/img/${team.current_presence ? 'active' : 'inactive'}.png">
                            <h2>Presence</h2>
                        </span>
                        <input type="hidden" id="slug-input" value="${team.slug}">
                    </div>
                    <div class="col text-right  no-grow">
                        <label id="presence-switch" class="switch">
                            <input id="presence-checkbox" type="checkbox" ${team.current_presence ? 'checked' : '' }>
                            <span class="slider presence round"></span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col ${team.current_presence_text? '' : 'hidden'}">
                        <span class=""> ${team.current_presence_text}</span> <span
                            class=" ${team.schedule_enabled ? '' : 'hidden'}">until @ ${team.schedule_active &&
                            team.current_presence ? team.schedule_active_end : team.schedule_active_start} <img
                                style="width:16px;position:relative;top:2px;" src="assets/img/calendar.png"></span>
                    </div>
                </div>
            </section>
            <section class="page mb-2">
                <div class="row with-checkbox">
                    <div class="col">
                        <span style="display: flex;align-items: center;"><img class="team-icon " src="assets/img/status.png">
                            <h2>Status</h2>
                        </span>
                    </div>
                </div>
                <div class="row" style="margin-bottom:0;">
                    <div class="col" style="margin-bottom:0;">
                        <p class="">
                            <input class="status-input" type="text" id="current-status-textarea" value="${team.current_status}">
                        </p>
                        <p class="mt-1 ${team.schedule_enabled ? '' : 'hidden'}"><img
                                style="width:16px;position:relative;top:2px;" src="assets/img/calendar.png"> @
                            ${team.schedule_active ? team.schedule_active_end : team.schedule_active_start} -
                            ${team.schedule_active ? team.schedule_inactive_status_text : team.schedule_active_status_text}</p>
                    </div>
                </div>
                <div class="row  mb-1">
                    <div class="col">
                        <blockquote>If you start your status with an emoji, this will be used to set the emoji next to your name. Otherwise, your status emoji will be 💬.</blockquote>
                    </div>
                </div>
            </section>
            <section class="page mb-2">
                <div class="row with-checkbox">
                    <div class="col">
                        <span style="display: flex;align-items: center;"><img class="team-icon "
                                src="assets/img/notifications.png">
                            <h2>Notifications</h2>
                            
                        </span>
                    </div>
                </div>
                
                <div class="row" style="margin-bottom:0;">
                    <div class="col" style="margin-bottom:0;">
                        <div class="radio radio-group">
                            <input type="radio" id="current-notifications-all" name="notifications" checked value="ALL">
                            <label class="radio-label" for="current-notifications-all">
                                <div>All new messages</div>
                            </label>
                        </div>
        
                        <div class="radio radio-group">
                            <input type="radio" id="current-notifications-keywords" name="notifications" value="KEYWORDS">
                            <label class="radio-label" for="current-notifications-keywords">
                                <div>
                                    <p>Direct messages, mentions & keywords (below)</p>
                                    <textarea class=""
                                        id="current-notifications-keywords-textarea">${team.current_notifications_keywords}</textarea>
                                    <p class="small"><em>Use commas to separate each keyword. Keywords are not case
                                            sensitive.</em></p>
                                </div>
                            </label>
                        </div>
        
                        <div class="radio radio-group">
                            <input type="radio" id="current-notifications-none" name="notifications" value="NONE">
                            <label class="radio-label" for="current-notifications-none">
                                <div>None</div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row ${team.schedule_enabled ? '' : 'hidden'}">
                    <div class="col">
                        <span class="mt-1"><img style="width:16px;position:relative;top:2px;" src="assets/img/calendar.png"> @
                            ${team.schedule_active ? team.schedule_active_end : team.schedule_active_start} -
                            ${team.schedule_active ? team.schedule_inactive_notifications_text :
                            team.schedule_active_notifications_text}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <blockquote>These control mobile notifications, not desktop.</blockquote>
                    </div>
                </div>
            </section>
            <section class="page" style="border-bottom:none;">
                <div class="row with-checkbox">
                    <div class="col">
                        <span style="display: flex;align-items: center;"><img class="team-icon " src="assets/img/moon.png">
                            <h2>Do Not Disturb</h2>
                        </span>
                    </div>
                    <div class="col text-right  no-grow">
                        <label id="donotdisturb-switch" class="switch">
                            <input id="donotdisturb-checkbox" type="checkbox" ${team.do_not_disturb_enabled ? 'checked' : '' }>
                            <span class="slider dnd round"></span>
                        </label>
                    </div>
                </div>
                <div class="row" style="border-bottom:none;">
                    <div class="col" style="border-bottom:none;">
                        <table class="no-border">
                            <tbody>
                                <tr>
                                    <td>
                                        Start
                                    </td>
                                    <td>
                                        <select id="do_not_disturb_start" class="select-css">
                                            <option value="0">12:00 AM</option>
                                            <option value="1">12:30 AM</option>
                                            <option value="2">1:00 AM</option>
                                            <option value="3">1:30 AM</option>
                                            <option value="4">2:00 AM</option>
                                            <option value="5">2:30 AM</option>
                                            <option value="6">3:00 AM</option>
                                            <option value="7">3:30 AM</option>
                                            <option value="8">4:00 AM</option>
                                            <option value="9">4:30 AM</option>
                                            <option value="10">5:00 AM</option>
                                            <option value="11">5:30 AM</option>
                                            <option value="12">6:00 AM</option>
                                            <option value="13">6:30 AM</option>
                                            <option value="14">7:00 AM</option>
                                            <option value="15">7:30 AM</option>
                                            <option value="16">8:00 AM</option>
                                            <option value="17">8:30 AM</option>
                                            <option value="18">9:00 AM</option>
                                            <option value="19">9:30 AM</option>
                                            <option value="20">10:00 AM</option>
                                            <option value="21">10:30 AM</option>
                                            <option value="22">11:00 AM</option>
                                            <option value="23">11:30 AM</option>
                                            <option value="24">12:00 PM</option>
                                            <option value="25">12:30 PM</option>
                                            <option value="26">1:00 PM</option>
                                            <option value="27">1:30 PM</option>
                                            <option value="28">2:00 PM</option>
                                            <option value="29">2:30 PM</option>
                                            <option value="30">3:00 PM</option>
                                            <option value="31">3:30 PM</option>
                                            <option value="32">4:00 PM</option>
                                            <option value="33">4:30 PM</option>
                                            <option value="34">5:00 PM</option>
                                            <option value="35">5:30 PM</option>
                                            <option value="36">6:00 PM</option>
                                            <option value="37">6:30 PM</option>
                                            <option value="38">7:00 PM</option>
                                            <option value="39">7:30 PM</option>
                                            <option value="40">8:00 PM</option>
                                            <option value="41">8:30 PM</option>
                                            <option value="42">9:00 PM</option>
                                            <option value="43">9:30 PM</option>
                                            <option value="44">10:00 PM</option>
                                            <option value="45">10:30 PM</option>
                                            <option value="46">11:00 PM</option>
                                            <option value="47">11:30 PM</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        End
                                    </td>
                                    <td>
                                        <select id="do_not_disturb_end" class="select-css">
                                            <option value="0">12:00 AM</option>
                                            <option value="1">12:30 AM</option>
                                            <option value="2">1:00 AM</option>
                                            <option value="3">1:30 AM</option>
                                            <option value="4">2:00 AM</option>
                                            <option value="5">2:30 AM</option>
                                            <option value="6">3:00 AM</option>
                                            <option value="7">3:30 AM</option>
                                            <option value="8">4:00 AM</option>
                                            <option value="9">4:30 AM</option>
                                            <option value="10">5:00 AM</option>
                                            <option value="11">5:30 AM</option>
                                            <option value="12">6:00 AM</option>
                                            <option value="13">6:30 AM</option>
                                            <option value="14">7:00 AM</option>
                                            <option value="15">7:30 AM</option>
                                            <option value="16">8:00 AM</option>
                                            <option value="17">8:30 AM</option>
                                            <option value="18">9:00 AM</option>
                                            <option value="19">9:30 AM</option>
                                            <option value="20">10:00 AM</option>
                                            <option value="21">10:30 AM</option>
                                            <option value="22">11:00 AM</option>
                                            <option value="23">11:30 AM</option>
                                            <option value="24">12:00 PM</option>
                                            <option value="25">12:30 PM</option>
                                            <option value="26">1:00 PM</option>
                                            <option value="27">1:30 PM</option>
                                            <option value="28">2:00 PM</option>
                                            <option value="29">2:30 PM</option>
                                            <option value="30">3:00 PM</option>
                                            <option value="31">3:30 PM</option>
                                            <option value="32">4:00 PM</option>
                                            <option value="33">4:30 PM</option>
                                            <option value="34">5:00 PM</option>
                                            <option value="35">5:30 PM</option>
                                            <option value="36">6:00 PM</option>
                                            <option value="37">6:30 PM</option>
                                            <option value="38">7:00 PM</option>
                                            <option value="39">7:30 PM</option>
                                            <option value="40">8:00 PM</option>
                                            <option value="41">8:30 PM</option>
                                            <option value="42">9:00 PM</option>
                                            <option value="43">9:30 PM</option>
                                            <option value="44">10:00 PM</option>
                                            <option value="45">10:30 PM</option>
                                            <option value="46">11:00 PM</option>
                                            <option value="47">11:30 PM</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
        
                    </div>
                </div>
            </section>
            <div class="scheduling_container">
                <section class="page schedule schedule-disabled" id="schedule-block">
                    <div class="row with-checkbox">
                        <div class="col">
                            <a href="#/edit-schedule/${team.slug}" style="display: flex;align-items: center;"><img
                                    class="team-icon " src="assets/img/calendar.png">
                                <h2>Schedule </h2>
                            </a>
                        </div>
                        <div class="col text-right  no-grow">
                            <label id="schedule-switch" class="switch">
                                <input id="schedule-checkbox" type="checkbox" ${team.schedule_enabled ? 'checked' : '' }>
                                <span class="slider schedule round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="row schedule-hide" id="schedule-container">
                        <div class="col">
        
                            <h3><span class="highlight-active">Active</span> days, hours, status and notifications</h3>
                            <table class="mb-1">
                                <tbody>
                                    <tr>
                                        <td>Days</td>
                                        <td id="schedule-active-days-cell">${team.schedule_active_sunday ? 'Sn, ' :
                                            ''}${team.schedule_active_monday ? 'M, ' : ''}${team.schedule_active_tuesday ? 'T, '
                                            : ''}${team.schedule_active_wednesday ? 'W, ' : ''}${team.schedule_active_thursday ?
                                            'Th, ' : ''}${team.schedule_active_friday ? 'F, ' :
                                            ''}${team.schedule_active_saturday ? 'S' : ''}</td>
                                    </tr>
                                    <tr>
                                        <td>Start</td>
                                        <td>${team.schedule_active_start}</td>
                                    </tr>
                                    <tr>
                                        <td>End</td>
                                        <td>${team.schedule_active_end}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td id="schedule-active-status">${team.schedule_active_status_text}</td>
                                    </tr>
                                    <tr>
                                        <td>Notifications</td>
                                        <td id="schedule-active-notifications">${team.schedule_active_notifications_text}</td>
                                    </tr>
                                </tbody>
                            </table>
        
                            <h3 class=""><span class="highlight-inactive">Inactive</span> Status and Notifications</h3>
                            <table class="mb-1">
                                <tbody>
                                    <tr>
                                        <td>Status</td>
                                        <td id="schedule-inactive-status">${team.schedule_inactive_status_text}</td>
                                    </tr>
                                    <tr>
                                        <td>Notifications</td>
                                        <td id="schedule-inactive-notifications">${team.schedule_inactive_notifications_text}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <a href="#/edit-schedule/${team.slug}" class="btn btn-border mt-2">Edit Schedule</a>
        
                        </div>
                    </div>
                </section>
                <section class="page exceptions schedule-hide" id="exception-block">
                    <div class="row with-checkbox">
                        <div class="col">
                            <h2>Exception Days</h2>
                        </div>
                    </div>
                    <div class="row" id="exception-container">
                        <div class="col">
                            <blockquote>Vacations, time off or other days where we should ignore the schedule and leave the team
                                in whatever state it is in when the day starts.</blockquote>
                            <table class="mb-1">
                                <tbody>
                                    ${team.exceptions.map(exception=> 
                                                                /*html*/`<tr>
                                        <td><em>${exception.start_date !== exception.end_date ? exception.start_date + " - " +
                                                exception.end_date : exception.start_date}</em></td>
                                        <td><a id="exception-edit-btn-${exception.exception_id}"
                                                data-id="${exception.exception_id}" data-startdate="${exception.start_date}"
                                                data-enddate="${exception.end_date}"
                                                data-startdateformatted="${exception.start_date_formatted}"
                                                data-enddateformatted="${exception.end_date_formatted}"
                                                data-title="${exception.start_date !== exception.end_date ? exception.start_date + " - " + exception.start_date : exception.start_date}" class="btn-tiny">EDIT</a>
                                        </td>
                                    </tr>`).join('')}
                                </tbody>
                            </table>
                            <div class="row hidden" id="edit-exception-input">
                                <div class="col">
                                    <h3 id="edit-exception-title">New Exception</h3>
                                    <table class="">
                                        <tbody>
        
        
                                            <tr>
                                                <td>Start Date</td>
                                                <td>
                                                    <p class="error small" id="start-date-warnings"
                                                        style="display:inline;margin-right:10px;"></p><input
                                                        id="edit-exception-start-date-input" type="text" readonly="readonly">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>End Date</td>
                                                <td>
                                                    <p class="error small" id="end-date-warnings"
                                                        style="display:inline;margin-right:10px;"></p><input
                                                        id="edit-exception-end-date-input" type="text" readonly="readonly">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="mt-1 edit-exception-buttons">
                                        <div><span class="btn btn-border" id="save-exception">Save</span></div>
                                        <div><span class="btn btn-border" id="cancel-exception">Cancel</span></div>
                                        <div class="exception-delete"><span class="btn btn-border hidden"
                                                style="border-color:#D32121 !important;color:#D32121;"
                                                id="delete-exception">Delete</span></div>
                                    </div>
                                </div>
                            </div>
                            <span class="btn btn-border mt-2 " id="add-exception">Add Day/Range</span>
                        </div>
                    </div>
                </section>
            </div>
            <section class="page mt-1">
                <div class="row">
                    <div class="col">
                        <h2>Team Timezone</h2>
                        <blockquote class="mt-1">This is the timezone set in slack for this team, to change it just change the dropdown
                        </blockquote>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col">
                        ${team.team_timezone_text}
                    </div>
                </div>
            </section>
            <div class="modal-container ${team.team_is_deleted ? '' : 'hidden'}" id="restore-team">
                <div class="modal">
                    <div class="modal-header" style="display:none;">
                        <div class="header_left">
                            <h1>Delete "${team.slug}"</h1>
                        </div>
                        <div class="header_right">
                            <a id="close-modal" class="btn btn-link">Close</a>
                        </div>
                    </div>
                    <div class="modal-body">
                        <section class="page">
                            <div class="row">
                                <div class="col">
                                    <p style="text-align:center;"><em>Team "${team.slug}" is currently inactive and will be
                                            deleted in ${team.team_days_til_deleted} days(s). Would you like to restore it for
                                            ${Urls.appPriceText} per month?</em></p>
                                </div>
                            </div>
                            <div class="row" style="margin-top:25px;text-align:center;">
                                <div class="modal-col col">
                                    <a id="restore-team-for-real" class="btn btn-border modal-btn">Yes</a>
        
                                    <a href="${Urls.appRoot}${Urls.appHome}" class="btn btn-border modal-btn">No</a>
                                </div>
                            </div>
                            <div class="row" style="margin-top:25px;text-align:center;">
                                <div class="modal-col col">
                                    <a id='delete-team-for-real'><em>... OR click here to delete it now!</em></a>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        
        `
    }
    , after_render: async () => {
        
        if(team.client_status == "in-progress"){
            let interval = setInterval(function(){
                if(team.client_status !== "in-progress"){
                    clearInterval(interval)
                }
                TeamSingleScheduleCombined.render();
            }, 7000)
        }
        
        document.getElementById("grrefresh").addEventListener ("click",  (e) => {
            document.getElementById('grrefresh').classList.remove("grr-show");
            document.getElementById('grrefresh').offsetWidth;
            document.getElementById('grrefresh').classList.add( "grr-hide");
            window.location.reload();
        });
        let exceptionStart = rome(document.getElementById("edit-exception-start-date-input"), {
            dateValidator: rome.val.beforeEq(document.getElementById("edit-exception-end-date-input")),
            inputFormat: "MM-DD-YYYY",
            time: false
          });
          
        let exceptionEnd = rome(document.getElementById("edit-exception-end-date-input"), {
            dateValidator: rome.val.afterEq(document.getElementById("edit-exception-start-date-input")),
            inputFormat: "MM-DD-YYYY",
            time: false,
          });

        selectElement('do_not_disturb_start', team.do_not_disturb_start);
        selectElement('do_not_disturb_end', team.do_not_disturb_end);
        
        
        setScheduleElement(team.schedule_enabled);

        let scheduleActiveDaysCell = document.getElementById("schedule-active-days-cell").innerHTML;
        if(scheduleActiveDaysCell.charAt(scheduleActiveDaysCell.length - 2) == ","){
            document.getElementById("schedule-active-days-cell").innerHTML = scheduleActiveDaysCell.slice(0,-2);
        }
        
        for (var i = 0; i < team.exceptions.length; i++) {
            document.getElementById("exception-edit-btn-" + team.exceptions[i].exception_id).addEventListener ("click",  (e) => {
                document.getElementById("edit-exception-start-date-input").value = e.srcElement.dataset.startdateformatted;
                document.getElementById("edit-exception-end-date-input").value = e.srcElement.dataset.enddateformatted;
                document.getElementById("edit-exception-title").innerHTML = `Edit Exception - ${e.srcElement.dataset.title}`;
                document.getElementById("save-exception").dataset.exception_id = e.srcElement.dataset.id;
                document.getElementById("edit-exception-input").classList.remove('hidden');
                document.getElementById("add-exception").classList.add('hidden');
                document.getElementById("delete-exception").classList.remove('hidden');
                
                
            });
        }

        document.getElementById("add-exception").addEventListener ("click",  (e) => {
            document.getElementById("save-exception").dataset.exception_id = "";
            document.getElementById("edit-exception-title").innerHTML = "New Exception";
            document.getElementById("edit-exception-input").classList.remove('hidden');
            document.getElementById("cancel-exception").classList.remove('hidden');
            document.getElementById("save-exception").classList.remove('hidden');
            document.getElementById("add-exception").classList.add('hidden');
            document.getElementById("delete-exception").classList.add('hidden');
            document.getElementById("edit-exception-start-date-input").value = "";
            document.getElementById("edit-exception-end-date-input").value = "";
        });

        document.getElementById("cancel-exception").addEventListener ("click",  (e) => {
            document.getElementById("edit-exception-title").innerHTML = "New Exception";
            document.getElementById("edit-exception-input").classList.add('hidden');
            document.getElementById("add-exception").classList.remove('hidden');
            document.getElementById("delete-exception").classList.add('hidden');
            document.getElementById("end-date-warnings").innerHTML = "";
            document.getElementById("end-date-warnings").classList.remove("active");
            document.getElementById("start-date-warnings").innerHTML = "";
            document.getElementById("start-date-warnings").classList.remove("active");
        });
        
        document.getElementById("edit-exception-start-date-input").addEventListener("change", (e) => {
            document.getElementById("end-date-warnings").innerHTML = "";
            document.getElementById("end-date-warnings").classList.remove("active");
            document.getElementById("start-date-warnings").innerHTML = "";
            document.getElementById("start-date-warnings").classList.remove("active");
            let end_date = document.getElementById("edit-exception-end-date-input").value;
            let start_date = document.getElementById("edit-exception-start-date-input").value;
            if(setExceptionDate(start_date)){
                if(!setExceptionDate(end_date)){
                    document.getElementById("edit-exception-end-date-input").value = start_date;
                }else if(end_date < start_date){
                    document.getElementById("edit-exception-end-date-input").value = start_date;
                }
            }
            if(!setExceptionDate(document.getElementById("edit-exception-start-date-input").value)){
                document.getElementById("start-date-warnings").innerHTML = "Oh no! Invalid Date!";
                document.getElementById("start-date-warnings").classList.add("active");
            }

            if(!setExceptionDate(document.getElementById("edit-exception-end-date-input").value)){
                document.getElementById("end-date-warnings").innerHTML = "Oh no! Invalid Date!";
                document.getElementById("end-date-warnings").classList.add("active");
            }
        });

        document.getElementById("edit-exception-end-date-input").addEventListener("change", (e) => {
            document.getElementById("end-date-warnings").innerHTML = "";
            document.getElementById("end-date-warnings").classList.remove("active");
            document.getElementById("start-date-warnings").innerHTML = "";
            document.getElementById("start-date-warnings").classList.remove("active");
            let end_date = document.getElementById("edit-exception-end-date-input").value;
            let start_date = document.getElementById("edit-exception-start-date-input").value;
            if(end_date < start_date){
                if(setExceptionDate(start_date)){
                    document.getElementById("edit-exception-end-date-input").value = start_date;
                }
            }
            if(!setExceptionDate(document.getElementById("edit-exception-start-date-input").value)){
                document.getElementById("start-date-warnings").innerHTML = "Oh no! Invalid Date!";
                document.getElementById("start-date-warnings").classList.add("active");
            }

            if(!setExceptionDate(document.getElementById("edit-exception-end-date-input").value)){
                document.getElementById("end-date-warnings").innerHTML = "Oh no! Invalid Date!";
                document.getElementById("end-date-warnings").classList.add("active");
            }
        });

        document.getElementById("save-exception").addEventListener ("click",  (e) => {
            
            document.getElementById("end-date-warnings").innerHTML = "";
            document.getElementById("end-date-warnings").classList.remove("active");
            document.getElementById("start-date-warnings").innerHTML = "";
            document.getElementById("start-date-warnings").classList.remove("active");
            let slug       = document.getElementById("slug-input");
            let exception_id = document.getElementById("save-exception").dataset.exception_id;
            let start_date = exceptionStart.getDate();
            let end_date = exceptionEnd.getDate();
            if(start_date && end_date){
                if(exception_id){
                    postExceptionUpdate(slug.value, exception_id, start_date, end_date)
                }else{
                    postExceptionNew(slug.value, start_date, end_date)
                }    
            }else{
                if(!start_date){
                    document.getElementById("start-date-warnings").innerHTML = "Oh no! Invalid Date!";
                    document.getElementById("start-date-warnings").classList.add("active");
                }

                if(!end_date){
                    document.getElementById("end-date-warnings").innerHTML = "Oh no! Invalid Date!";
                    document.getElementById("end-date-warnings").classList.add("active");
                }
            }
        });

        document.getElementById("delete-exception").addEventListener ("click",  (e) => {
            document.getElementById("end-date-warnings").innerHTML = "";
            document.getElementById("end-date-warnings").classList.remove("active");
            document.getElementById("start-date-warnings").innerHTML = "";
            document.getElementById("start-date-warnings").classList.remove("active");
            let slug       = document.getElementById("slug-input");
            let exception_id = document.getElementById("save-exception").dataset.exception_id;
            
             postExceptionDelete(slug.value, exception_id)
            
        });

        document.getElementById("current-notifications-" + team.current_notifications_type.toLowerCase()).checked = true;

        document.getElementById("do_not_disturb_start").addEventListener ("change",  (e) => {
            let select = document.getElementById("do_not_disturb_start").value;
            let slug = document.getElementById("slug-input").value;
            postDoNotDisturbStartUpdate(slug, select);
        });

        document.getElementById("do_not_disturb_end").addEventListener ("change",  (e) => {
            let select = document.getElementById("do_not_disturb_end").value;
            let slug = document.getElementById("slug-input").value;
            postDoNotDisturbEndUpdate(slug, select);
        });

        document.getElementById("current-notifications-keywords").addEventListener ("click",  (e) => {
            let radio = document.getElementById("current-notifications-keywords").value;
            let slug = document.getElementById("slug-input").value;
            postCurrentNotificationsTypeUpdate(slug, radio);
        });

        document.getElementById("current-notifications-all").addEventListener ("click",  (e) => {
            let radio = document.getElementById("current-notifications-all").value;
            let slug = document.getElementById("slug-input").value;
            postCurrentNotificationsTypeUpdate(slug, radio);
        });

        document.getElementById("current-notifications-none").addEventListener ("click",  (e) => {
            let radio = document.getElementById("current-notifications-none").value;
            let slug = document.getElementById("slug-input").value;
            postCurrentNotificationsTypeUpdate(slug, radio);
        });

        

        document.getElementById("current-notifications-keywords-textarea").addEventListener ("keydown",  (e) => {
            delaySaveKeywords(postCurrentNotificationKeywordsUpdate);
        });

        document.getElementById("current-status-textarea").addEventListener ("keydown",  (e) => {
            delaySaveStatus(postCurrentStatusUpdate);
        });

        

        document.getElementById("presence-switch").addEventListener ("click",  (e) => {
            e.preventDefault()
            let checkbox = document.getElementById("presence-checkbox");
            let slug       = document.getElementById("slug-input");
            if (slug.value =='') {
                alert (`The fields cannot be empty`)
            }else {
                postPresenceActiveUpdate(slug.value, !checkbox.checked);
            }
        });

        document.getElementById("schedule-switch").addEventListener ("click",  (e) => {
            e.preventDefault()
            let checkbox       = document.getElementById("schedule-checkbox");
            let slug       = document.getElementById("slug-input");
            if (slug.value =='') {
                alert (`The fields cannot be empty`)
            }else {
                postScheduleActiveUpdate(slug.value, !checkbox.checked);
            }
        });

        document.getElementById("donotdisturb-switch").addEventListener ("click",  (e) => {
            e.preventDefault()
            let checkbox       = document.getElementById("donotdisturb-checkbox");
            let slug       = document.getElementById("slug-input");
            if (slug.value =='') {
                alert (`The fields cannot be empty`)
            }else {
                postDoNotDisturbActiveUpdate(slug.value, !checkbox.checked);
            }
        });

   
       

        document.getElementById("delete-team-for-real").addEventListener ("click",  () => {
            let team       = document.getElementById("slug-input");
            deleteTeam(team.value);
            
        });
        document.getElementById("restore-team-for-real").addEventListener ("click",  () => {
            let team       = document.getElementById("slug-input");
            restoreTeam(team.value);
            
        });
        
    }
}

export default TeamSingle;