import Urls            from '../../services/Urls.js'

let TOS = {

    render : async () => {
        let view =  /*html*/`
        <style>
        .hide-mobile{
            display: table-cell;
        }
        @media screen and (max-width: 719px) {
            .hide-mobile{
                display: none !important;
            }
        }
        .body_container{
            background:transparent;
            
            // color:#D5D9DE;
        }
        a, a:hover, a:visited, blockquote{
            // color:#D5D9DE;
        }

        .body_container.inactive{
            // border: 1px solid #435264;
            // background:#242949;
            // color:#D5D9DE;

        }
        section a, section a:hover, section a:visited{
            border-bottom:2px solid #0060BF;color:#0060BF
        }
        </style>
        <div class="header_container ">
        <div class="header_container_inner" style="">
        <div class="header_left " style="display: flex;align-items: center;"> 
        <a onclick="history.back()" style="display: flex;align-items: center;">
            <img class="back-arrow" src="assets/img/chevron.png">
            <h1 class="truncate ">
                
                <span class="">Terms of Service</span>
            </h1>
        </a>
    </div>
        </div>
    </div>
        <div class="body_container inactive">
        <section class="page">
            <div class="">
                <div class="row">
                    <div class="col">
                        
                        <blockquote class="mb-1">Updated January 15, 2020</blockquote>
                        <ul class="register-list">
                            <li>A team is defined as any single url, username, password combination that accesses a messaging service (like slack)</li>
                            <li>You agree you're of legal age to use the software in your jurisdiction</li>
                            <li>You agree that you have legal right to use the payment method you use for this software</li>
                            <li>You agree that you have the legal right to use the accounts that will be managed by Status Hammer</li>
                            <li>You agree to keep your account username and password secure</li>
                            <li>You understand our service interfaces with 3rd party software to manage accounts and will share usernames, passwords, team urls and information entered into the software interface with those 3rd parties (this is what the software does)</li>
                            <li>You understand that we will share your billing information including credit card numbers with our payment provider and they will store that information (we use Stripe currently)</li>
                            <li>You understand that we will send emails using SendGrid's api, and this will require sharing your email address with them - <a href="https://sendgrid.com/solutions/email-api/">https://sendgrid.com/solutions/email-api/</a></li>
                            <li>You understand that support requests are handled by Zendesk and request details and your email address will be shared with them - <a href="https://www.zendesk.com/support/">https://www.zendesk.com/support/</a></li>
                            <li>You understand that we will send annonymous technical data to Sentry.io to monitor code errors and uptime. - <a href="https://sentry.io/">https://sentry.io/</a></li>
                            <li>You understand that this agreement may change, and we will give you 30 days notice before that happens including what the changes will be</li>
                            <li>You understand that while we want the software to be live all the time, we may experience outages, thus we cannot guarantee uptime</li>
                            <li>You understand that the software will be updated over time</li>
                            <li>You understand that each team costs $${Urls.appPriceFloat} per month and is billed before the month starts</li>
                            <li>You understand that you will be asked to enter a credit card, this information will be sent to our payment processor (currently stripe) and we will store an id for this card (not the actual card information) in our system for future billing</li>
                            <li>You understand that after your first charge you will be billed monthly on the day of the month of your first charge and if you need to change it you will need to contact customer support.</li>
                            <li>You understand that you may cancel your account at anytime via the software or by contacting customer service</li>
                            <li>You understand that we provide refunds for unused time durinng a billing cycle. Refunds are calculated by subtracting the Stripe fee ($10 * 2.9% )+ $0.30 = $0.59 minus days used, rounded to the nearest 1 day. For example, if you subscribed and immediately canceled you’d be charged for 1 day and the Stripe fee.</li>
                            <li>And finally, you understand that we may cancel an account or services for any reason. While we don't intend to use this, we need this clause in case of jerks (or litigious 3rd parties, or corporate god-emperors, etc).</li>
                        </ul>
                        
                    </div>
                </div> 
            </div>
        </section>
    </div>
        `
        return view
    }
    , after_render: async () => {
    }
}
export default TOS;