import Fetcher        from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'
import Urls            from '../../services/Urls.js'

function singleTeamCallback(json){
    return json
}

let getAccount = async () => {
    try {
        const response = await Fetcher.get(Urls.apiAccount, singleTeamCallback);
        return response;
    } catch (err) {
        console.log('Error getting documents', err)
    }
}

let formatDate = (date) => {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [month, day, year].join('/');
}

let deleteResponse = (json) => {
    if(json['success']){
        Utils.grr("Account deleted. Thanks for using Status Hammer.")
        window.location.hash = Urls.appLogin
   }else{
    document.getElementById("delete-team-for-real").classList.remove('action');
    document.getElementById('loader').classList.remove('loaderFadeInUp');     
    document.getElementById('loader').classList.add('loaderFadeOutDown');   
    Utils.grr(json.message, "error");
   }
};


let deleteAccount = async () => {
    Fetcher.delete(Urls.apiAccount, deleteResponse);
}

let showModal = () => {
    document.getElementById("are-you-sure").classList.remove("hidden");
}

let hideModal = () => {
    document.getElementById("are-you-sure").classList.add("hidden");
}

let UserAccount = {
    render : async () => {
        let request = Utils.parseRequestURL()
        let account = await getAccount()
        let view =  /*html*/`
        <style>
        .hide-mobile{
            display: table-cell;
        }
        @media screen and (max-width: 719px) {
            .hide-mobile{
                display: none !important;
            }
        }
       
        
        </style>
        <div class="header_container ">
            <div class="header_container_inner">
                <div class="header_left" style="display: flex;align-items: center;"> 
                    <a href="${Urls.appRoot}${Urls.appHome}" style="display: flex;align-items: center;">
                        <img  class="back-arrow" src="assets/img/chevron.png"><img class="back-icon" src="assets/img/account.png" style="margin-right:6px;top:0;"><h1>Account</h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="body_container">
            <section class="page">    
                <div class="row">
                    <div class="col">
                        <h2>Billing</h2>
                    </div>
                </div>    
                <div class="row">
                    <div class="col">
                        <ul>
                        ${account.teams.map(team => 
                        `<li>Your next invoice for '${team.slug}' team is <strong>${formatDate(team.next_invoice_date)}</strong> <a class="btn-tiny" href="${Urls.appRoot}${Urls.appEditTeam}${team.slug}">View Team Settings</a></li>`
                        ).join('')}
                        </ul>
                    </div>
                </div>
                <div class="row mt-1 mb-1 ${account.charges.length < 1 ? 'hidden' : ''}">
                    <div class="col">
                        <h3>Recent Charges</h3>
                        <div class="table-container">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Date Paid</th>
                                        <th  class="hide-mobile">Description</th>
                                        <th style="text-align:right;">Amount Paid</th>
                                        <th style="text-align:right;">Amount Refunded</th>
                                        <th style="text-align:right;">Reciept Link</th>
                                    </tr>
                                </thead>
                                <tbody>
                            ${account.charges.map(charge => 
                                /*html*/`<tr>
                                    <td>${charge.date_paid}</td>
                                    <td class="hide-mobile">${charge.description}</td>
                                    <td style="text-align:right;">$${parseFloat(charge.amount_paid).toFixed(2)}</td>
                                    <td style="text-align:right;">$${parseFloat(charge.amount_refunded).toFixed(2)}</td>
                                    <td><a target="_blank" href="${charge.receipt_url}" class="btn-tiny">View</a></td>
                                </tr>
                                `
                            ).join('')}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <a href="${Urls.appRoot}/change-email"><h2>Email <span class="btn-tiny">Change</span></h2></a>
                    </div>
                </div>    
                <div class="row">
                    <div class="col">
                        ${account.username}
                    </div>
                </div>
            </section>
            <section class="page">    
                <div class="row">
                    <div class="col">
                        <a href="${Urls.appRoot}/change-password"><h2>Password <span class="btn-tiny">Change</span></h2></a>
                    </div>
                </div>    
                <div class="row">
                    <div class="col">
                        ************
                    </div>
                </div>
            </section>
            <section class="page"> 
                <div class="row">
                    <div class="col">
                        <a href="${Urls.appRoot}/change-cc"><h2>Current Card <span class="btn-tiny">Change</span></h2></a>
                    </div>
                </div>    
                <div class="row">
                    <div class="col">
                        ${account.card_text}
                    </div>
                </div>
            </section>
            <section class="page mt-2"> 
                <div class="row">
                    <div class="col">
                        <a href="#" id="delete-account" style="color:#FD2D55;">Delete Status Hammer Account</a>
                    </div>
                </div>
            </section>
        </div>
        
            <div class="modal-container hidden" id="are-you-sure">
                <div class="modal">
                    <div class="modal-header" style="display:none;">
                        <div class="header_left"><h1>Delete Account</h1></div>
                        <div class="header_right">
                            <a id="close-modal" class="btn btn-link">Close</a>
                        </div>
                    </div>
                    <div class="modal-body">
                        <section class="page">
                            <div class="row" >
                                <div class="col">
                                    <h2 class="mb-1" style="text-align:center;display:block;">Are you sure you want to delete your Status Hammer account? </h2>
                                    <blockquote>Your account will be permanently deleted along with all teams. You will no longer have access to this page. We will send you a confirmation email and issue refunds for any remaining balances on your account.</blockquote>
                                    <blockquote>Refunds are calculated by subtracting the Stripe fee ($10 * 2.9% )+ $0.30 = $0.59 minus days used, rounded to the nearest 1 day. For example, if you subscribed and immediately canceled you’d be charged for 1 day and the Stripe fee. </blockquote>
                                </div>
                            </div>  
                            <div class="row" style="margin-top:25px;text-align:center;">
                                <div class="modal-col col">
                                    <a id="delete-account-for-real" class="btn btn-border modal-btn">Yes</a>
                                
                                    <a id="close-modal-2" class="btn btn-border modal-btn">No</a>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        `
        return view
    },
    after_render: async () => {
        document.getElementById("delete-account").addEventListener ("click",  (e) => {
            e.preventDefault();
            // let team       = document.getElementById("team-name");
            // deleteTeam(team.value);
            showModal();
        });

        document.getElementById("close-modal").addEventListener ("click",  () => {
            // let team       = document.getElementById("team-name");
            // deleteTeam(team.value);
            hideModal();
        });
        
        document.getElementById("close-modal-2").addEventListener ("click",  () => {
            // let team       = document.getElementById("team-name");
            // deleteTeam(team.value);
            hideModal();
        });

        document.getElementById("delete-account-for-real").addEventListener ("click",  () => {
            document.getElementById("delete-account-for-real").classList.add('action');
            document.getElementById('loader').classList.remove('loaderFadeOutDown');
            document.getElementById('loader').classList.add('loaderFadeInUp');
            deleteAccount();
            
        });
    }
        
}

export default UserAccount;