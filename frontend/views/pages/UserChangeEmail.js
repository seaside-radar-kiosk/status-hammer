import Fetcher        from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'
import Urls            from '../../services/Urls.js'
import Helpers            from '../../services/Helpers.js'

function singleTeamCallback(json){
    return json
}

let getAccount = async () => {
    try {
        const response = await Fetcher.get(Urls.apiAccount, singleTeamCallback);
        return response;
    } catch (err) {
        console.log('Error getting documents', err)
    }
}

let setCookiesAndForward = (json) => {
    if(json.success){
        Utils.grr("Email udpated")
        window.location.hash = Urls.appAccount
    }else{
        document.getElementById("save_btn").classList.remove('action');
        Utils.grr(json.message, "error");
    }
};

let postemailUpdate = async (username, email, password) => {
    let body = JSON.stringify({
        'new-email':email,
        'username':username,
        'password':password
    })
    Fetcher.post(Urls.apiAccount, body, setCookiesAndForward);
}

let UserChangeemail = {

    render : async () => {
        let request = Utils.parseRequestURL()
        let account = await getAccount()
        
        return /*html*/`
      
            <div class="header_container ">
                <div class="header_container_inner">
                    <div class="header_left" style="display: flex;align-items: center;"> 
                        <a href="${Urls.appRoot}${Urls.appAccount}" style="display: flex;align-items: center;">
                            <img  class="back-arrow" src="assets/img/chevron.png">
                            <h1 class="">
                            <span class="">Change Email</span>
                                
                            </h1>
                        </a>
                    </div>
                </div>
            </div>
            </div>
            <div class="body_container">
                <form>
                    <section class="page">
                        <div class="row ">
                            <div class="col">
                                <blockquote>Update your email address here</blockquote>
                            </div>
                        </div>     
                        <div class="row">
                            <div class="col">
                                <h2>Current Email Address: ${account.username} </h2>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col">
                                <h2>New Email Address</h2>
                            </div>
                        </div>    
                        <div class="row ">
                            <div class="col">
                                <input type="text" id="new-email-input" value="" placeholder="New email address">
                                <input type="hidden" id="username-input" value="${account.username}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h2>Current Password (for security)</h2>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col">
                                <input type="password" id="password" value="" placeholder="Enter current password">
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col">
                                <button class="btn btn-border is-primary" id="save_btn">Save</button>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        `
    }
    , after_render: async () => {
        document.getElementById("save_btn").addEventListener ("click",  () => {
            Utils.grr(``, "hide");
            let username       = document.getElementById("username-input");
            let email       = document.getElementById("new-email-input");
            let password       = document.getElementById("password");
            if (username.value =='' | email.value == '' | password.value == '') {
                Utils.grr("The fields cannot be empty", "warning");
            }else if(!Helpers.validateEmail(email.value)){
                Utils.grr(`${email.value} is not a valid email address.`, "warning");
            }else {
                document.getElementById("save_btn").classList.add('action');
                postemailUpdate(username.value, email.value, password.value);
                
            }    
        })
    }
}

export default UserChangeemail;