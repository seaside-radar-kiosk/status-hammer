import Fetcher        from '../../services/Fetcher.js'
import Utils        from '../../services/Utils.js'
import Urls            from '../../services/Urls.js'

function singleTeamCallback(json){
    return json
}

let getAccount = async () => {
    try {
        const response = await Fetcher.get(Urls.apiAccount, singleTeamCallback);
        return response;
    } catch (err) {
        console.log('Error getting documents', err)
    }
}

let setCookiesAndForward = (json) => {
    if(json.success){
        window.location.hash = Urls.appAccount
    }else{
        console.log("ERROR")
    }
};

let postCCUpdateCallback = (json) => {
    if(json.success){
        Utils.grr(json.message)
        window.location.hash = Urls.appAccount
   }else{
        Utils.grr(json.message, "error");
        document.getElementById("save_btn").classList.remove('action');
        
        document.getElementById('loader').classList.remove('loaderFadeInUp');     
        document.getElementById('loader').classList.add('loaderFadeOutDown');   
   }
};

let postCCUpdate = async (username, newCcNumber, newCcExpMonth, newCcExpYear, newCcCvc , password) => {
    let body = JSON.stringify({
        'new-cc-number':newCcNumber,
        'new-cc-exp-month':newCcExpMonth,
        'new-cc-exp-year':newCcExpYear,
        'new-cc-cvc':newCcCvc,
        'username':username,
        'password':password
    })
    Fetcher.post(Urls.apiAccount, body, postCCUpdateCallback);
}

let cc_format = (value) => {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    var matches = v.match(/\d{4,16}/g);
    var match = matches && matches[0] || ''
    var parts = []
    for (let i=0, len=match.length; i<len; i+=4) {
      parts.push(match.substring(i, i+4))
    }
    if (parts.length) {
      return parts.join(' ')
    } else {
      return value
    }
  }

let UserChangeCC = {

    render : async () => {
        let request = Utils.parseRequestURL()
        let account = await getAccount()
        
        return /*html*/`
            <div class="header_container ">
                <div class="header_container_inner">
                    <div class="header_left" style="display: flex;align-items: center;"> 
                        <a href="${Urls.appRoot}${Urls.appAccount}" style="display: flex;align-items: center;">
                            <img  class="back-arrow" src="assets/img/chevron.png">
                            <h1 class="">
                            <span class="">Change CC</span>
                                
                            </h1>
                        </a>
                    </div>
                </div>
            </div>
            <div class="body_container">
                <form>
                    <section class="page">
                        <div class="row">
                            <div class="col">
                                <blockquote>Update your credit card here</blockquote>
                            </div>
                        </div>     
                        <div class="row ${account.has_card ? '' : 'hidden'}">
                            <div class="col">
                                <h2>Current Credit Card #: ${account.card_text}</h2>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col">
                                <h2>New Credit Card #</h2>
                            </div>
                        </div>    
                        <div class="row ">
                            <div class="col">
                                <input type="text" id="new-cc-number" value="" placeholder="New credit card #">
                                <input type="hidden" id="username-input" value="${account.username}">
                            </div>
                        </div>
                        <div class="row " style="flex-wrap:unset;">
                            <div class="col" style="min-width:unset;padding-right:10px;">
                                <select id="new-cc-exp-month" class="input select-css" style="width:100%;">
                                    <option value="1">01</option>
                                    <option value="2">02</option>
                                    <option value="3">03</option>
                                    <option value="4">04</option>
                                    <option value="5">05</option>
                                    <option value="6">06</option>
                                    <option value="7">07</option>
                                    <option value="8">08</option>
                                    <option value="9">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                            <div class="col" style="min-width:unset;padding-right:10px;">
                                <select id="new-cc-exp-year" class="input select-css" style="width:100%;">
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                    <option value="2026">2026</option>
                                    <option value="2027">2027</option>
                                    <option value="2028">2028</option>
                                    <option value="2029">2029</option>
                                    <option value="2030">2030</option>
                                    <option value="2031">2031</option>
                                    <option value="2032">2032</option>
                                    <option value="2033">2033</option>
                                    <option value="2034">2034</option>
                                    <option value="2035">2035</option>
                                    <option value="2036">2036</option>
                                    <option value="2037">2037</option>
                                    <option value="2038">2038</option>
                                    <option value="2039">2039</option>
                                    <option value="2040">2040</option>
                                </select>
                            </div>
                            <div class="col" style="min-width:unset;">
                                <input type="text" id="new-cc-cvc" value="" placeholder="CVC" style="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h2>Current Password (for security)</h2>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col">
                                <input type="password" id="password" value="" placeholder="Enter password">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button class="btn btn-border is-primary" id="save_btn">Add New Card</button>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        `
    }
    , after_render: async () => {
        document.getElementById('new-cc-number').addEventListener ("keyup",(e) => {
            document.getElementById('new-cc-number').value = cc_format(document.getElementById('new-cc-number').value);
        });
        document.getElementById("save_btn").addEventListener ("click",  () => {
            Utils.grr(``, "hide");
            let username       = document.getElementById("username-input");
            let newCcNumber       = document.getElementById("new-cc-number");
            let newCcCvc       = document.getElementById("new-cc-cvc");
            let newCcExpMonth       = document.getElementById("new-cc-exp-month");
            let newCcExpYear       = document.getElementById("new-cc-exp-year");
            let password       = document.getElementById("password");
            if (username.value =='' || newCcNumber.value =='' || newCcExpMonth.value =='' || newCcExpYear.value =='' || newCcCvc.value =='' || password.value =='') {
                Utils.grr(`The fields cannot be empty`, 'warning');
            }else {
                //alert(`User with email ${email.value} was successfully submitted!`)
                document.getElementById('loader').classList.remove('loaderFadeOutDown');
                document.getElementById('loader').classList.add('loaderFadeInUp');  
                document.getElementById("save_btn").classList.add('action');
                postCCUpdate(username.value, newCcNumber.value, newCcExpMonth.value, newCcExpYear.value, newCcCvc.value, password.value);
                
            }    
        })
    }
}

export default UserChangeCC;