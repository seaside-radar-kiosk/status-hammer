import Fetcher        from '../../services/Fetcher.js'
import Urls            from '../../services/Urls.js'
import Cookies        from '../../services/Cookies.js'

let postResponse = (json) => {
    if(json['logout']){
        let body = JSON.stringify({
            'logout':true,
        })
        Fetcher.postLogoutRefresh(Urls.apiLogoutRefresh, body, postResponse2);
   }else{
       console.log("ERROR!")
   }
};

let postResponse2 = (json) => {
    if(json['logout']){
        Cookies.eraseCookie('csrf_access_token');
        Cookies.eraseCookie('csrf_refresh_token');
        window.location.hash = Urls.appLogin;
   }else{
       console.log("ERROR!")
   }
};

let logout = async () => {
    let body = JSON.stringify({
        'logout':true,
    })
    Fetcher.postLogoutAccess(Urls.apiLogoutAccess, body, postResponse);

}

let Bottombar = {
    render: async (lastUpdateStamp) => {
        let view = ''
        if(Cookies.getCookie('csrf_access_token')){
            view =  /*html*/`
            <footer class="footer">
            <div class="row">
                                <div class="col wrap " style="text-align:center;padding:0;min-width:unset;width:100%;">
                                <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appRoot}${Urls.appLanding}">Home</a> &nbsp;
                                <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" id="logout">Logout</a> &nbsp;
                                <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appRoot}${Urls.appPrivacy}">Privacy</a> &nbsp; 
                                <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appRoot}${Urls.appTOS}">TOS</a> &nbsp; <span class="footer-piece">Questions? Comments? <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appSupportHref}">Email</a> or <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appSupportCalendarly}" target="_blank">schedule a call</a>.</span> <span class="footer-piece">&copy; 2019-2020 Status Hammer</span>
                            
                                </div>
                                
                            </div>
               
            </footer>
            `;
        }else{
            view =  /*html*/`
            <footer class="footer">
            <div class="row">
                                <div class="col wrap " style="text-align:center;padding:0;min-width:unset;width:100%;">
                                <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appRoot}${Urls.appLanding}">Home</a> &nbsp;
                                <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appRoot}${Urls.appLogin}">Login</a> &nbsp;
                                <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appRoot}${Urls.appRegister}">Register</a> &nbsp; 
                                <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appRoot}${Urls.appPrivacy}">Privacy</a> &nbsp; 
                                <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appRoot}${Urls.appTOS}">TOS</a> &nbsp; <span class="footer-piece">Questions? Comments? <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appSupportHref}">Email</a> or <a style="border-bottom:1px solid #A6AFBA;color:#A6AFBA" href="${Urls.appSupportCalendarly}" target="_blank">schedule a call</a>.</span> <span class="footer-piece">&copy; 2019-2020 Status Hammer</span>
                                
                                </div>
                                
                            </div>
               
            </footer>
            `;
        }
        
        return view
    },
    after_render: async () => { 
        if(document.getElementById("logout")){
            document.getElementById("logout").addEventListener ("click",  () => {
                logout();
            });    
        }
    }

}

export default Bottombar;