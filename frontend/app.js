"use strict";

import Router                   from '../../services/Router.js'

// Listen on hash change:
window.addEventListener('hashchange', Router.router);

// Listen on page load:
window.addEventListener('load', Router.router);
