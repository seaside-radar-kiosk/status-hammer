"""empty message

Revision ID: 4e4ed82799fb
Revises: 2267a48a3f03
Create Date: 2020-01-13 12:00:51.086275

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4e4ed82799fb'
down_revision = '2267a48a3f03'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('has_used_trial3', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users', 'has_used_trial3')
    # ### end Alembic commands ###
